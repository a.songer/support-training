## 1. Training name and description
--------------------
The training entails introduction and Troubleshooting to GitLab migration. 

Migrations are a convenient way for you to alter your database in a structured and organized manner. The traditional method would be to edit SQL fragments by hand, but you would then be responsible for telling other developers that they need to go and run them. You’d also have to track which changes need to be run against the production machines next time you deploy.


## 2. Prerequisites
--------------------
Introduction to database migrations you require the following [Google doc](https://docs.google.com/document/d/1EowVXYiMBvSFKG0Ii64HhmqUMrBxYv3pOr-s8HF6TH8/edit#heading=h.ydfnf7tc0m15) 

## 3. Video link
--------------------
Provide a link to the video which should be uploaded to GitLab Unfiltered and added to the play list 

## 4. Training Steps
--------------------
The training steps are on the [Google doc](https://docs.google.com/document/d/1EowVXYiMBvSFKG0Ii64HhmqUMrBxYv3pOr-s8HF6TH8/edit#heading=h.ydfnf7tc0m15) 


## 5. Troubleshooting Steps
--------------------
Troubleshooting steps are on the [Google doc](https://docs.google.com/document/d/1EowVXYiMBvSFKG0Ii64HhmqUMrBxYv3pOr-s8HF6TH8/edit#heading=h.ydfnf7tc0m15) 
