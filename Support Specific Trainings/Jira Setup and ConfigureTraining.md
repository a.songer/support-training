## 1. Training name and description
--------------------
The training entails how to setup Jira server and to perfom various integration that are in our documentation. 


## 2. Prerequisites
--------------------
For you to setup Jira Server, you require the following [prerequisites](https://handbook.gitlab.com/handbook/support/workflows/jira-server/#prerequisite-software)

## 3. Video link
--------------------
Provide a link to the video which should be uploaded to GitLab Unfiltered and added to the play list [Support Training Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KoikKNUBkzx7m0VcjeSaHGt)


## 4. Training Steps
--------------------
The training steps are on the [handbook](https://handbook.gitlab.com/handbook/support/workflows/jira-server/) 


## 5. Troubleshooting Steps
--------------------
Troubleshooting steps are on the [handbook](https://handbook.gitlab.com/handbook/support/workflows/jira-server/#common-troubleshooting-steps-for-jira-tickets)
