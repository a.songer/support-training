# Support Specific Trainings

This directory is designed as a location to store training material created by support engineers which is not suitable for creating using the existing module issues template format.  Training that should typically stored here includes but is not limited to:

- Technical walkthroughs
- GitLab configuration and/or migrations
- Troubleshooting GitLab components
- Technical how to's
- Third party intergrations





## Training Creation Steps

1. Create a new blank file in this `Support Specific Trainings` directory 
1. Copy the template below into the blank file to use as guideance on how to format the training material
1. Complete the various sections, provide an accurate file name and save the file the `.md` extension
1. Reqeust a peer review from a collleague in support
1. Consider if your new training material could be added to any of the existing [module training templates](https://gitlab.com/gitlab-com/support/support-training/-/tree/main/.gitlab/issue_templates) or the GitLab Docs
1. Make the wider support team aware of the training in the #support_team_chat, regional meeting and the SWIR


## Template

### 1. Training description
--------------------
Provide a synopsis of the training and what it entails


### 2. Prerequisites
--------------------
Provide a link to or a description of the required prerequisites for completing the training


### 3. Video link
--------------------
Provide a link to the video which should be uploaded to GitLab Unfiltered and added to the play list [Support Training Videos](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq30AAIrAhnxip2UIPrEiLC)


### 4. Training Steps
--------------------
Add the configuration or training steps in this section


### 5. Troubleshooting Steps
--------------------
Add the steps required for debugging issues
