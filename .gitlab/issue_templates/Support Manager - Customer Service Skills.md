---
module-name: "Support Managers - Customer Service Skills"
area: "Customer Service"
maintainers:
  - lyle
level: Beginner
---

## Introduction

Welcome to the Customer Service Skills training issue for Support Managers. This module is an abbreviated version of the full [Customer Service Skills Module](Customer Service Skills.md). As a manager, you likely already have some training in this set of skills so this overview is intended to make sure you're aware of the standards GitLab Support expects of its Support Engineers.

> **As you work through this issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**Goals of this module**

At the end of this module you will be able to:
- understand how we interact with customers
- utilize your customer service skills to ensure customer success through coaching your direct reports in these skills
- capture the shared language we use to talk about the different aspects of customer service at GitLab

**General Timeline and Expectations** 

- This module should take 30-60m to complete.

## Customer Service, Customer Communications and Ticket Management at GitLab Support

### Related Handbook Pages

1. [ ] Read the `We care for our customers` section on the [support team's home page](https://handbook.gitlab.com/handbook/support/#we-care-for-our-customers) in the handbook to understand what we are responsible for.
1. [ ] Read [how to respond to tickets](https://handbook.gitlab.com/handbook/support/workflows/how-to-respond-to-tickets/) for tips and tricks for humanizing your responses when talking to customers.
1. [ ] Read about how to handle [feedback and complaints](https://handbook.gitlab.com/handbook/support/workflows/feedbacks_and_complaints/) from customers.

### Course Introduction

- [ ] Please read the [Introduction to Communications Training document](https://docs.google.com/document/d/1Ft8A91sc8AA2A-7KkdfOrOeoUO5xUnsGDZ0bZoKBntw/edit?usp=sharing) - it's only one page - to get a little flavor of what this is about.

### Supplemental Course Materials

- [Ticket Management - Quick Reference Guide](https://docs.google.com/document/d/1O9ZzUCJxnuE42ssJ-pir7QK16Vucja6pKk321LBzyyo)
  : A very handy one-page document with key ideas and tips from all six core concepts of the course
- Presentations - The original Google Slides presentations for all six core concepts, for your convenience
  - [01 Connect](https://docs.google.com/presentation/d/1I78r2pmF_GEyTOf3UuAXPaYObdp3SBPoLW1lHS3VX4M)
  - [02 Clarify](https://docs.google.com/presentation/d/1LkE2f6bvuFuPVMAnPm1p49JhM4RrGCnQRf9hDkcDoIU)
  - [03 Create Positive Outcomes](https://docs.google.com/presentation/d/1rimVg34grAbQdwnE8o7yxCKj69mlyrwLVjVIRdgVLtc)
  - [04 Own the Interaction](https://docs.google.com/presentation/d/1MhmcqL5QtZRPaY1lgbpSwE683R5whwTzgHSWW7UJLkM)
  - [05 Educate](https://docs.google.com/presentation/d/1Y3PK5GgA6GZoWonMzRBLd9QS0bHpJsgEg_V9NFCewvQ)
  - [06 Close the Loop](https://docs.google.com/presentation/d/1StXRwYh8Uzjor0IeZdXncUq-3kQ8AyP_Pq7s7BjuJ8I)
- Google's IT Support Customer Service Course: [How to Deal with Difficult Situations](https://www.coursera.org/lecture/technical-support-fundamentals/how-to-deal-with-difficult-situations-yWuOR). This short (~10 mins) video provides some great tips on how to handle yourself and difficult customers.


Unless otherwise indicated, each video below can be found on YouTube in the
[Support Training - Ticket Management & Communications](https://youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
playlist. They're listed in order by module number, video number and title. For
example, the `01 Introduction` video on module 1 can be found in the playlist
under `1 01 Introduction`.

Note: Currently, the videos are not captioned. If you need captions please reach out to your manager and make use of automatic captioning in the meantime.

<details>
  <summary>Section 01: Connect</summary>

### Section 1: Connect

#### Key Concepts:
- Humans (Support Engineers _and_ Customers) have needs, wants and fears.
- Tickets are human interactions: we need to seek to connect by personalizing interactions based on what customers need from us.
- Customers will tell us what they need IF we pay attention.

#### Managers, watch out for:
- Are your Support Engineers seeking to build rapport with their customers?
- In your reviews of tickets, are your Support Engineers understanding the problems that customers are expressing?
- Are your Support Engineers reflecting the expressed emotions or urgency in their tickets?


#### Exercises:
1. [ ] Think about two different experiences in which **you** were the
  customer - one that was good and one that was not. In a comment in this
  issue with a title `1.01 What And How`:
    1. Describe the "what" and "how" for each of them
    1. For the good experience, note whether both the "what" and the "how"
      were present
    1. For the bad experience, note what was missing - the "what", the "how"
      or both 
    **_(Remember that this issue is [non-confidential and is public ](https://about.gitlab.com/handbook/values/#transparency)- do not specify the company or customer name in your example)_**
    1. Note the impact each experience had on you as a customer of that business
2. [ ] As a Support Manager, who is _your_ customer? Leave a comment in this issue with the title `1.02 - Who?`
    1. What are the results - the "what" - that those customers want?
    1. "How" do they want those results delivered?
3. [ ] In a comment in this issue with a title `1.12 Triggering Fears`,
      1. Describe some things you have observed or done that played to a
         customer's fears instead of supporting their core human needs.
      1. Identify which one or more of the core fears were triggered.
4.  [ ] In a comment in this issue with a title `1.13 Supporting Core Needs`,
      1. Describe something you try to do when working with customers to
         support their core human needs.
      1. Describe something new you will try to do, or a technique you'll try to coach in order to better support customers' core human needs
</details>

<details>
  <summary>Section 02: Clarify</summary>

### Section 02: Clarify

#### Key Concepts
- To succeed in tickets, we have to take a proactive posture in communicating with our customers.
- Avoid fluff: send substantive replies where you set, reset and exceed expectations for what the next actions are on your side and the customers' side.
- We define a specific structure for good communication:
   - **Greet** the customer
   - **State the ticket subject** in your own words
   - Give a **brief history** of previous actions
   - Explain the **current state**
   - Describe the next **action**, including who is responsible, what it is, why it's important and when it should be expected.
   - **Wrap up and close**


#### Managers, watch out for:
- Have your Support Engineers consistently set expectations in their replies?
- Have they met or exceeded those expectations?
- Is their communication clear and complete?
- Are they explicit in their asks?
- If they have languishing tickets, where is the breakdown in communication occurring?
- Are your Support Engineers using calls appropriately in their work?

#### Exercises

1. [ ] Watch [Video 03a Two Sync Viewpoints](https://www.youtube.com/watch?v=zPRCSVaGT1g&list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o&index=27) and read the [Customer Calls Workflow](https://handbook.gitlab.com/handbook/support/workflows/customer_calls/)
    - [ ] In a comment in this issue with a title `2.01 Sync Viewpoints`,
        1. Summarize the pros and cons you perceived in each of the presented perspectives.
        2. Now watch the follow-up video [Video 03b Why Sync Is Valuable](https://www.youtube.com/watch?v=ufliYIZmqrA&list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o&index=28)
        3. Describe how you plan to coach (or currently coach) your Support Engineers as they approach synchronous support in a company that emphasizes async.
2. [ ] Watch [2 05 - SLA Clock](https://www.youtube.com/watch?v=CVue_aHcGlc&list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o&index=30) and in a comment in this issue with a title `2.02 Managing Expectations`:
      1. To what extent do your Support Engineers respond quickly and set expectations vs. take more time to respond with a more full action plan?
      2. Which tactics are appropriate in which situation?

</details>
<details>
  <summary>Section 03: Create Positive Outcomes</summary>

### Section 03: Create Positive Outcomes

#### Key Concepts
- Find the “yes” - what **can** we do? (even if we can’t do exactly what the customer wants)
- Continue driving the ticket, even when there are external factors preventing progress.
- Apologize well
- Always speak highly and honestly of our products and team

#### Managers, watch out for:
- Are your engineers creating positive outcomes by taking ownership (different from assignment) of the ticket?
- Do your engineers disparage, blame or otherwise speak poorly of members of other GitLab teams?
- When mistakes are made, do your engineers recover gracefully?

#### Exercises
- [ ] **Exercise 3.01**
  - In a comment in this issue with a title `3.01 Understanding the Customer`,
    1. Answer this question: What are the different priorities, pressures and
        motivations our customers have? Some guidance as you consider your
        answer:
        1. Think about the situation behind the customer's problem or question.
          Are they anxious about something, is something pushing them, why are
          they trying to do what they're doing?
        1. Possible things include: security, time, personal reputation,
          organization reputation, their boss, not understanding their
          assigned task, etc.
- [ ] **Exercise 3.02**
    - In a comment in this issue with a title `3.02 Advocate for GitLab`,
      1. Write down some thoughts about what NOT to say. That is, what things
         might we say, but shouldn't, because they are somewhat opposite to
         advocating for GitLab, our product, and our team members?

</details>

<details>
  <summary>Section 04: Own the Interaction</summary>

### Section 04: Own the Interaction

#### Key Concepts
- Calls should be used appropriately to move tickets forward with clear purposes and boundaries.
- Engineers should vary their communication strategies based on customer ability, familiarity and emotional state.
- Expectations should be continually set, reset and exceeded.

#### Managers, watch out for:
- Do your engineers hesitate to move to calls and delay resolution of their tickets? Do they over rely on calls and struggle to communicate effectively asynchronously? _(Yes! We asked this in Section 02 - it's that important.)_
- To what extent are your engineers able to vary their language with customers?
- Can your engineers effectively run calls, track action items and follow-up with customers?
- Do your engineers set and exceed expectations in their communications with customers?

#### Exercises
 
In the following, feel free to take the perspective of either a Support Engineer _or_ a manager handling an escalated case.
 
- [ ] Read, or reread the [Customer Calls Workflow](https://handbook.gitlab.com/handbook/support/workflows/customer_calls/)
- [ ] **Exercise 4.01:**
  - In a comment in this issue with a title `4.01 Effective call use`,
    1. Reflect on your career: how long did it take for you to get comfortable with customer calls? What things helped you? 
    1. Describe how you're helping your reports navigate this topic: what are you doing to help your Support Engineers
       become more comfortable in using calls.

</details>

<details>
  <summary>Section 05: Educate</summary>

### Section 05: Educate

#### Key Concepts
- Enhance the customer’s knowledge and understanding of the product, with a goal of the customer standing on their own and understanding our products to the extent that makes sense for them and their role.
- Avoid telling the customer what won't help them. For example, speaking poorly about the product or company or your own or team constraints.
- Educate customers every step of the way. Tell them what you're doing, and why you're doing it, in a clear and succinct manner.

#### Managers, watch out for:
- Are your engineers bringing the customer with them by explaining along the way?
- Do your engineers consistently make excuses for delivering less-than-ideal results by blaming the product, other teams, process, personal or team limitations?
- Are your engineers maintaining strong control over their tickets from beginning to end? How often do they fall into reactive patterns, only communicating when the customer or a bot prompts them to?

#### Exercises
- [ ] **Exercise 5.01:**
  - In a comment in this issue with a title `5.01 Ticket closure`,
    1. One of the concepts this module stresses is proactively securing customer agreement to close tickets. Describe how you approach this topic with your Support Engineers and to what extent you monitor for, and give feedback about proactive communication.
</details>

<details>
  <summary>Section 06: Close the Loop</summary>

### Section 06: Close the Loop

#### Key Concepts
- Following meetings, make sure to send a summary of the meeting and next steps.
- Before closing a ticket, summarize and confirm.
- Following ticket interactions, update documentation, create issues or improve training materials.

#### Managers, watch out for:
- Are your engineers writing meeting summaries that would let others follow along and take over if needs be?
- To what extent are your engineers providing closing summaries to tickets?
- Are your engineers creating docs MRs, training MRs or issues to improve things for the future?

#### Exercises
- [ ] Another free checkbox!
</details>

<details>
  <summary>Section 07: Reflect</summary>

### Section 07: Reflect

#### Exercises
- [ ] Read back through the concepts and questions posed in this training.
- [ ] **Exercise 7.01:**
  - In a comment in this issue with a title `7.01 Reflection`,
     1. Describe, in general, in which areas do your direct reports do well in taking care of customers?
        Which areas do they struggle with?
-  [ ] Update your 1:1 docs as appropriate for any items you want to start tracking or coaching on. 
</details>


#### Congratulations! You made it, and should now have a strong understanding of how we approach customer service and ticket management at GitLab!

We hope that in the future you'll revisit individual
sections whenever you want a little refresher on specific topics.

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~"Module::Support Manager - Customer Service Skills"
