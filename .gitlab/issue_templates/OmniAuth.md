---
module-name: "OmniAuth"
area: "Product Knowledge"
gitlab-group: "Manage:Access"
maintainers:
  - TBD
---

## Overview

**Goal**: Set a clear path for OmniAuth Expert training

**Objectives**: At the end of this module, you should be able to:

- Understand how GitLab leverages the OmniAuth gem for user authentication
- Troubleshoot customer's issues with OmniAuth

Remember to contribute to any documentation that needs updating

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: OmniAuth - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical OmniAuth questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what OmniAuth is

- [ ] **Done with Stage 1**

1. [ ] Read through the [GitLab OmniAuth Documentation](https://docs.gitlab.com/ee/integration/omniauth.html)
1. [ ] Read through the [OmniAuth gem documentation](https://github.com/omniauth/omniauth)
1. [ ] Watch the Support Authentication Deep Dive (recorded June 2020) and review the accompanied slides:
      - [ ] [Session 1 of Deep Dive](https://drive.google.com/file/d/16hDb4lHXril_1UmchI5_NlH8iN_b6Kdl/view?usp=sharing)
      - [ ] [Session 2 of Deep Dive](https://drive.google.com/file/d/1nNFX-v1AvCaoibDrcw57jxD59BR-sT8Z/view?usp=sharing)
      - [ ] [Deep Dive Slides](https://docs.google.com/presentation/d/1S8IrmKBLMOsSxEJNQHBLja1ax3qZ0YFicUBM_RFtvVg/edit?usp=sharing)

## Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Implement OmniAuth on a test Omnibus instance
   1. [ ] Choose two OmniAuth Supported Providers from the [list](https://docs.gitlab.com/ee/integration/omniauth.html#supported-providers) and read through their documentation.
   1. [ ] Set up the [initial OmniAuth Configuration](https://docs.gitlab.com/ee/integration/omniauth.html#initial-omniauth-configuration)
   1. [ ] Implement OmniAuth on your test instance using the providers' documentation.

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through 10 solved OmniAuth tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 OmniAuth tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Pair on Customer Calls (Optional)

- [ ] **Done with Stage 4**

1. [ ] Pair on two Support calls, where a customer has a problem with OmniAuth.
   1. [ ] call with ___
   1. [ ] call with ___

## Stage 5: Assessment

Note: Please do not look at the assessment until you are ready to complete it.

- [ ] **Done with Stage 5**

1. [ ] Complete [the assessment](https://forms.gle/4RBqSATu7FfxLMLg9).
  - **Please consider improvements/additional questions! An assessment should have 8-10 questions. Once that's reached, feel free to remove this line.**
  - If you are linked to a Google form, please complete the self-assessment. If you have any questions about the answers, please ask your trainer or an expert.
  - If something is incorrect, please consult with your trainer or an expert and fix it in the [Support Team Drive Training/Training Module Assessments folder](https://drive.google.com/drive/u/0/folders/147D5ecSDsV2J9OCN_6t-J4vYzPLTebue).


## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in OmniAuth on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).
1. [ ] Consider the LDAP and/or SAML modules to become a user authentication expert.

/label ~module
/label ~"Module::OmniAuth"
/label ~"group::authentication and authorization" 
/assign me