## Overview

This module has been DEPRECATED. Please use the [License and Renewals](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/License%20and%20Renewals.md) training issue template instead.

- **Goal**: Set a clear path for Growth queue training
- **Length**: 10 - 14 hours. _(Consider using the [Time Tracking](https://docs.gitlab.com/ee/user/project/time_tracking.html) functionality so that the estimated length for the module can be refined.)_
- **Objectives**: At the end of this module, you should be able to:

    1. Respond and triage tickets in the License, Renewals & Upgrades queue.
    2. Provide internal support to sales and accounts on requests related to licensing, subscriptions, customers portal capabilities, trials and EULA requests.
    3. Have an advanced understanding of how self-managed licenses are generated and be able to efficiently troubleshoot related issues.
    4. Create issues related to the Growth Stage in various issue trackers.
    5. Be eligible to act as a support counterpart for any of the groups within the Growth stage.

___
### Stage 0: Create Your Module
1. [ ] Create an issue using this template by making the issue title: **Growth Queue Module - NAME**
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Ask your manager or trainer to add you to the [#support-module-growth_queue](https://gitlab.slack.com/archives/GSW2V1YF3) Slack channel
1. [ ] Commit to this by adding yourself to the [knowledge areas page](https://handbook.gitlab.com/handbook/support/workflows/knowledge_areas/).
1. [ ] Add your name to the [L&R Queue Engineers](https://gitlab.com/gitlab-com/support/license-and-renewals/-/issues/4) issue to inform the L&R team if you are interested in working the L&R queue, or if you are doing the module to increase your knowledge.
1. [ ] Stage 0 Complete!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

___
### Stage 1: Foundations

1. [ ] Learn about the Growth Stage and how it is relevant to this queue. Read, [What do you mean by "Growth" related requests/queue?](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1995#what-do-you-mean-by-growth-related-requestsqueue)
2. [ ] Systems Review
     - [ ] Zendesk [Watch this video](https://drive.google.com/open?id=1Kpijhb65GqnIoSnX81ipjafE5i9GJ4nu)
     - [ ] Salesforce.com [Watch this video](https://drive.google.com/open?id=1j8zJzHIjU8D8QvjNzYVk2QbzD7tEFcgB)
     - [ ] Customers Portal [Watch this video](https://drive.google.com/open?id=1mjcyfGLomg0ToIJTZ0GR7lIq0ybPFafn)
     - [ ] License App [Watch this video](https://drive.google.com/open?id=10cyOTNUd5XNzqyNrsaQRhiNzTHj0ubXd)
3. [ ] Review which issue trackers are used when working the Growth queue. [here](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#issue-trackers)
4. [ ] Stage 1 Complete!

___
### Stage 2: Workflows

1. [ ] Sales lead pass
    - [Read this workflow](https://handbook.gitlab.com/handbook/support/workflows/passing_a_lead_to_sales/)
    - [Watch this video](https://drive.google.com/open?id=1FOuNV1mv0AC5mGBkO5SaTAtoyroJQbqb)
    - [Check-out this example ticket #140841](https://gitlab.zendesk.com/agent/tickets/140841)
1. [ ] GitLab Special Programs
     - [ ] Open Source Software Program
          - [Read this page](https://about.gitlab.com/solutions/open-source/program/)
          - [Read this workflow](https://handbook.gitlab.com/handbook/support/workflows/special-programs/)
          - [Check-out this example ticket #143879](https://gitlab.zendesk.com/agent/tickets/143879)
     - [ ] GitLab for Education
          - [Read this page](https://about.gitlab.com/solutions/education/)
          - [Read this workflow](https://handbook.gitlab.com/handbook/support/workflows/special-programs/)
          - [Check-out this example ticket #143825](https://gitlab.zendesk.com/agent/tickets/143825)
1. [ ] Troubleshooting tickets related to transactions
    - [Read the workflows on this page](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#transactions)
1. [ ] Troubleshooting tickets related to Licensing issues
    - [Read the faqs on this page](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#licensing)
    - [Watch this video](https://drive.google.com/open?id=1wNlNXNMEhMdKmEulhZRbq2EDcwSd7xYp) where we walk through two different types of licensing issues ([ZD#141221](https://gitlab.zendesk.com/agent/tickets/141221) & [ZD#141225](https://gitlab.zendesk.com/agent/tickets/141225) )
    - [Take note of ZD #150955](https://gitlab.zendesk.com/agent/tickets/150955) that relates to an edge case where the Max historical count includes blocked users because of the expiration date on the license.
1. [ ] Troubleshooting tickets related to billing issues
    - [Read the faqs on this page](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#billing)
1. [ ] Troubleshooting Sales Assisted Orders
    - [Read the workflows on this page](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#troubleshooting-sales-assisted-transactions-for-gitlabcom)
1. [ ] Changing a subscription contact in the [customers portal](https://customers.gitlab.com/customers/sign_in)
    - [Read the workflow on this page](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#associating-customer-portal-subscriptions-with-additional-accounts)
1. [ ] Generating a license
    - [Read more](https://handbook.gitlab.com/handbook/support/workflows/license_troubleshooting/#creating-a-license-key)
    - [Watch this video](https://www.youtube.com/watch?v=mYFzL3iweyw) **Be sure you are under the [GitLab Unfiltered](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#livestream-with-zoom) youtube account as this video is private**
1. [ ] Read customer-facing Subscription & Licensing FAQs [here](https://about.gitlab.com/pricing/licensing-faq/)
1. [ ] Read over and bookmark the [L&R Notice Board](https://gitlab.com/gitlab-com/support/license-and-renewals/-/issues/1) where we track bugs, features, and other things of note.
1. [ ] Read over the [L&R project wiki](https://gitlab.com/gitlab-com/support/license-and-renewals/-/wikis/home)
    - [ ] Read over and bookmark the [When to escalate to Sales](https://gitlab.com/gitlab-com/support/license-and-renewals/-/wikis/When-to-escalate-to-Sales) wiki.
1. [ ] Take a look over the [Growth flows](https://app.mural.co/t/gitlab2474/m/gitlab2474/1569500330861/8f9fd73826c42ad809d51be886db27494da91353) to understand the system process. 
1. [ ] Stage 2 Complete! 

___
### Stage 3: Check Your Knowledge

1. [ ] Find `10` **New or Open** tickets in the queue and copy yourself. Follow the interaction and post any questions as internal comments on the ticket.
    1. Ticket #
    2. Ticket #
    3. Ticket #
    4. Ticket #
    5. Ticket #
    6. Ticket #
    7. Ticket #
    8. Ticket #
    9. Ticket #
    10. Ticket #


2. [ ] Find `10` **New or Open** tickets in the queue and answer them via an _internal note_ and copy your trainer.
    1. Ticket #
    2. Ticket #
    3. Ticket #
    4. Ticket #
    5. Ticket #
    6. Ticket #
    7. Ticket #
    8. Ticket #
    9. Ticket #
    10. Ticket #

___
### Stage 4: Iterate

Make **2** improvements. Fix any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list the MRs below.
1. [ ] Update:
2. [ ] Update:

___
### Final Stage: Completion

1. [ ] Let your trainer know you are ready for the Growth Queue Module quiz via a comment in this issue.
2. [ ] Complete the quiz and let your trainer know.
1. [ ] Trainer: provide quiz feedback.
1. [ ] Once complete, add this module to the list of trainings you have completed! ᕦ(  ͡° ͜ʖ ͡°) ᕤ

___

### Recommended Productivity Apps
_Note, you may expense the apps with associated fees_

1. **[Skitch by Evernote](https://evernote.com/products/skitch)** _(free)_ - annotate screenshots with super easy to use pixalation tools
1. **[aText](https://www.trankynam.com/atext/)** _(one-time fee $4.99USD)_ - text expander
1. **[Alfred](https://www.alfredapp.com/)** _(one-time fee $25£)_ - retain clip history up to 3 months (plus much more)
1. **[Loom](https://www.loom.com)** _(free)_ - create short screen capture videos for users, product or documentation
