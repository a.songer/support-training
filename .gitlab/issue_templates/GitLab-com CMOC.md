---
module-name: "GitLab-com CMOC"
area: "Customer Service"
maintainers:
  - tristan
  - faleksic
---

**Goal of this module:** Instruct the taker on the duties and responsibilities of being the [Communications Manager On Call (CMOC)](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) for an active GitLab.com incident. This includes providing a clear understanding of what an incident is, how to work with reliability engineering during one, and how to use the tools at our disposal to effectively communicate updates to incidents both internally and externally to end-users and stakeholders.

Tackle each stage in sequential order, **but first**:

+ [ ] Notify your manager on this issue to let them know them you have started.
+ [ ] Open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to request access to **Status.io**, **PagerDuty** (if you don't already have an account) and the **Social Media Admin** 1Password vault.

>**Note: Ensure that you request access to the `Test Page` along with the production page on Status.io - this is the testing environment and you'll need this to complete Stage 4 of this module. See [this access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8306) for an example.**
>
> If you register your account, then have problems accepting the email invitation: add a Comment to your Access Request Issue requesting a new invitation. It is not possible to register the account twice with the same invitation link.

## Introduction

CMOC is a communication-focused role that focuses on updating Gitlab.com customers about the current state of known Gitlab.com incidents. The CMOC will interface with Engineering during production issues, working with them to characterize the issue and use Status.io to update Gitlab.com social media and our status page with the current incident status.

The CMOC responsibilities are to have a clear knowledge of what is the affectation that happens when a specific incident is triggered so that it can properly communicate it to all the required channels. This is generally achieved by collecting information from the EOC and IM and/or monitoring tools to provide the most accurate update about what happened and how it is being solved.

## **Stage 1:** On-Call basics & expectations

+ [ ] Done with Stage 1

1. [ ] Review [GitLab Support's On-Call Guide](https://handbook.gitlab.com/handbook/support/on-call/), paying special attention to the [expectations](https://handbook.gitlab.com/handbook/support/on-call/#expectations-for-support-on-call) for being on-call.
1. [ ] Open and complete the [On-Call Basics training module](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=On-Call%20Basics).

## Stage 2: Additional Incident Preparation

+ [ ] Done with Stage 2

In addition to the incident preparation from the On-Call basics, as a CMOC, there are some additional resources to monitor for possible incidents.

### Slack Channels

+ [ ] Note that for each new incident, a dedicated slack channel is generated automatically for all communication around that incident. Example: `#incident-3188`.
The channel handle can be found in [#incident-management](https://gitlab.slack.com/archives/CB7P5CJS1) slack channel.

Optionally, consider joining the following channels as well. They aren't necessary to monitor when working through most incidents but they will be useful eventually.

+ [#cloud-provider-alerts](https://gitlab.slack.com/archives/CBL6LVDHT)
+ [#alerts](https://gitlab.slack.com/archives/C12RCNXK5)
+ [#feed_alerts-general](https://gitlab.slack.com/archives/CD6HFD1L0)
+ [#announcements](https://gitlab.slack.com/archives/C8PKBH3M5)
+ [#dev-escalation](https://gitlab.slack.com/archives/CLKLMSUR4)

### GitLab Community Forum

Reports of problems with GitLab.com will primarily come from end-users through Zendesk but they may come from other sources as well.

Bookmark the following and be prepared to check them for reports of issues with GitLab.com:

+ [ ] [GitLab Community Forum](https://forum.gitlab.com/)
+ [ ] [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues)

## **Stage 3:** Managing Incidents

+ [ ] Done with Stage 3

The incident management process normally begins with a PagerDuty page to the CMOC from the EOC (Engineer On Call) when they've detected a problem with GitLab.com severe enough to require us.

However, if we or our users notice an issue that we suspect could turn into an incident we're encouraged to contact the on-call EOC for their opinion.

+ [ ] Read about [how to determine who the on-call EOC is](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#who-is-the-current-eoc).

In more severe cases if we've received enough reports from users of a particular issue with GitLab.com that we feel is indicative of an incident we can page the on-call EOC through PagerDuty.

+ [ ] Read about [how to page to on-call EOC](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#when-to-contact-the-current-eoc).

As a general rule, at least **three** separate reports of the same type of issue within a relatively short timeframe is a strong indicator that GitLab.com may be facing an incident and a page to the EOC is justified. Be sure to include links to Zendesk tickets and any other information to the EOC that they may need to understand the issue clearly and concisely.

+ [ ] Watch the [CMOC training recording](https://drive.google.com/drive/u/0/folders/1JArmPtHxfx6YMIWA4Dz1StqGEU2XllRy) where a member of the site reliability team provides guidance on the role and expectations of being on CMOC PagerDuty.
+ [ ] Read about [finding related tickets for an incident](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#monitor-zendesk).

Once the severity of an ongoing incident has been set and has been posted to our status page, internal stakeholders must be notified by the CMOC.

+ [ ] Read about [notifying stakeholders](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#notify-stakeholders)

### The Status Page - Status.io

Once an incident has [been declared](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#how-incidents-are-declared) the CMOC will be paged via PagerDuty and it'll be up to you to start managing Status.io. Along with this, one of your first tasks should be to join the incident Zoom call so that you can follow along with the EOC and anyone else involved in working the incident.

+ [ ] Read about [how to conduct yourself while in the incident zoom call](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#join-incident-channel--zoom).

Effective communication with production engineering during an incident is crucial as the content of our status updates will largely come from them. Keeping yourself informed on the progress of an incident will allow you to communicate updates quickly and concisely with stakeholders and affected users.

Incidents and maintenance events are managed by the CMOC throughout their entire lifecycle through our [status page](https://status.gitlab.com/), powered by [Status.io](https://status.io). Updates made to incidents and maintenance events through Status.io are automatically tweeted out via [@gitlabstatus](https://twitter.com/gitlabstatus) through the [broadcast feature](https://kb.status.io/notifications/twitter-notifications/) of Status.io.

+ [ ] Learn how to create, update, and resolve incidents in Status.io.
  + [Engage](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#stage-1-engage)
  + [Manage](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#stage-2-manage)
  + [Monitor (Situational)](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#stage-3-monitor-situational)
  + [Resolve](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#stage-4-resolve)
+ [ ] Read about [Frequency of Updates](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#frequency-of-updates) to learn how often you should aim to update Status.io depending on the severity of the incident.
+ [ ] Learn how to [Perform a Handover](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#end-of-shift-handover-procedure) at the end of your on-call shift.

## **Stage 4:** Review

+ [ ] Done with Stage 4

### Review Past Incidents

+ [ ] Review the following past incidents to get an idea of the proper tone to use and what details to include when posting status updates.

#### [S1] Site outage due to CDN connectivity issues

Connectivity issues for GitLab.com caused by CloudFlare outage.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/62b16aa5d28faf12a3973460/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/62b16aa5d28faf12a3973460)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7288)

#### [S2] 500 Error Returned During Project Security Configuration

Receiving 500 error when navigating to Security Configuration on projects caused by an MR which needed to be reverted.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/628d1ea340179d0538c99fd1/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/628d1ea340179d0538c99fd1)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7127)

#### [S3] Repository mirroring delays

Investigating unusually high delays in our repository mirroring feature.

+ [Status.io link](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incident/62a0abafa9ca2b12a3a42f2b/edit)
+ [Status Page link](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/62a0abafa9ca2b12a3a42f2b)
+ [Production Issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7223)

#### CMOC Practice Events

Look in the [Google Shared Drive](https://drive.google.com/drive/folders/1cs1v1dGSf2n0KE0MAfe5vH54zU_JqKJH) for recordings of previous CMOC Practice Events that you can watch to get an understanding of the process.

If you need to review more examples, browse the [incident history](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incidents) section of Status.io.

### Status Page Testing

Our [Status.io](https://status.io) instance gives us access to a testing environment that allows you to create, update, and resolve incidents on our status page in an internal environment.

Once your access request has been fulfilled and you've logged in to Status.io you can access our testing environment by clicking the `GitLab System Status` dropdown box in the top right of the window and selecting `Test Page`. **If you continue without doing this you will be creating an incident on our LIVE status page**.

Test your knowledge by creating an incident, guide it through the entire incident lifecycle, and then provide a link to it. It is safe to use the "Broadcast" feature in the test environment, as it will only broadcast the status to a [protected test Twitter account](https://app.status.io/dashboard/5bedc0c2a394fc04c9ccc974/notifications/twitter) and any [GitLab team member emails configured here](https://app.status.io/dashboard/5bedc0c2a394fc04c9ccc974/notifications/email).

+ [ ] Create, update, change to monitoring, and then resolve one incident on our test status page.
  + Link:

## **Stage 5:** Access & Shadowing

This stage ensures that all of the necessary accounts and access you need to perform CMOC duties are provisioned and that you get a preview of what being a CMOC is like before you officially join the rotation.

### Twitter

While tweets are usually made through our Status.io integration, there are times where a CMOC will need direct access to `@gitlabstatus` to make a tweet. The steps below will allow you to gain access to the account to tweet directly from it.

+ [ ] Log in to the `@gitlabstatus` twitter account using the credentials in the 1Password `Social Media Admin` vault.
  + [ ] If you do not have access to the `Social Media Admin` vault, submit an [individual access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request).

### Shadowing

+ [ ] Shadow one or more incidents **before** you join the CMOC rotation. This gives you opportunities to ask questions of the current CMOC, as well as get a feel of what is happening in the incident room during an active incident.
  * [ ] Fill out the [Super Form](https://support-super-form-gitlab-com-support-support-op-651f22e90ce6d7.gitlab.io/) to have Support-ops add you to the CMOC Shadow rotation for your region/timezone in PagerDuty. Make sure to specify which layer applies to you.

Please be sure to reach out in `#support-team-chat` if you run into issues with any of the steps.

## **Stage 6:** Setting up for success

Jumping into an incident and having everything ready can be a challenge. Consider having the following bookmarks in a folder to find what you need faster and easier:

- [Escalation Policies & Schedules - PagerDuty](https://gitlab.pagerduty.com/escalation_policies)
- [Status.io](https://app.status.io/statuspage/5b36dc6502d06804c08349f7)
- [Servicing Internal Requests](https://handbook.gitlab.com/handbook/support/workflows/internal_requests/#contact-request)
- [Sending Notices](https://handbook.gitlab.com/handbook/support/workflows/sending_notices/)
- [CMOC Workflow](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/)
- [Issue Tracker CMOC Handover](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/-/issues)
- [Oncall Schedules](https://gitlab-com.gitlab.io/support/team/oncall.html)
- [Twitter](https://twitter.com/)
- [GitLab System Status](https://status.gitlab.com/)
- [GitLab.com Status (@gitlabstatus) / Twitter](https://twitter.com/gitlabstatus)
- [Who's IM now](https://gitlab.pagerduty.com/service-directory/PE8A5MX)

Having all of these saved has a lot of uses. Perhaps you aren't sure about the next step that you need to make as a CMOC - the CMOC Workflows helps you find that out.

It is hard to jump in into an incident unprepared or late, having bookmarks helps get up to speed.

+ [ ] (Optional) Review [CMOC-Helper](https://gitlab.com/gitlab-com/support/toolbox/cmoc-helper) tool to support you in the on-call responsibilities. The tool aggregates many helpful resources and helps to find the required information faster using your terminal. Read [the list of available features here](https://gitlab.com/gitlab-com/support/toolbox/cmoc-helper#cmoc-on-call-engineers-helper-tool)

## **Stage 7:** Final Steps

+ [ ] Have your trainer review your practice incidents. If you do not have a trainer, ask another CMOC or your manager to review.
+ [ ] (Optional but highly recommended) Work with your trainer through a [CMOC practice event](https://gitlab.com/gitlab-com/support/support-team-meta/-/blob/master/.gitlab/issue_templates/CMOC%20Practice%20Event.md). Your trainer should act as IMOC, and you are CMOC.
  + [ ] Once finished add that issue as a related issue to this module (`/relate`).
+ [ ] Ping your manager on this issue to let them know that you've completed this module and are ready to be added to the CMOC schedule rotation in PagerDuty.
   + You may expense the [cost of your mobile phone *service*](https://handbook.gitlab.com/handbook/support/on-call/#mobile-phone-service-and-data-reimbursement) for any month that you are performing on-call duties.
+ [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.

### Documenting your CMOC status

**NOTE:** Perform these steps ONLY if you have joined the CMOC rotation in PagerDuty.

+ [ ] Create an MR to declare yourself a **GitLab.com CMOC** on the [team page](https://about.gitlab.com/company/team/) by updating the [your yaml file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person). Detailed instructions can be found [here](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page).

+ [ ] Submit a MR to update your entry in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry).
   + [ ] Add `CMOC` to the list in `modules`:

```yaml
  modules:
    - CMOC
```

   + [ ] Add `'Support Focus: CMOC'` to the list of Zendesk `groups` so you gain access to the [GitLab Incidents](https://gitlab.zendesk.com/agent/filters/360073862640) shared view in Zendesk:

```yaml
  zendesk:
    main:
      id: 1234
      groups:
      - 'Support Focus: CMOC'
```
+ [ ] Open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to have a `Support Staff - CMOC` role in Zendesk

/due in 8 weeks
/label ~on-call-training
/label ~"Module::GitLab-com CMOC"
