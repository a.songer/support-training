---
module-name: "Git and GitLab Basics"
area: "Core Technologies"
maintainers:
  - ralfaro
---

## Introduction

- We use Git and GitLab to build GitLab! Becoming familiar with the core tools is the first step to being an effective Support team member.
- We'll also introduce you to the GitLab product in this module.

**Goals of this checklist**

At the end of the checklist, you will:
- be equipped with the necessary technical baseline knowledge to understand our products and services.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take  **2 days to complete**.

## Stage 0: Git Basics

If you are already comfortable with using Git, and you are able to retain a good amount of information by just watching or reading through, go for it! But if you see a topic that is completely new to you, stop and try it out for yourself before continuing.

1. [ ] Read one or both of the following articles about the uses, design philosophy, and architecture of Git: 
      - [ ] [Getting Started - What is Git?](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)
      - [ ] [Learn the Basics of Git in Under 10 Minutes](https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/) (ignore the tutorial portion, as this will be covered in steps 2 and 4 below).
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git).  
     - If you have questions or want help, don't hesitate to ask your manager or other Support Engineers.
1. [ ] Optional. Bookmark and explore [Git internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain), and go back to it from time to time to learn more about how Git works.
1. [ ] Complete the [GitLab Certified Git Associate Pathway](https://levelup.gitlab.com/learning-paths/certified-git-associate-learning-path) training.

## Stage 1: GitLab Basics

1. [ ] Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab Enterprise Edition (EE)](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab Community Edition (CE)](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md) (also known as GitLab FOSS).
        * **Note:** Gitlab CE is a read-only mirror of GitLab EE with the proprietary software removed. 
1. [ ] Read about [GitLab releases](https://handbook.gitlab.com/handbook/engineering/releases/#overview-and-terminology).
1. [ ] Read about [choosing between Self-managed, GitLab.com (SaaS), and Dedicated](https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed) | (Optional) [Video](https://www.youtube.com/watch?v=7GxO6qvATwU)
1. [ ] Complete the [GitLab Certified Project Management Associate Pathway for GitLab Team Members](https://levelup.gitlab.com/learning-paths/gitlab-project-management-associate-learning-path) training, which focuses on how things are organized in GitLab.


## Stage 2: GitLab Product Stages & Services

1. [ ] Refer to the [GitLab Features](https://about.gitlab.com/features/) page for information on the features of GitLab.
1. [ ] Get familiar with the [different teams in charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages). 
   1. [ ] Bookmark this page. You will refer to it frequently when determining which Engineering team to ask for help on a challenging ticket, and when adding labels to new issues.
1. [ ] Get familiar with the services GitLab offers: 
   1. [ ] Review the [pricing page](https://about.gitlab.com/pricing/) about the Free, Premium, and Ultimate tiers. Note which features are available based on whether a customer chooses GitLab.com (SaaS), Self Managed, or Dedicated.
      * **Note:** Be aware that [this three-tier subscription model was introduced in 2021-01-26](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/), and some customers are still on Bronze/Starter.
   1. [ ] Take a look at [the full feature comparison](https://about.gitlab.com/pricing/feature-comparison/) for different GitLab tiers.

#### Congratulations! You made it, and now have a baseline knowledge of Git, GitLab and its services!

You are now ready to continue on your onboarding path to tackle the next module in line, check our [Support Training](https://handbook.gitlab.com/handbook/support/training/) page or your `New Support Team Member Start Here` issue for details!

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::Git and GitLab Basics"
