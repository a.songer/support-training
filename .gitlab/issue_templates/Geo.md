---
module-name: "Geo"
area: "Product Knowledge"
gitlab-group: "Enablement:Geo"
maintainers:
  - abuerer
---

**Goal of this checklist:** Set a clear path for Geo Expert training

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Geo - "your name"
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] In your Slack Notification Settings, set **Geo** as a **Highlight Word**
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Geo questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: Geo
     level: 1
   ```

## Stage 1: Become familiar with what Geo is

- [ ] **Done with Stage 1**

1. [ ] Read about the initial decisions, implementation and background
   1. [ ] GitLab University (Be aware that we have added a lot of features since those materials and they might not be up to date but are great in providing context)
      1. [ ] [GLU Deck on Geo](https://drive.google.com/open?id=1r9nXLxU9fuAJdEDTi0ZWg7JyU5Ng4hNJPY3jnQyaor8)
      1. [ ] [GLU Recording on Geo](https://drive.google.com/open?id=0BxSd33hPSs-jRGRLUkpuWHF2cjQ)
   1. [ ] Read the [detailed "How we built GitLab Geo" blog post](https://about.gitlab.com/2018/09/14/how-we-built-gitlab-geo/) for details about implementation and phases we went through
   1. [ ] Be aware that we have (_Geo Scheduling Call_, _Geo Monthly Testing Update_) and [Geo Group on YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0KoY_6FXXVgj7wPE9ZDS4cOw). Take note the playlist is sorted by latest video first.
1. [ ] Read through all the [Geo Documentation](https://docs.gitlab.com/ee/administration/geo/)

## Stage 2: Technical Setup

- [ ] **Done with Stage 2**

Remember to contribute to any documentation that needs updating

1. [ ] Standard installations following the [Setup Instructions](https://docs.gitlab.com/ee/administration/geo/setup/two_single_node_sites.html)
   to install Geo on two VMs. If you have a test license, Geo will replicate the license applied on the primary, but you will need to recreate the [test license changes] (https://docs.gitlab.com/omnibus/development/setup.html#use-customers-portal-staging-in-gitlab) to gitlab.rb on your secondary node.
1. [ ] Set up a local repo to push to the primary and pull from the secondary
1. [ ] Set up a local repo to push to the secondary and pull from the secondary
1. [ ] Destroy the primary and promote the secondary node to primary by following
   the [GitLab Geo Disaster Recovery Instructions](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html)
   1. [ ] Connect a local repo to this new primary and make sure you can push and pull
1. [ ] Test HTTP/HTTPS to make sure Geo is working after each step
    If you are using self-signed certificate checkout [mkcert](https://github.com/FiloSottile/mkcert) to make this an easy process.
   1. [ ] Set up [custom / self signed certificates](https://docs.gitlab.com/omnibus/settings/ssl/index.html#configure-https-manually)
   1. [ ] Start with HTTP on both servers and change to the following:
   1. [ ] Primary use HTTP, secondary HTTPS
   1. [ ] Primary use HTTPS, secondary HTTP
   1. [ ] HTTPS on both
   1. [ ] Back to HTTP
1. [ ] [Update your Geo setup](https://docs.gitlab.com/ee/administration/geo/replication/upgrading_the_geo_sites.html) 3 times and make sure at least they are new minor versions.
    Do this throughout the module
   1. [ ] from version __ to version __
   1. [ ] from version __ to version __
   1. [ ] from version __ to version __
1. [ ] [Set up a Docker Registry on a secondary](https://docs.gitlab.com/ee/administration/geo/replication/docker_registry.html) Geo node to mirror the primary one
1. [ ] Set up custom ports for SSH and HTTP (If you haven't done this before)
1. [ ] With and without license
   1. [ ] Remove your license on the primary
   1. [ ] Check what happens (configs,log files, etc.)
   1. [ ] Add your license back to the primary
   1. [ ] Check what happens (configs,log files, etc.)

## Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Find 5 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Answer 10 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Calls where we help a Customer troubleshoot Geo
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___


## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage

1. [ ] Have your trainer review your tickets. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Bookmark [How to ask for support from Geo](https://about.gitlab.com/handbook/engineering/development/enablement/geo/#how-to-ask-for-support-from-geo) to know how to ask Geo team for help when working Geo tickets and emergencies.
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: Geo
     level: 2
   ```

/label ~module
/label ~"Module::Geo"
