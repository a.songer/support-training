---
module-name: "Support Stable Counterparts Basics"
area: "Making Support Better"
maintainers:
  - TBD
---

## Introduction

Welcome to the SSC bootstrapping module - we are glad you want to be a Support Stable Counterpart!

> **As you work through this issue, remember that this issue is [non-confidential and is public](https://handbook.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**Goals of this checklist**

At the end of the checklist, support engineers should be

- Familiar with the purpose of the Support Stable Counterparts initiative.
- Aware of the resources related to this initiative.
- Familiar with the guidelines and best practices around this initiative.
- Signed up as a counterpart to one (or more) teams.
- Interacting with their selected team(s).

**General Timeline and Expectations** 

- Completing the tasks to the end of the quick start checklist should take you **1 day to complete**.

### Why Stable Counterparts?

1. [ ] Read about the [Stable Counterparts initiative in the leadership page](https://about.gitlab.com/handbook/leadership/#stable-counterparts).
1. [ ] Read more about why and how GitLab has adopted this to our workflows through [An ode to stable counterparts](https://about.gitlab.com/blog/2018/10/16/an-ode-to-stable-counterparts/)
1. [ ] Head over to [Support's Stable Counterparts page](https://handbook.gitlab.com/handbook/support/support-stable-counterparts/#expected-outcomes-of-the-support-stable-counterpart-ssc-initiative) to learn about the goals of the initiative in Support, and expectations from the different players.

### Quick Start Checklist

1. [ ] Select an engineering team to be a counterpart for.
    - Look at [the DevOps stages](https://about.gitlab.com/handbook/product/categories/#devops-stages)
    - Within the list of roles for each team should be `Support:`
    - If you're interested in a number of teams, rank higher those with no existing Support counterpart.
    - It can be very helpful to meet with members of the team synchronously. 
      - For example a monthly coffee chat with the engineering and product managers,
        and the team's weekly meeting if they have one.
      - Which timezone members of the team are in is a consideration. This is
        not essential since GitLab is async first, and team composition will change over time.

1. [ ] Create a MR to add your name under 'Support' for the relevant team on [data/stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) and assign it to your manager. Make sure to spell your name exactly as in the [YAML file for your team page entry](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) to ensure it will be linked up correctly.
    1. [ ] If you are struggling to balance SSC and other work, collaborate with your manager to help with time management and prioritization.
1. [ ] Reach out to your manager (or another manager in #spt_managers Slack channel) to get yourself added to the `@gitlab-com/support/support-stable-counterparts` GitLab group as an `Owner` (https://gitlab.com/groups/gitlab-com/support/support-stable-counterparts/-/group_members)
    - This group is used for async communication with all the counterparts.
1. [ ] Create a [single person access request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request) to get yourself added to the `@support-stable-counterparts` Slack user group
1. [ ] Join the `#spt_stable-counterparts` Slack room.
1. [ ] Announce your new role in the #support-team-chat Slack channel. Here is a sample announcement: "Hi folks! Here is some exciting news - I have signed up as a Support Stable Counterpart for _group_ and I am looking forward to enabling both product and us by acting as a bridge and influencing changes. Reach out to me if you have any questions!".
1. [ ] Consider adding yourself as a CC to the [request-for-help](https://gitlab.com/gitlab-com/?filter=request-for-help) issue template for your product group. This ensures you will be notified anytime someone in Support needs to reach out to your group via an issue. You might be able to provide additional context, help your colleague or just benefit from increased awareness yourself.
1. [ ] Check if there is a [Support Pod](https://gitlab.com/gitlab-com/support/support-pods) that is topically aligned with your product group and consider joining it – you're an ideal candidate to benefit from the additional exposure to relevant tickets.
1. [ ] Reach out to your group's product manager and engineering manager sync or async.
    - A coffee chat is preferred, however, reach out via Slack if there are timezone and/or availability conflicts.
    - Follow your own style; consider discussing the following points:
        - What is the role of Support Stable Counterparts.
        - Why are you interested in that group.
        - How does the team organise its work?
          - How can you identify work that the team is planning to work on.
          - How does the team prioritise and triage issues.
        - What sort of insight from our work with customers (in tickets and in calls) might be useful?
    - [ ] Ask for an invitation to any regular sync meetings they have.

### Get to know the engineering team

1. [ ] Establish a regular cadence for communicating with the Product group.
    - If you are going async, we recommend using a Google doc with a template similar to [this one](https://docs.google.com/document/d/1m9t-sxPzwie2D40cXTwpjnQ2VxYJb3GoEB5ig38Nkmw/edit).
    - Depending on the group and the type of meetings they have, choose the one that would make the most sense for you and Support.
    - Get yourself added to the meeting(s) even if you will be unable to join due to timezone conflicts: you can still read the agenda doc later!
1. [ ] To keep yourself up to date on what's happening with the group,
    - [ ] Join their Slack channel(s).
    - [ ] Subscribe to the pertinent trackers/labels to be aware of new issues and MRs.
        - There are labels for each [section (devops::) and group (group::)](https://about.gitlab.com/handbook/product/categories/#hierarchy). Search for, say, [group::](https://gitlab.com/groups/gitlab-org/-/labels?&search=group%3A%3A) and each label is provided with a link to a view of issues, MRs and epics.
          - Be sure to search at the [group level](https://gitlab.com/groups/gitlab-org/-/issues) to pick up all the projects that make up GitLab.
          - Filter by milestone to review what work the team achieved and is planning to achieve.
        - You can [subscribe to labels](https://about.gitlab.com/blog/2016/04/13/feature-highlight-subscribe-to-label/) and set up [email notifications](https://docs.gitlab.com/ee/user/profile/notifications.html).
          - **This can create a lot of noise in your inbox and might be hard to properly filter.**
            - You may need to have another strategy to follow along to the team's work.
            - [The team's triage reports](https://gitlab.com/gitlab-org/quality/triage-reports/-/issues) might be useful.
          - For issues only (not MRs or Epics) RSS is an option. From the issue search results, you can select the RSS feed button to get a custom RSS feed.
          - Alternatively you can subscribe only to the [Planning Issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Planning%20Issue&first_page_size=20) of your group.
        - Be especially aware of severity::1/severity::2 issues in their areas of the product, including workarounds.
    - [ ] Engineering team pages usually have information about their labels, slack channels, boards, and meetings under 'useful links' at the end.
    - [ ] Attend their team sync meetings or set a reminder to read the meeting notes.

### Find out more about the role


1. [ ] Learn from other SSCs about what they do and share tips of the trade:
    - Schedule a pairing or coffee chat with a current SSC.
    - There is a *support stable counterpart office hours* meeting in the support calendar.  It's on Thursdays, just before the EMEA Support team meeting.
1. [ ] If the group you chose already has an SSC, reach out to them to discuss the best way to collaborate and share information.
1. [ ] Watch [this internal video](https://drive.google.com/drive/u/0/search?q=parent:1-Cb1GVf5KfsehPDtCWjz6Q1u1b4xE37D) that discusses how to perform the role.

### Sharing Information

**Sharing Information with Support**

1. [ ] Use the [Support Week in Review - SWIR](https://handbook.gitlab.com/handbook/support/#support-week-in-review) as your primary mode of communication when sharing group related information with the rest of the Support team.
    - Follow it up with communications in relevant Support Slack channels.
1. [ ] A monthly communication cadence is recommended - however, some groups might not have a lot of updates to share with Support every month. In such instances, make a decision based on your own discretion.
1. [ ] Group related updates and announcements can be:
    - New features added in an upcoming release
    - Bug fixes in an upcoming release
    - Breaking changes in the next major release – ideally, inquire early (three months in advance) about your group's plans and try to properly understand the customer impact of any breaking changes well in advance.
    - Issues likely to generate tickets
    - Major documentation changes
    - Discovered bugs and applicable workarounds
    - Any special processes or troubleshooting workflows that might pertain to the features in your group
1. [ ] Share best practices with new and existing SSCs (see the last stage in this issue for access to the below groups):
    1. [ ] Use the `@support-stable-counterparts` Slack user group to discuss best practices and other tips and tricks.
    1. [ ] Use the `@gitlab-com/support/support-stable-counterparts` GitLab group for process change related ideas, feedback and other discussions.
1. [ ] Create and review relevant training materials and run sessions as needed.
1. [ ] Optionally, consider doing quarterly office hours to chat about your group and share your experiences as an SSC with newer team members.

**Sharing Information with Product Teams**

1. [ ] Keep an eye out for interesting tickets affecting the group(s) you are working with.
    - [ ] Think about how to track this information and share it efficiently. Ask other SSCs how they do this.
1. [ ] For sync communication, use the most relevant product team meeting.
    - Be prepared and add your agenda in advance.
1. [ ] For async communication, use the [template](https://docs.google.com/document/d/1m9t-sxPzwie2D40cXTwpjnQ2VxYJb3GoEB5ig38Nkmw/edit) (recommended), or, use Slack.
1. [ ] Loop them in on relevant issues, tickets and Slack threads.
1. [ ] A monthly communication cadence is recommended.
1. [ ] Group related updates and announcements can be:
    - Customer feedback from tickets related to the group
    - Recent trends on tickets related to the group
    - Be the customer's voice and an influencing agent on product related decisions and future roadmap
1. [ ] Optionally, help with questions from non product groups in the Product team's Slack channels if and when possible.

/label ~"Module::Support Stable Counterparts Basics"
