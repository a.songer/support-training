---
module-name: "GitLab Support Basics"
area: "Customer Service"
maintainers:
  - TBD
---

## Introduction

Welcome to the next module in your Support Onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**Goals of this checklist**

At the end of the checklist, new team member should
- Be familiar with how GitLab Support operates
- Be familiar with some common Support Workflows and Processes
- Be familiar with issue creation and escalation

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.

### Stage 0: GitLab Support Basics

1. [ ] Get familiar with our [Customer facing Support](https://about.gitlab.com/support/) page and [Statement of Support](https://about.gitlab.com/support/statement-of-support).
1. [ ] Read and bookmark the [Support Engineer Responsibilities page](https://handbook.gitlab.com/handbook/support/support-engineer-responsibilities/)
1. [ ] Familiarize yourself with the [Support Workflows](https://handbook.gitlab.com/handbook/support/workflows/) handbook page. Bookmark this page and use it as a reference, rather than trying to read through all the workflows right now.
1. [ ] See how our Support Engineers handle tickets by reading and understanding the [Working on Tickets](https://handbook.gitlab.com/handbook/support/workflows/working-on-tickets/) workflow.
1. [ ] Do the [GitLab Data Classification Standard](https://levelup.gitlab.com/courses/gitlab-data-classification-training) training pathway to learn how to identify and handle sensitive data, which we handle on a daily basis.
1. [ ] Learn about the [roles our Support Engineers rotate in](https://handbook.gitlab.com/handbook/support/support-engineer-responsibilities/#support-engineer-areas-of-focus), so that people know what to do and have variation in their work.
1. [ ] Read about the [Support Channels](https://handbook.gitlab.com/handbook/support/channels/) and how customers on paid plan vs free plan receive Support.
   - Here you will find tickets from both paid customers and free/trial users.
   - Tickets are extremely varied.
   - You should be prepared for basic tickets given the knowledge gained from the previous steps of your training.
   - Tickets in Zendesk contain a wealth of knowledge from past interactions. Often times, the issue has occurred at least once and a solution has been provided, or an issue created on the customer's behalf. Reading through past tickets can aide in the learning process, getting you up to speed quicker.
1. [ ] Review the [gitlab.io Support Team page](https://gitlab-com.gitlab.io/support/team/), which gives up-to-date information on team members and schedules.
1. [ ] Review your entry in your [support team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) file and check your information there. (Your entry was created as part of your Support baseline entitlements AR.) If you would like to add interests, hobbies, etc. then create an MR to update the file, and assign to your manager to review and merge.
1. [ ] Create an MR to add yourself to the "Ticket Pairing - Team Tracking" [issue template](https://gitlab.com/gitlab-com/support/support-pairing/-/blob/master/.gitlab/issue_templates/Ticket%20Pairing%20-%20Team%20Tracking.md). Please include a colon and a space after your name, for example "Firstname Lastname: ", also make sure that the list stays in alphabetical order.
1. [ ] Familiarize yourself with the [Support Team Meta project](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues), where all internal discussions and improvement proposals take place in the form of issues. Make sure to [Star the project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#star-a-project).
   1. [ ] Optional. [Set your notification level](https://docs.gitlab.com/ee/user/profile/notifications.html#change-level-of-project-notifications) to "Custom" with "New issue" turned on, so you always receive emails about new issues. **Note**: To be notified of further updates for a specific issue, you need to enable Notifications for that issue.
1. [ ] Learn about the Support Week in Review: A weekly digest with important announcements and relevant information for all Support team members.
   - [ ] Visit the [SWIR project's label settings](https://gitlab.com/gitlab-com/support/readiness/support-week-in-review/-/labels?subscribed=&search=digest) and [subscribe](https://docs.gitlab.com/ee/user/project/labels.html#receive-notifications-when-a-label-is-used) to the `SWIR::Digest` label. That way you'll get an email with each new digest every week.
   - [ ] Bookmark the [SWIR form](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/SWIR/) – this is where you can add items yourself, but will also find all relevant SWIR-related links in the header bar.

### Stage 1: Creating issues and Fielding Feature Proposals

1. [ ] Understand what's in the pipeline at GitLab as well as proposed features: [Direction Page](https://about.gitlab.com/direction/).
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/.gitlab/issue_templates) to see what is expected (look at the comments in the source markdown for more details.). Raise issues for bugs in a manner that would make the issue easily reproducible. A Developer or a contributor may work on your issue. Also note that some [labels](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/contributing/issue_workflow.md#labels) are already included in the template.
1. [ ] Read the [How to Engage with Product Management](https://about.gitlab.com/handbook/product/how-to-engage/) handbook. Pay attention to the [Feedback template](https://about.gitlab.com/handbook/product/how-to-engage/#feedback-template). Use this template every time you add a comment to an issue.
1. [ ] If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk.
1. [ ] Add [customer labels](https://handbook.gitlab.com/handbook/support/workflows/working-with-issues/#adding-labels) for those issues relevant to our subscribers.
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/groups/gitlab-org/-/labels) to find existing feature proposals and bugs.

### Stage 2: Issue Escalation Process

Some tickets need specific knowledge or a deep understanding of a particular component and will need to be escalated to a Senior Support Engineer or a Developer.

1. [ ] Read about [creating and escalating issues](https://handbook.gitlab.com/handbook/support/workflows/working-with-issues/) including how issues are prioritized.
1. [ ] Take a look at the [Team page](https://about.gitlab.com/team/) to find the resident experts in their fields and the [knowledge areas page](https://handbook.gitlab.com/handbook/support/workflows/knowledge_areas/) for further topics where team members are keen on helping with specific areas.
1. [ ] Add yourself to the [knowledge areas page](https://handbook.gitlab.com/handbook/support/workflows/knowledge_areas/) for topics you may know already from previous experience.

_Note: If you're interested in making a contribution to the code, please see the [GitLab development guidelines](https://docs.gitlab.com/ee/development/)._

### Stage 3: Resources for Getting Help

1. [ ] To ask for help, post in the appropriate Slack channel and add a link as an internal note in the ticket, if applicable.
    - #questions for general GitLab questions
    - #support_gitlab-com or #support_self-managed for ticket (or issue) specific questions
    - Appropriate #s_ (stage) or #g_ (group) channel. Broken up according to the [product categories page](https://about.gitlab.com/handbook/product/categories/#devops-stages).
1. [ ] When you get no response or need someone more urgently, you can ping the appropriate [support counterpart](https://handbook.gitlab.com/handbook/support/#support-stable-counterpart) or someone with [knowledge in the area](https://handbook.gitlab.com/handbook/support/workflows/knowledge_areas/).
1. [ ] If someone asks you for help, whether directed at the team or you individually, consider these [Slack reactions tips](https://handbook.gitlab.com/handbook/support/workflows/communication-tips/#slack-reactions).

### Stage 4: Dealing with Imposter Syndrome thoughts whilst working remotely at GitLab

We appreciate that for some new hires working remotely at GitLab may appear initially challenging and as a consequence of this some employees may experience feelings of imposter syndrome. As such we we would encourage you to:

1. [ ] Watch the following video where [support managers discuss feelings of imposter syndrome whilst working at GitLab](https://drive.google.com/file/d/1KVtni9ZNbyL2CmL9ScD3hm2v1wZ8NM3z/view)
1. [ ] Watch the L&D team host a discussion with Sid (GitLab CEO) in GitLab Unfiltered on [mental health and addressing imposter syndrome](https://youtu.be/od_KdZqc69k?t=1784) (the relevant discussion starts at 29:44 and ends at 37:43)
1. [ ] Read the following blog on [how to tackle impostor syndrome while working remotely](https://about.gitlab.com/blog/2020/09/02/imposter-syndrome-and-remote-work/)
1. [ ] Check in with your manager at the 3 months and at 6 month interval to discuss any thoughts or concerns you may have regarding feelings of imposter syndrome

## Stage 5: Adapting to GitLab's work culture

We recognize that for many new team members, GitLab's work culture is significantly different from their former work organizations.
In addition to what's in the company onboarding, Support records its discussions on company values and operating principles in a few ways.
We encourage you to review [the values page](https://handbook.gitlab.com/handbook/values/) and for any operating principle where you may be unsure how they're applied,
see if there's an existing discussion in one of the following places:

1. [ ] [Operating principle of the month](https://gitlab.com/groups/gitlab-com/support/-/epics/213) discussion issues.

## Congratulations! You made it, and now have a basic understanding of how GitLab Support operates and the most common workflows we have!

You are now ready to continue on your onboarding path to tackle the next module in line. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [training page](https://handbook.gitlab.com/handbook/support/training/) or your `New Team Member Start Here` issue to see the list of all modules under your Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::GitLab Support Basics"
