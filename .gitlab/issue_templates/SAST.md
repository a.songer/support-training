---
module-name: "SAST"
area: "Product Knowledge"
gitlab-group: "Sec:Secure"
maintainers:
  - [bcarranza]
  - [kategrechishkina]
  - [lyle]
  - [cmutua]
---

### Overview

**Goal**: 

**Objectives**: 

**Pre-requisites**:
 - Understanding of how to use GitLab to create and modify project
 - Understanding of GitLab CI, including modifying and creating `.gitlab-ci.yml` files and variables
 - Understanding of Docker fundamentals

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: SAST - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: SAST
     level: 2
   ```

### Stage 1: SAST? Huh??

#### What is it?

Static Application Security Testing (SAST) is a pre-deploy scan of customer code using opensource scanners that look for any vulnerabilities present.

#### What customer problem does it solve?

Customers want to avoid deploying code with known vulnerabilities. By using SAST customers can have an understanding of what _known_ vulnerabilities their code may contain.

While customers might build their own custom scanning based on the same open source tools, GitLab allows customers to:
- Scan a large number of languages with a single `include` in their `.gitlab-ci.yml`
- Normalize output across tools
- Display output to developers directly to allow them to fix vulnerabilities _before_ code even reaches the approval stage
- Set custom rules within GitLab that conform to their requirements
- Reporting within projects and groups to show which vulnerabilities may be present in a codebase


##### Scan a large number of languages
The opensource SAST scanners included in GitLab SAST scan a large number of common frameworks and languages. Even if a codebase contains multiple languages or frameworks, GitLab SAST will detect and run the appropriate scanners.

##### Normalize output
The various opensource tools we use for our analyzers means that output may be in a variety of formats: JSON, CSV or others. Post-scan, we process these files into a single JSON report that gets exposed as an artifact. GitLab uses this standard format within the interface to display vulnerability data.


### Stage 2: How does these scanners work?

Briefly:
1. GitLab's custom Docker image, loaded with scanning tools is started by a GitLab Runner with customer code cloned in.
1. Any requirements not included in the base image are installed (as required by the customer)
1. SAST scan runs the appropriate scanners based on what's included in the customer code
1. Reports are collated and produced as an artifact of the pipeline

#### Exercise 2.1: Experiment with SAST using a very empty project.

1. Create a new blank project and initialize it with a README and SAST on project creation.
   - If you forget to click the "Enable Static Application Security Testing (SAST)" checkbox, that's okay! Add the following in a `.gitlab-ci.yml` (or start over!)
   ```
   stages:
   - test
   sast:
      stage: test
   include:
   - template: Security/SAST.gitlab-ci.yml
   ```
1. Attempt to run a pipeline manually.
  -  Did a pipeline run?
1. Add a new file `test.rb`, the contents are not important at this stage. A simple `puts "Hello"` is totally fine! 
1. Create a new MR
    - Did a pipeline run this time? Why?
    - What (if any) jobs ran? Why that set of jobs?
1. Read through the `rules` section of the [SAST template](https://gitlab.com/gitlab-org/gitlab/-/blob/22767151d26ca8c9bbb2fc68a3f158c534657278/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml#L46-61) for the job that _should_ have run.
  - Note, the link goes to a specific commit, so you may not be viewing the most modern version of the SAST template.

##### Challenge 2.1

Now that you've seen how the `SAST` template is included and how it detects which scanners to run, try and:
 1. [ ] Get the `flawfinder-sast` job to run
 1. [ ] Stop the `brakeman-sast` job from running without removing anything from the codebase

#### Exercise 2.2: Experiment locally with a SAST analyzer

1. Clone https://github.com/OWASP/railsgoat, this is a Rails project with built in vulnerabilities!
1. `pull` the `brakeman` analyzer locally by running `docker pull registry.gitlab.com/security-products/brakeman:4`
   - NB: You may need to log in to registry.gitlab.com using a personal access token.
1. Mount the `railsgoat` repository into the container and enter an interactive session of `sh`. For example: 
   - `docker run -v railsgoat:/builds -it registry.gitlab.com/security-products/brakeman:4 /bin/sh`
   - NB: You may need to use the fullpath from root to the cloned railsgoat project. eg `docker run -v /Users/charlmarais/projects/SAST-goat/railsgoat:/builds -it registry.gitlab.com/security-products/brakeman:4 /bin/sh`
1. Run the scanner by running `/analyzer run`
   - Observe the generated `/gl-sast-report.json` in the root directory of the container.
   - NB: in the example above the mount point was /build and you may need to specify the directory to scan using the --target-dir parameter. eg `/analyzer run --target-dir /builds`. If you specify a directory the `gl-sast-report.json` will be in the root of the scanned directory. eg: `/builds/gl-sast-report`.json
1. Run in debug mode by setting the `SECURE_LOG_LEVEL` to `debug`. For example:
   - `SECURE_LOG_LEVEL="debug" /analyzer run`
   - Observe the original output of `brakeman` at `/tmp/brakeman.json`

Read more in the [Analyzers README](https://gitlab.com/gitlab-org/security-products/analyzers/common#how-to-use-the-analyzers)

###### Challenge 2.2

Now that you've seen how the analzyer runs, use your knowledge to put the `railsgoat` project on to GitLab and integrate SAST scanning. In the next stage, we'll observe how GitLab uses this vulnerability information in the web interface.


#### Check your understanding
 - How can you determine the path of a Docker container for a given scanner?
 - What are some ways for determining why a particular scanner did not run?
 


### Stage 3: Using SAST in GitLab

If you haven't completed **Challenge 2.2** yet, go back! We'll rely on having the `railsgoat` project set up to get scanned by SAST.

#### Working with Vulnerabilities in GitLab

1. Read through the [documentation on Vulnerability Reports](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)
1. Go to the Vulnerability Report in your `railsgoat` project - compare the results there to the `gl-sast-report.json` (or `brakeman.json` that you generated previously)

##### Exercise 3.1

In this exercise we're going to go through a typical flow for how a detected vulnerability might be managed. The general process we'll follow:
- Go through the list of vulnerabilities marked as "in triage" and move it to "confirmed" 
- Use the GitLab interface to create an issue to fix it
- Create an MR to fix the vulnerability, and verify in the MR window that it is no longer detected.
- Merge the MR (and automatically close the issue)
- Go back to the Vulnerability Report and note the activity and confirm the fix by changing the status to "Resolved".


**Confirm a vulnerability and create an issue**
1. Select a vulnerability to fix - `Session cookies should be set to HTTP only`, for example.
1. From the Vulnerability Report, select the vulnerability and then change the status to "Confirm"
1. Click on the vulnerability and "Create issue"

**Create an MR to fix the vulnerability**
1. Update the vulnerable code in a new MR. 
1. After a successful pipeline run, expand the "Security Scanning" section and note the FIXED message.
1. Add a section in the MR description to close the raised issue.
   - NB: You can [relate issue and automatically close issue using patterns](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
1. Merge the MR

**Follow-up in the Vulnerability Report**
1. Navigate back to the Vulnerability Report
2. On your chosen vulnerability, notice the linked issues and remediation icon.
3. Mark your vulnerability as "Resolved"

##### Exercise 3.2

In this exercise we're going to add [Scan Result Security Policy](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html) so that if a vulnerability is detected in an MR, it must go through an approval process before it can be be merged to the target branch.

1. Start by creating/inviting two additional users to your project.
  - If you're following this on GitLab.com and need to invite some users to the project, you can choose to invite your onboarding buddy, your manager, or even your Auditor/Admin accounts. 
1. Follow the [Set up a scan result policy tutorial](https://docs.gitlab.com/ee/tutorials/scan_result_policy/)

###### Challenge 3.2

 1. [ ] Update the scan result policy so that only 1 approver is required

#### Check your understanding

 - Where is the security policy located after creating it?

### Stage 4: Advanced Configuration and Topics

#### Custom Rulesets

Out of the box, GitLab scanners may contain many false positives or alert on items that need no remediation. A customer may want to disable these. Alternatively, they may have a specific set of vulnerabilities that they always want to check for and scan for in every project across the company. Read more about [Custom Rulesets](https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html)

##### Challenge 4.1 (Optional)

Using the `railsgoat` repository from earlier, create a custom ruleset that will suppress one or more vulnerabilities reported.

#### Custom Analyzers

More advanced customers may want to go beyond customizing the rules in the built-in scanners, and add their own custom scanners. This is very possible: as long it produces an artifact `gl-sast-report.json` in the appropriate format, custom scanners can be used to populate the Vulnerability Report in GitLab.

[Read more about custom analyzers](https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html#custom-analyzers)

##### Challenge 4.2 (Optional)

Write a simple pipeline to simulate a "custom analyzer" that exposes a `gl-sast-report.json` that _always_ reports a vulnerability with a funny name.
Integrate it into the SAST pipeline on one of your test projects.

#### Security Report Schemas

GitLab maintains a repository of schemas for all security reports at https://gitlab.com/gitlab-org/security-products/security-report-schemas. These schemas are important, because as of GitLab 15.0 security report artifacts are validated against these schemas to prevent the ingestion of broken vulnerability data into the database.

See: https://docs.gitlab.com/ee/user/application_security/#security-report-validation

#### Running SAST in an offline environment

Customers running GitLab in an air-gapped environment will need some special preparation to be able to run Secure products offline.

Review:
 - [Running SAST in an offline environment](https://docs.gitlab.com/ee/user/application_security/sast/#running-sast-in-an-offline-environment)
 - [Loading Docker images onto your offline host](https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html#loading-docker-images-onto-your-offline-host)

 ##### Challenge 4.3 (Optional)

 Configure an offline GitLab environment and successfully run a SAST scan.

### Stage 5: Troubleshooting

   Troubleshooting is often the harder part of SAST tickets, as sometimes getting scanners to work for specific languages or frameworks sometimes requires specific knowledge _of_ that language or framework. As a result, the troubleshooting tips here will be somewhat generalized - but should help you get through the basics before moving onto a [Request for Help](https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-use-gitlabcom-to-formally-request-help-from-the-gitlab-development-team).

- Start by verifying that what the customer is reporting is not a bug by setting up a reproduction environment. The [SAST (Static Application Security Testing) Demo Project](https://gitlab.com/gitlab-com/support/test-projects/ci-examples/sast) will be useful here.
- Have the customer enable more logging output by setting `SECURE_LOG_LEVEL` to `debug`
- Have the customer run the scan offline and locally as described in Exercise 2.2.
    - You can get the command by running a regular scan with SECURE_LOG_LEVEL set to debug. You'll see the command for the analyzer in the job output.
    - Look at the documentation for the analyzer and have them run the command manually, possibly adding or removing some flags could help reveal better error logging for troubleshooting.
    - This will _also_ help verify that the issue they're encountering is not related to a runner configuration issue.
- If you need to get the raw output of the `gl-sast-report.json` [you may not be able to fetch it through the API](https://gitlab.com/gitlab-org/gitlab/-/issues/352644), but you _can_ use a workaround to expose it as a job artifact:
   ```
   artifacts:
     reports:
        sast: gl-sast-report.json
     paths:
        - gl-sast-report.json
   ```

#### `Error parsing security reports`

This error typically comes from a mismatch between report schemas. Often, this comes from customers pinning an analyzer to an older version (and subsequently upgrading GitLab). In SAST, watch out for the `SAST_ANALYZER_IMAGE_TAG` as described in https://docs.gitlab.com/ee/user/application_security/sast/#pinning-to-minor-image-version

Get a copy of their generated `gl-sast-report.json` and review [schema_validator.rb](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/parsers/security/validators/schema_validator.rb) for their version of GitLab to compare valid report schemas. 

### Penultimate Stage: Review

### Final Stage: Completion

1. [ ] Have your trainer review this issue. If you do not have a trainer, ask a support code contributor to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ]  Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).
1. [ ] Consider also taking the [Documentation training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Documentation.md) which focuses on GitLab docs changes.

### Quick References
 - [Analyzers Common Library README](https://gitlab.com/gitlab-org/security-products/analyzers/common#how-to-use-the-analyzers)
 - [SAST (Static Application Security Testing) Demo Project](https://gitlab.com/gitlab-com/support/test-projects/ci-examples/sast)
 - [Secure Pod](https://gitlab.com/gitlab-com/support/support-pods/-/blob/main/Secure/README.md)
    - [`#spt_pod_secure`](https://gitlab.slack.com/archives/C03FV8G5LV7) in Slack
 - [Sec Section Request for Help Project](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues)
 - [Past SAST tickets](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=SAST)

/label ~module
/label ~"Module::SAST"
/assign me
