---
module-name: "Strace Part 1"
area: "Troubleshooting & Diagnostics"
gitlab-group: ""
maintainers:
  - rvzon
---

**Goal:** By the end of this module you will be competent at using strace for basic performance troubleshooting

### Stage 1: Commit and Become familiar with Strace

- [ ] **Done with Stage 1**

1. [ ] Notify your manager on the issue to let them know them you have started.
1. [ ] Commit to this by notifying any Support Engineers with advanced strace skills that you are learning Strace.
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: strace
     level: 1
   ```

### Stage 2: What is Strace

- [ ] **Done with Stage 2**

1. [ ] Start with reading [Spying on your programs with strace](https://jvns.ca/strace-zine-v3.pdf).
1. [ ] Read through - [Linux Strace Command Tutorial for Beginners (8 Examples)](https://www.howtoforge.com/linux-strace-command/).
1. [ ] Learn about - [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser).
1. [ ] Watch this video! [Using Strace to Understand GitLab - Intro / ls](https://www.youtube.com/watch?v=fcY5pPUq35U&list=PL05JrBw4t0KoC7cIkoAFcRhr4gsVesekg&index=1).
   1. [ ] Share a few key takeaways from the video in the comment section.

### Stage 3: Challenge

- [ ] **Done with Stage 3**

Run `strace` on `ls`.

Find the following and share them through a screenshot in the comment section.
 - [ ] The execution command of `ls`.
 - [ ] Opening of a lib file.
 - [ ] The file same lib file being loaded into memory.
 - [ ] Closing of file lib file.

1. [ ] Ask a Support Engineer with competent or above Strace skills look at the results.

### Stage 4: Strace Git

- [ ] **Done with Stage 4**

1. [ ] Watch this video: [using Strace to Understand GitLab -  Git](https://www.youtube.com/watch?v=tThs8QeP2qY)

### Stage 5: Challenge

- [ ] **Done with Stage 5**

Run `strace` for  `git commit -a -m ‘initial message’` on a new repository.

Find the following actions and share them through a screenshot in the comment section.
 - [ ] The change from temp file to hash file.
 - [ ] The index file being replaced to include the new hash.
 - [ ] Show the exclusive lock of the master file.
 - [ ] The child process executing a command.

1. [ ] Ask a Support Engineer with competent or above Strace skills look at the results.

### Final Stage

1. [ ] Schedule a call with your manager (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).
   
   ```
   knowledge_areas:
   - name: strace
     level: 1
   ```

/label ~"Module::Competent Strace module"
