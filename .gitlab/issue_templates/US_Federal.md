---
module-name: "US Government Support"
area: "Customer Service"
maintainers:
  - JamesRLopes
---

### Overview

**Goal**: You feel comfortable with the constraints one must work in to support US Government customers

**Note**: US Federal Support was renamed US Government Support to more accurately describe the kinds of customers we support. You may still see "Federal" while we transition.

*Length*: 2-3 weeks (estimated)

**Objectives**: At the end of this module, you should be able to:

- Log into the [US Government Support Zendesk instance](https://gitlab-federal-support.zendesk.com/agent/dashboard).
- Understand and follow the data confidentiality policies.
- Understand the functional constraints under which our customers work with us.
- Feel comfortable working with end users to resolve problems in the US Government Support Instance

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: US Government Support Training - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 1: Introduction

1. [ ] Read the GitLab Support Handbook page on [Working US Government Tickets](https://handbook.gitlab.com/handbook/support/workflows/usgovernment_tickets/).
1. [ ] Read what [data confidentiality policies we promise to our US Government customers](todo)
1. [ ] If an account wasn't provisioned for you during onboarding you can open [an access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to get access to the [US Government Support Zendesk instance](https://gitlab-federal-support.zendesk.com/agent/dashboard).
1. [ ] Ask a member of the [#spt_us-government](https://gitlab.slack.com/archives/C03RTN3JEJ2) channel to add you as a member to the channel if you haven't already joined.
1. [ ] Join the weekly GitLab US Government Support Team Call or review the current week's items in the [agenda](https://docs.google.com/document/d/1ypFHpm3McUNcUVCgZY1aZMlCOIP1jaEQp8oYUO8G36U/edit).


#### Where to ask for help

If at any time, you have questions regarding a case or issue, please reach out in the following channels. When doing so you should be mindful not to share specifics that could be considered customer confidential information:

- [#spt_us-government](https://gitlab.slack.com/archives/C03RTN3JEJ2)
- [#support_self-managed](https://gitlab.slack.com/archives/C4Y5DRKLK) 


### Stage 2: Pairings!

In order to better understand how engineers and users interact within the US Federal support environment you should schedule some pairing sessions with existing US Federal engineers. Below are a few suggestions for different topics to pair on. Once you feel comfortable with each topic you should do at least one reverse pairing where you lead and an engineer shadows you to provide feedback.

Ticket pairings:
1. [ ] Case # ____ | Engineer: ________
1. [ ] Case # ____ | Engineer: ________
1. [ ] Case # ____ | Engineer: ________
1. [ ] Case # ____ | Engineer: ________
1. [ ] Case # ____ | Engineer: ________

Customer calls:
1. [ ] Case # ____ | Engineer: ________
1. [ ] Case # ____ | Engineer: ________

Live Upgrade assistance:
1. [ ] Case # ____ | Engineer: ________
1. [ ] Case # ____ | Engineer: ________

### Final Stage: Completion

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.

### Congratulations on completing the US Government Support Engineer module. You are now equipped to work cases from US Public Sector users!

/label ~module
/assign me
/label ~"Module::US Government"
