---
module-name: "Customer Service Skills"
area: "Customer Service"
maintainers:
  - mdunninger
level: Beginner
---

## Introduction

Welcome to the next module in your Support Onboarding pathway!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**Goals of this checklist**

At the end of the checklist you will be able to:
- understand how we interact with customers
- utilize your customer service skills to ensure customer success

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/#support-onboarding-pathway), the page also shows you the different modules you'll need to complete as part of your Onboarding.
- The training in this issue should take you **2 days to complete** with a further 2-4 weeks to complete the assessment.
- Note that this module was introduced and presented to SEs who had already been at GitLab for some time. As such, if you see this in your training and you have yet to handle a ticket or encounter a situation where a customer wants a meeting, for example, please use examples from your past work experience. Alternatively, please imagine what you might want to say in the described situation and based on this training.

## Customer Service, Customer Communications and Ticket Management at GitLab Support

### Related Handbook Pages

1. [ ] Read the `We care for our customers` section on the [support team's home page](https://handbook.gitlab.com/handbook/support/#we-care-for-our-customers) in the handbook to understand what we are responsible for.
1. [ ] Read [how to respond to tickets](https://handbook.gitlab.com/handbook/support/workflows/how-to-respond-to-tickets/) on tips and tricks to humanize your responses when talking to customers.
1. [ ] Read about how to handle [feedback and complaints](https://handbook.gitlab.com/handbook/support/workflows/feedbacks_and_complaints/) from customers.

### Course Introduction

- [ ] Please read the [Introduction to Communications Training document](https://docs.google.com/document/d/1Ft8A91sc8AA2A-7KkdfOrOeoUO5xUnsGDZ0bZoKBntw/edit?usp=sharing) - it's only one page - to get a little flavor of what this is about.

### Supplemental Course Materials

- [Ticket Management - Quick Reference Guide](https://docs.google.com/document/d/1O9ZzUCJxnuE42ssJ-pir7QK16Vucja6pKk321LBzyyo)
  : A very handy one-page document with key ideas and tips from all six modules of the course
- Presentations - The original Google Slides presentations for all six modules, for your convenience
  - [01 Connect](https://docs.google.com/presentation/d/1I78r2pmF_GEyTOf3UuAXPaYObdp3SBPoLW1lHS3VX4M)
  - [02 Clarify](https://docs.google.com/presentation/d/1LkE2f6bvuFuPVMAnPm1p49JhM4RrGCnQRf9hDkcDoIU)
  - [03 Create Positive Outcomes](https://docs.google.com/presentation/d/1rimVg34grAbQdwnE8o7yxCKj69mlyrwLVjVIRdgVLtc)
  - [04 Own the Interaction](https://docs.google.com/presentation/d/1MhmcqL5QtZRPaY1lgbpSwE683R5whwTzgHSWW7UJLkM)
  - [05 Educate](https://docs.google.com/presentation/d/1Y3PK5GgA6GZoWonMzRBLd9QS0bHpJsgEg_V9NFCewvQ)
  - [06 Close the Loop](https://docs.google.com/presentation/d/1StXRwYh8Uzjor0IeZdXncUq-3kQ8AyP_Pq7s7BjuJ8I)
- Google's IT Support Customer Service Course: [How to Deal with Difficult Situations](https://www.coursera.org/lecture/technical-support-fundamentals/how-to-deal-with-difficult-situations-yWuOR). This short (~10 mins) video provides some great tips on how to handle yourself and difficult customers.

### How to Complete the Course

This is the async version of the Ticket Management and Communications course
that Support Management originally presented as four, one-hour, live,
interactive class sessions.

There are multiple paths you can take through this course. You should work with
your manager to determine the best path for you based on your customer support
experience and skills, and your communication skills. Regardless of the path
you choose, successful completion of the course requires that your manager
agrees that you've met all the criteria in the [assessment](#assessment).

#### Recommended Paths

You and your manager might want to choose one of these recommended paths for
you to follow, or might prefer to create your own path.

##### Traditional Path

- This path gives you the opportunity to experience and learn from the full
  course. Depending on your experience, you may find some of the concepts to be
  new and you may find some to be familiar. The idea is to learn the new ones,
  and strengthen your grasp on the familiar ones.
- We've created this async version from the recorded live sessions to allow you
  to hear the full discussions and in so doing hopefully to gain something more
  than you would by simply reading through the materials or by listening to the
  presentations without discussions.
- **The Path:**
  1. Complete all six modules, in order, from start to finish.
  1. Complete the exercises in their listed order as you go through the course.
  1. Proceed to the [assessment](#assessment)
  1. If your manager identifies any gaps in your demonstrated skills,
     agree on a plan for improving those skills and re-assessing

##### Non-Video (mostly) Path

- This path gives you an alternative to watching videos, which can be a
  difficult means of learning for some people.
- **The Path:**
  1. Read through the [presentations](#supplemental-course-materials),
     watching only those videos that you feel might help you to understand
     specific topics better. Complete the exercises as you go as well.
  1. Proceed to the [assessment](#assessment)
  1. If your manager identifies any gaps in your demonstrated skills,
     agree on a plan for improving those skills and re-assessing

##### Pre-Test Path

- This path allows you to skip some or all of the training based on your ability
  to demonstrate what skills and knowledge you already have and consistently
  use in working on tickets.
- **The Path:**
  1. (Optional) Review the course content through the
  [supplemental materials](#supplemental-course-materials).
  1. Proceed to the [assessment](#assessment)
  1. If your manager identifies any gaps in your demonstrated skills, then:
     1. work through the associated modules -- watch the videos and complete
        the exercises
     1. take a week or so to practice what you've learned before asking your
        manager to do the assessment with you again

### The Full Course

Unless otherwise indicated, each video below can be found on YouTube in the
[Support Training - Ticket Management & Communications](https://youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
playlist. They're listed in order by module number, video number and title. For
example, the `01 Introduction` video on module 1 can be found in the playlist
under `1 01 Introduction`.

Note: Currently, the videos are not captioned. If you need captions please reach out to your manager and make use of automatic captioning in the meantime.

_Total video content: 4h 59m 48s_

<details>
  <summary>Module 01: Connect</summary>

### Module 1: Connect

_Total video content in Module 1: 67m 38s_
- [ ] Video 01 Introduction (3:01)
- [ ] Video 02 Practical Applications (2:50)
- [ ] Video 03 Modules Introduction (0:46)
- [ ] Video 04 Connect Module Introduction (1:01)
- [ ] Video 05 Build Rapport - Introduction (3:26)
- [ ] Video 06 What Is Rapport (7:00)
- [ ] Video 07 Rapport and the Hot Queue (4:00)
- [ ] Video 08 Written Greeting (7:22)
- [ ] Video 09 Customer Needs - What and How (4:10)
  - [ ] **Exercise 1.09: Complete after viewing the video**
    - Think about two different experiences in which **you** were the
       customer - one that was good and one that was not. In a comment in this
       issue with a title `1.09 What And How`:
       1. Describe the "what" and "how" for each of them
       1. For the good experience, note whether both the "what" and the "how"
          were present
       1. For the bad experience, note what was missing - the "what", the "how"
          or both 
        **_(Remember that this issue is [non-confidential and is public ](https://about.gitlab.com/handbook/values/#transparency)- do not specify the company or customer name in your example)_**
       1. Note the impact each experience had on you as a customer of that business
- [ ] Video 10 Customer Perspective Intro (0:38)
  - [ ] **Exercise 1.10: Complete after viewing the video**
    - In a comment in this issue with a title `1.10 Customer Perspective`,
      provide your thoughts on these questions:
       1. What are the results - the "what" - that our customers want?
       1. "How" do they want those results delivered?
- [ ] Video 11 Core Human Needs Intro (2:19)
- [ ] Video 12 Triggering Fears (0:58)
  - [ ] **Exercise 1.12: Complete after viewing the video**
    - In a comment in this issue with a title `1.12 Triggering Fears`,
      1. Describe some things you have observed or done that played to a
         customer's fears instead of supporting their core human needs.
      1. Identify which one or more of the core fears were triggered.
- [ ] Video 13 Supporting Core Needs (0:50)
  - [ ] **Exercise 1.13: Complete after viewing the video**
    - In a comment in this issue with a title `1.13 Supporting Core Needs`,
      1. Describe something you try to do when working with customers to
         support their core human needs.
      1. Describe something new you will try to do to support customers'
         core human needs.
- [ ] Video 14 Core Human Needs Conclusion (1:05)
- [ ] Video 15 Know Your Audience (1:16)
  - [ ] **Exercise 1.15: Complete after viewing the video**
    - In a comment in this issue with a title `1.15 Know Your Audience`,
      1. List at least 3 other spectra that describe opposing types of customers
      1. Pick one spectrum example and describe how the needs of customers on
         opposite ends of that spectrum would differ.
      1. Describe how you would tailor your approach for each.
- [ ] Video 16 Know Your Audience Conclusion (1:38)
- [ ] Video 17 Matching Words (3:44)
- [ ] Video 18 Personalized and Succinct (3:31)
- [ ] Video 19 Catch Intro (7:28)
- [ ] Video 20 Catch 3 Factors (2:27)
- [ ] Video 21 Catch - Facts (2:24)
- [ ] Video 22 Catch - Emotions (2:44)
- [ ] Video 23 Catch - Needs (3:00)
</details>

<details>
  <summary>Module 02: Clarify</summary>

### Module 02: Clarify

_Total video content in Module 02: 79m 25s_
- [ ] Video 01 Clarify Introduction (0:52)
- [ ] Video 02 Regular Contact (2:37)
- [ ] Video 03 Sync Intro (0:54)
  - The next video presents two different viewpoints regarding working
    synchronously with a customer. As you watch it think about what pros and
    cons you see in each of the two perspectives.
  - [ ] Video 03a Two Sync Viewpoints (2:21)
    - [ ] **Exercise 2.03a: After watching the Sync Viewpoints video:**
      - In a comment in this issue with a title `2.03a Sync Viewpoints`,
        1. Summarize the pros and cons you perceived in each of the presented
           perspectives
      - Now watch the follow-up video
  - [ ] Video 03b Why Sync Is Valuable (5:53)
- [ ] Video 04 No Silence (2:48)
- [ ] Video 05 SLA Clock (4:04)
  - **NOTE:** The next video is almost 13 minutes long. You may still wish to
    watch it, as it contains a valuable discussion around managing the SLA
    clock. Whether you watch the video or not, please be sure to complete the
    first part of exercise 2.05 before moving on to the next video.
  - [ ] Video 05a SLA Clock Discussion (12:56)
    - [ ] **Exercise 2.05:**
      - In a comment in this issue with a title `2.05 SLA Clock`,
        1. Note your key takeaways from this topic
    - [ ] Take a look at our [GitLab Reminders App](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/zendesk/apps/global-apps/#gitlab-reminders-app)
      - In the same comment for exercise 2.05:
        1. Describe whether and how you plan to change your approach to
           managing SLA
- [ ] Video 06 Managing Expectations (9:01)
  - [ ] **Exercise 2.06:**
    - In a comment in this issue with a title `2.06 Managing Expectations`,
      1. If possible, give an example of when you've managed a customer's
         expectations
         1. In your example, did you manage expectations throughout the
            interaction, or just sometimes?
         1. What impact did it have?
         1. What might you have done differently, based on what you know now?
      1. Give an example of when you haven't managed expectations
         1. What impact did that have?
         1. What might you have done differently, based on what you know now?
- [ ] Video 07 Communications Structure Intro (1:33)
- [ ] Video 08 Clarify Request (14:44)
- [ ] Video 09 Brief History (4:14)
- [ ] Video 10 Action Plan (7:40)
- [ ] Video 11 Closing (2:45)
- [ ] Video 12 First Response (1:39)
- [ ] Video 13 Attention to Detail (4:19)
- [ ] Video 14 Meeting Follow-up (1:05)
</details>

<details>
  <summary>Module 03: Create Positive Outcomes</summary>

### Module 03: Create Positive Outcomes

_Video content for Module 03: 67m 58s_
- [ ] Video 01 Create Positive Outcomes - Introduction (1:13)
- [ ] Video 02 Understanding the Customer - Intro (1:00)
  - [ ] **Exercise 3.02: Complete before moving on to the next video**
    - In a comment in this issue with a title `3.02 Understanding the Customer`,
      1. Answer this question: What are the different priorities, pressures and
         motivations our customers have? Some guidance as you consider your
         answer:
         1. Think about the situation behind the customer's problem or question.
            Are they anxious about something, is something pushing them, why are
            they trying to do what they're doing?
         1. Possible things include: security, time, personal reputation,
            organization reputation, their boss, not understanding their
            assigned task, etc.
    - Now watch the discussion video to hear what previous students had to say.
  - [ ] Video 02a Understanding the Customer - Discussion (4:39)
- [ ] Video 03 Interest Based Problem Solving (11:17)
- [ ] Video 04 Find the Yes (3:33)
  - [ ] **Exercise 3.04: Complete before moving on to the next video**
    - In a comment in this issue with a title `3.04 Find the Yes`,
      1. Write down some thoughts about one or more tickets you've worked in
         which you:
         1. succeeded in finding the yes - talk about how you did it
         1. struggled to find the yes - talk about what you might do differently
            based on what you've learned today
         1. maybe didn't struggle at the time but now recognize you didn't try
            to find the yes and so could have handled it better
b) Then watch the second video for some examples and discussion
  - [ ] Video 04a Find the Yes - Examples (09:40)
- [ ] Video 05 Engaging Additional Expertise (2:13)
- [ ] Video 06 When Customers Push Back (9:46)
- [ ] Video 07 Recovery Introduction (6:32)
  - [ ] **Exercise 3.07: Complete before moving on to the next video**
    - In a comment in this issue with a title `3.07 Recovery`,
      1. Write down responses to the prompt in the video: what are things we
         should never say to customers, specifically when we're trying to
         recover?
    - Then watch the next video for some recovery tips.
  - [ ] Video 07a Recovery Tips (4:47)
- [ ] Video 08 Advocate for GitLab - Intro (3:26)
  - [ ] **Exercise 3.08: Complete before moving on to the next video**
    - In a comment in this issue with a title `3.08 Advocate for GitLab`,
      1. Write down some thoughts about what NOT to say. That is, what things
         might we say, but shouldn't, because they are somewhat opposite to
         advocating for GitLab, our product, and our team members?
    - Then watch the next video to learn about what we **SHOULD** actually say
  - [ ] Video 08a Advocate - What To Say (9:03)
- [ ] Video 09 Create Positive Outcomes - Wrap-up (0:49)
</details>

<details>
  <summary>Module 04: Own the Interaction</summary>

### Module 04: Own the Interaction

_Video content for Module 04: 48m 38s_
- [ ] Video 01 Own the Interaction - Intro (0:42)
- [ ] Video 02 Plan Meetings (4:54)
- [ ] Video 03 Manage Your Time (2:13)
  - [ ] **Exercise 4.03:**
    - In a comment in this issue with a title `4.03 Manage Your Time`,
      1. Describe how you feel and how you respond when a customer asks to get
         into a Zoom call with you
      1. Describe what comes to mind when we say that you should create and
         agree on an agenda for a meeting
  - [ ] Video 03a Setting an Agenda (7:38)
    - [ ] **Exercise 4.03a:**
      - In a comment in this issue with a title `4.03a Setting an Agenda`,
        1. Think about an interaction you've had recently in which the customer
           requested a meeting. Describe what agenda you would create for
           that meeting. Be sure your agenda is within your control, and describes
           what will happen by the end of the meeting.
- [ ] Video 04 Bring the Customer with You (8:28)
- [ ] Video 05 Obstacles - Intro (1:17)
  - [ ] Video 05a Obstacles - Awaiting Customer (7:07)
  - [ ] Video 05b Obstacles - No Show (2:17)
  - [ ] Video 05c Obstacles - No Contact (9:12)
  - [ ] Video 05d Obstacles - User Error (3:58)
- [ ] Video 06 Own the Interaction - Wrap-up (0:52)
</details>

<details>
  <summary>Module 05: Educate</summary>

### Module 05: 

_Video content for Module 5: 18m 30s_
- [ ] Video 01 Educate - Intro (10:06)
- [ ] Video 02 Enhance Customer Knowledge (2:40)
- [ ] Video 03 Educate at Every Step (5:44)
</details>

<details>
  <summary>Module 06: Close the Loop</summary>

### Module 06: Close the Loop

_Video content for Module 06: 17m 39s_
- [ ] Video 01 Close the Loop - Intro (0:50)
- [ ] Video 02 Meeting Follow-Up (0:50)
- [ ] Video 03 When to Close (8:17)
- [ ] Video 04 Closing Message (6:43)
- [ ] Video 05 Document Internally (0:59)
</details>

<details>
  <summary>Assessment</summary>

### Assessment

There are two parts to this assessment. The first part is only applicable if
you've gone through all of the modules and completed all of the exercises.
The second part is applicable for everyone. Should you not receive a passing
score on the assessment, agree with your manager on a plan for improving the
necessary skills and knowledge before re-assessing.

#### 1. Modules and Exercises

- [ ] Ask your manager (or any Support Manager) to review your responses to the
  exercises
- [ ] Meet with that manager to hear any feedback they might have to offer, and
  to discuss in general what you've learned from the course

#### 2. Demonstrating Ticket Management and Customer Communication

It will take some practice to build habits based on what you've learned in this
course. Allow yourself 2-4 weeks of working support tickets - enough time so
that you feel you've become somewhat consistent - before proceeding to the
final tasks.

Please work with your manager, in whatever way works best for the two of you.
Select two or more tickets that demonstrate mastery of the below modules, and document
briefly how it does so. It is OK to use the same ticket in multiple modules 
if you feel that the ticket handling demonstrates this module's topic:

- [ ] Module 1: Connect (Build rapport or relationship, Focus on customer
  needs, Know your audience, Catch)
  1. `Ticket 1` (and link)
     - Summary: 
  1. `Ticket 2` (and link)
     - Summary: 
- [ ] Module 2: Clarify (Keep regular contact, Manage expectations, Complete
  and clear communication)
  1. `Ticket 1` (and link)
     - Summary: 
  1. `Ticket 2` (and link)
     - Summary: 
- [ ] Module 3: Create Positive Outcomes (Understand the customer’s priorities,
  pressures and motivations, Find the yes, Recover, Advocate for GitLab)
  1. `Ticket 1` (and link)
     - Summary: 
  1. `Ticket 2` (and link)
     - Summary: 
- [ ] Module 4: Own the Interaction (Value the customer’s time, Bring the
  customer along, Keep moving toward a successful outcome)
  1. `Ticket 1` (and link)
     - Summary: 
  1. `Ticket 2` (and link)
     - Summary: 
- [ ] Module 5: Educate (Enhance the customer’s knowledge and understanding of
  the product, Educate the customer every step of the way)
  1. `Ticket 1` (and link)
     - Summary: 
  1. `Ticket 2` (and link)
     - Summary: 
- [ ] Module 6: Close the Loop (Summarize meetings and resultant action plans,
  Verify before closing, Communicate internally)
  1. `Ticket 1` (and link)
     - Summary: 
  1. `Ticket 2` (and link)
     - Summary: 

<!-- 
For the future, we might consider creating more specific criteria, requiring
manager review of a selection of tickets.

- [ ] Schedule a meeting with your manager (or any Support Manager), about two
  weeks in advance, for an assessment of your ticket work
- [ ] Manager: Select about six tickets that this support engineer has owned
  and worked during the two-week period - be sure they all have at least a
  couple of meaningful responses in them - and assess whether, to what extent,
  and to what quality level the support engineer has employed ticket management
  and customer communications skills as taught in this course
  - [ ] Passing Score
    - TBD
  -->
</details>

#### Congratulations! You made it, and should now have a strong understanding of how we approach customer service and ticket management at GitLab!

We hope that in the future you'll revisit individual
videos whenever you want a little refresher on specific topics.

You are now ready to continue on your onboarding path to tackle the next module in line based on your role. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [onboarding page](https://handbook.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under the Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::Customer Service Skills"
