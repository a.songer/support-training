---
module-name: "Subscriptions License and Renewals"
area: "Product Knowledge"
gitlab-group: "Growth"
maintainers:
  - TBD
---

## Introduction

Welcome to the Subscriptions/ License & Renewals Learning Module!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://handbook.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**Your Training Buddy:** `[tag buddy]`

**Goals of this checklist**

At the end of the checklist, team member is able to

- triage, understand and answer most of the L&R tickets.
- provide internal support to sales and accounts on requests related to licensing, subscriptions, customers portal capabilities and trials.
- understand how subscriptions and licenses work, and be able to efficiently troubleshoot related issues.

**General Timeline and Expectations**

- This module should take you approximately 2 Weeks to complete.

## Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: `<module title> - <your name>`.
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Submit an [Individual or Bulk Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request) to be added to the `support-licensing-subscription` Slack **Group** (not CHANNEL). This can only be done by a Slack Admin in our IT department. In the `System name` area, specify as follows:
   ```
   - [ ] System name: Slack Group support-licensing-subscription
     - Justification for this access: I am completing the Licensing & Renewals Learning Module.
   ```
1. [ ] Submit an access request using [this template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=role_support-CDot-full-access-request-template) to have write permissions added to your Customers Portal access.


Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

**Prerequisites**

If you are a new hire, please make sure that you have completed the following modules in the new hire onboarding pathway before starting this one for it to be effective.
1. [ ] [Git and GitLab Basics](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Git-GitLab-Basics)
1. [ ] [GitLab Support Basics](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab-Support-Basics)
1. [ ] [Zendesk Basics](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Zendesk-Basics)

### Stage 1: License and Renewals - An Introduction

1. [ ] L&R queue is used to help customers with issues and questions related to licensing and renewals. Read the [overview of L&R work in Support](https://handbook.gitlab.com/handbook/support/license-and-renewals/#overview) and [What is not in the scope of L&R work in Support?](https://handbook.gitlab.com/handbook/support/license-and-renewals/#what-is-not-in-the-scope-of-lr-work-in-support) sections to understand the scope of tickets you'll see in this queue.
1. [ ] Understand how this queue aligns with the product's Growth Stage by reading the [How does this queue align with the "Growth" Stage?](https://handbook.gitlab.com/handbook/support/license-and-renewals/#growth-stage) section.
1. [ ] You will notice that L&R tickets have a slightly different Service Level Agreement for response time on tickets. However, this should not majorly impact how we want to approach these tickets and our priority to ensure customers get help.

**Resources**

As you progress through this issue, and as you begin working on L&R tickets, you'll have a lot of questions! Don't worry - we have several resources and a very helpful team. Please either bookmark (docs, pages, wikis) or join (Slack) the below resources as applicable.

Note: **Do not try to read everything in this list right now** - understand the intent of the resource to know where help is available when you need it!

1. [ ] [#support_licensing-subscription Slack Channel](https://gitlab.slack.com/archives/C018C623KBJ)
1. [ ] [Customer facing Subscription and Licensing FAQs](https://about.gitlab.com/pricing/licensing-faq/)
1. [ ] [Support Workflows related to L&R](https://handbook.gitlab.com/handbook/support/workflows/#License%20and%20subscription)
1. [ ] [License and Renewals Queue Workflow](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/)
1. [ ] [L&R Notice Board](https://gitlab.com/gitlab-com/support/license-and-renewals/-/issues/1)
1. [ ] [When to escalate to Sales](https://gitlab.com/gitlab-com/support/license-and-renewals/-/wikis/When-to-escalate-to-Sales)
1. [ ] [Issue Trackers used for L&R](https://handbook.gitlab.com/handbook/support/license-and-renewals/#issue-trackers)
1. [ ] [#s_fulfillment Slack Channel](https://gitlab.slack.com/archives/CMJ8JR0RH)

### Stage 2: Systems & Tools

1. [ ] **Zendesk**: ZenDesk is our ticket management system, and L&R tickets are currently managed through the License & Renewals View on [ZenDesk](https://gitlab.zendesk.com/agent/dashboard). **You won't be able to see the `L&R` Zendesk view unless you belong to the `Support Focus: L&R` group in Zendesk.** If your area of focus will be L&R tickets, please edit your [support team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) file, adding `Support Focus: L&R` to the ZenDesk groups section. Submit your changes on a new branch and raise a merge request, which will be reviewed by the Support Ops team. Once this has been merged and processed, you will see the `L&R` view in Zendesk.
    1. [ ] (Optional) You can create your own personal Views on ZenDesk to be able to easily manage tickets that have been escalated to teams outside of Support. If you decide to create one, make sure that you can see the same new/opened tickets that are visible in the `L&R` view.
    1. [ ] Watch this [~9 minute video](https://drive.google.com/file/d/1Kpijhb65GqnIoSnX81ipjafE5i9GJ4nu/view) to understand creation of Views on ZenDesk.
1. [ ] **Salesforce**: Salesforce is our CRM, and houses all details including opportunities and renewals information. L&R tasks require occasional communication in Salesforce's Chatter. To keep track of such conversations, please file a [Single Person Access request](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#single-person-access-request) to get access to Salesforce.
   - The permission set you need is as follows:
     - If a US citizen:
       - Role: [Executive](https://gitlab.my.salesforce.com/00E61000000QQoB)
       - Profile: [Account Executive](https://gitlab.my.salesforce.com/00e61000000uKvK)
     - If **not** a US citizen:
       - Role: [Executive - Global Minus Pubsec](https://gitlab.my.salesforce.com/00E8X000001vcSb)
       - Profile: [Account Executive](https://gitlab.my.salesforce.com/00e61000000uKvK)
    1. [ ] Watch this [~10 minute video](https://drive.google.com/file/d/1j8zJzHIjU8D8QvjNzYVk2QbzD7tEFcgB/view) on usage of Salesforce for the purposes of this queue.
1. [ ] **Customer Portal**: Customer portal admin account will allow you to view, impersonate and check customer's subscription and license information. You should be able to login to the [Customer Portal](https://customers.gitlab.com/admin) with your Okta credentials.
    1. [ ] Watch this [~20 minute video](https://drive.google.com/file/d/1mjcyfGLomg0ToIJTZ0GR7lIq0ybPFafn/view) on usage of the Customer Portal for the purposes of this queue.
1. [ ] **Zuora**: Zuora is our subscription management system, for Sales Orders processing and invoicing. Please file a [Single Person Access request](https://about.gitlab.com/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#single-person-access-request) to get **read-only** access to Zuora.
    1. [ ] Watch this [~8 minute video](https://drive.google.com/file/d/1ZaIZczycOB0AJz3aSAS7ZTaohqYhI6t8/view) on usage of the Zuora for the purposes of this queue.
1. [ ] **Internal Requests**: We also handle internal requests from Sales and Customer Success Managers (CSM) etc to assist customers on licensing and subscription related issues. These are handled via tickets in Zendesk with a subject starting with `IR`, and were previously handled in the [Internal Requests project](https://gitlab.com/gitlab-com/support/internal-requests#requires-customers-admin-andor-license-app).
1. [ ] **GitLab.com Admin Access**: Complete [GitLab.com Admin access Training Module](.gitlab/issue_templates/GitLab-com Admin.md)


### Stage 3: Purchasing & Transactions

Customers can purchase a subscription to our product in different ways, and also purchase it as part of certain special programs (explained in Stage 4 below).

1. [ ] **Web Direct Purchases**: Customer can [directly purchase a subscription](https://about.gitlab.com/pricing/licensing-faq/#how-do-i-purchase-a-subscription) through the customer portal. These will be reflected as "Web Direct" under Opportunity -> Type in Salesforce. Note that, typically, troubleshooting web direct purchases can be simpler as identifying the issue is limited to the usual systems (Customer Portal, LicenseApp and gitlab.com).
   1. [ ] [Read the customer-facing docs to understand the steps to **view**, **obtain**, **renew** and **upgrade** a GitLab.com subscription](https://docs.gitlab.com/ee/subscriptions/gitlab_com/)
        - [ ] [Read the customer-facing docs to understand the CI/CD minutes quota](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html)
        - [ ] [Read the customer-facing docs to understand the Storage usage quota](https://docs.gitlab.com/ee/user/usage_quotas.html)
    1. [ ] [Read the customer-facing docs to understand the steps to **view**, **obtain**, **renew** and **upgrade** a self-managed subscription](https://docs.gitlab.com/ee/subscriptions/self_managed)
1. [ ] **Sales Assisted Purchases**: Customer can also be assisted by Sales to make the purchase (typically in cases where Sales found a lead, and converted the lead into an opportunity). Typically, troubleshooting Sales Assisted Purchases can be more complicated since there are several things that could have gone wrong that don't fall directly under Support's expertise. For instance, a quote could be booked wrong, or, there was manual intervention from billing or sales-support or sales that has trickled down to cause issues in the portal. This means that we should be able to identify where in the chain the issue originated and be able to escalate accordingly.

**Troubleshooting Transactions**

Transactions are any issues or questions related to Purchasing.

1. [ ] Read the list of issue types under the [Transactions](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/license_troubleshooting/#transactions) section to get an idea of the scope.
1. [ ] [Pair](https://gitlab.com/gitlab-com/support/support-pairing) with your training buddy or any other L&R focused engineer to walk through a couple of purchasing related tickets. To be more productive, prepare prior to the call - review the L&R queue and take two tickets of your choice to the call.
    1. [ ] Link the pairing issue here: `Issue`
1. [ ] Pause here: spend a few hours on reviewing purchasing transaction related tickets in the queue (suggest spending 50% of your time the next day). Move to the next stage once this is done to give yourself a break and also get a better hands on experience in these types of tickets!

### Stage 4: Licenses and Subscriptions

1. [ ] What is the difference between a license and a subscription? Read the [License vs Subscription Explained](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/license_troubleshooting/#license-vs-subscription-explained) section to understand the terminologies.
1. [ ] What is Cloud Licensing? Read the [Cloud Licensing](https://docs.gitlab.com/ee/subscriptions/self_managed/#cloud-licensing) section to understand the basics of how it works and the features included.
    - [ ] (Optional) You can read more in the [GitLab Pricing FAQ](https://about.gitlab.com/pricing/faq-improved-billing-and-subscription-management/#cloud-licensing) page.
1. [ ] Read the [Quarterly Reconciliations](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html) section to understand the basics of how it works and the features included.

**Troubleshooting Self Managed Licenses**

1. [ ] License related requests can span a variety of issues, and the most common can be read in the [Licensing](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/license_troubleshooting/#licensing) section of our workflows.
    1. [ ] (Optional) Watch this [~60 minute video](https://drive.google.com/open?id=1wNlNXNMEhMdKmEulhZRbq2EDcwSd7xYp) of where two types of licensing issues were troubleshooted: [ZD #141221](https://gitlab.zendesk.com/agent/tickets/141221) & [ZD #141225](https://gitlab.zendesk.com/agent/tickets/141225).
    1. [ ] [Pair](https://gitlab.com/gitlab-com/support/support-pairing) with your training buddy or any other L&R focused engineer to walk through a couple of license related tickets. To be more productive, prepare prior to the call - review the L&R queue and take two tickets of your choice to the call.
        1. [ ] Link the pairing issue here: `Issue`
1. [ ] Read the [Creating a License](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/self-managed/creating_licenses/) workflow to understand when and how a license should be generated for a self managed GitLab instance.
    1. [ ] Watch this [~14 minute video](https://www.youtube.com/watch?v=mYFzL3iweyw) - you should be logged in as GitLab Unfiltered YouTube account as this video is private.
    1. [ ] Find one or two L&R [internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=subject%3A%22IR%20-%20SM%22%20%22generate%22%20order_by%3Acreated_at%20sort%3Adesc) that were filed to generate a new license file - follow the steps detailed for generating a license file and add the ticket/issue links below:
        1. [ ] Ticket/Issue: `Link`
        1. [ ] Ticket/Issue: `Link`

**Troubleshooting GitLab.com Subscriptions**

1. [ ] The most common issue with GitLab.com subscriptions would be customers unable to see their paid plan active on their GitLab.com account. Read the [GitLab.com subscriptions & purchases](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/#GitLab.com%20subscriptions%20&%20purchases) section to understand how to troubleshoot these types of issues.
    1. [ ] Find two L&R [internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=%22IR%20-%20SaaS%20-%20Investigate%20incorrect%20subscription%22%20order_by%3Acreated_at%20sort%3Adesc) or tickets that deal with subscription association issues - follow the steps detailed in the workflow above and add the ticket links below:
        1. [ ] Ticket/Issue: `Link`
        1. [ ] Ticket/Issue: `Link`

**Changing the Subscription Contact on Customer Portal**

1. [ ] We get requests from customers to have a colleague be given access to manage their subscription. Read and understand the [Associating customer portal subscriptions with additional accounts](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases/) workflow section to handle these requests.
    1. [ ] Find one or two L&R tickets that deal with such association requests - follow the steps detailed in the workflow above and add the ticket links below:
        1. [ ] Ticket/Issue: `Link`
        1. [ ] Ticket/Issue: `Link`
1. [ ] Pause here: spend a few hours on reviewing license and subscription related tickets in the queue (suggest spending 50% of your time the next day). Move to the next stage once this is done to give yourself a break and also get a better hands on experience in these types of tickets!

**Billable Members**

Sales might sometimes request help with getting the exact Billable Members count to help with renewal quotes.

1. [ ] Read and understand the [GitLab Billable Members API](https://docs.gitlab.com/ee/api/members.html#list-all-billable-members-of-a-group).
1. [ ] Read [the workflow](https://handbook.gitlab.com/handbook/support/internal-support/#gitlabcom-billable-members-list) to help with billable members requests.
1. [ ] Review [previous internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=%22IR%20-%20SaaS%20-%20%20Investigate%20incorrect%20subscription%20info%22).
1. [ ] Find two L&R tickets that deal with billable member count questions. Link them below:
    1. [ ] `Issue Link`
    1. [ ] `Issue Link`

### Stage 5: Collaboration Workflows

1. [ ] **Passing to Sales**: Often you will find yourself handling a ticket where the customer is requesting help with a new or renewals quote from Sales, credit request, help with migrating from self managed to SaaS and vice versa etc.
    1. [ ] In these cases, follow the [Passing to Sales](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/working_with_sales/#find-the-account-manager-in-salesforce-sfdc) workflow.
    1. [ ] Find two L&R tickets that deal with scenarios such as this that require passing the ticket to Sales - follow the above workflow on them as appropriate and add them below with links:
        1. [ ] Ticket: `ZD Link`
        1. [ ] Ticket: `ZD Link`

1. [ ] **Passing to Accounts Receivable**: Billing related requests are handled by our Accounts Receivable team.
    1. [ ] Read the [Billing workflow](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/billing_contact_change_payments/) section to understand how to deal with these tickets.
    1. [ ] Find two L&R tickets that deal with billing - follow the above workflow on them as appropriate and add them below with links:
        1. [ ] Ticket: `ZD Link`
        1. [ ] Ticket: `ZD Link`

1. [ ] **Passing to GitLab Special Programs** team: GitLab has 2 special programs, _Education_ and _Open Source_. The workflows for these are slightly different from other tickets.
    1. [ ] **GitLab for Education**: Read [this handbook page](https://about.gitlab.com/solutions/education/).
    1. [ ] **GitLab Open Source Program**: Read [this handbook page](https://about.gitlab.com/solutions/open-source/program/).
        1. [ ] Read our workflow for handling these special program tickets: [GitLab Special Programs" EDU & OSS](https://handbook.gitlab.com/handbook/support/workflows/special-programs/).
        1. [ ] Review these example tickets: [#143825](https://gitlab.zendesk.com/agent/tickets/143825) and [143879](https://gitlab.zendesk.com/agent/tickets/143879).
    1. [ ] Please redirect general eligibility/validity related questions from internal GitLab team members regarding these programs to `#education-oss` on Slack.
    1. [ ] Find one or two L&R tickets that deal with special programs related request - follow the above workflow on them as appropriate and add them below with links:
        1. [ ] Ticket: `ZD Link`
        1. [ ] Ticket: `ZD Link`

### Stage 6: Other Internal Requests

You will see [several internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=subject%3A%22IR%22) related to L&R in the L&R ticket queue. The following sections talk about the common issue types and how to handle each.

- [ ] Open an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) to be added to the [licensing-subscription group](https://gitlab.com/gitlab-com/support/licensing-subscription) on gitlab.com to ensure you are notified about upcoming changes.

**Trial Extensions and Plan Change Requests**

1. [ ] GitLab.com Trials: Read and understand the [Extending GitLab.com Trials](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/saas/trials_and_plan_change/#extending-trials) workflow.
    1. [ ] Note that you might have to escalate some of these to Support Engineers with console access using the `Console Escalation::Customers` label.
    1. [ ] Navigate to [SaaS - Trial Extension internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=subject%3A%22IR%20-%20SaaS%20-%20Extend%20an%20existing%20trial%22%20order_by%3Acreated_at%20sort%3Adesc) and work on two tickets of your choice. Link them below:
        1. [ ] `Issue Link`
        1. [ ] `Issue Link`
    1. To discourage and reduce abuse, we rolled out [credit card validation](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/) in 2021 - Any new users created after launch who want to run CI pipelines in a **free** namespace on shared runners will be required to provide a credit card.
    There is a process to exempt credit card validation for prospective customers(sales-assisted trials).
        1. [ ] CI minutes: Read and understand the [Enable CI minutes for sales assisted trials](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/saas/ci_minutes/) workflow.
1. [ ] Plan Change Requests: Plan change requests follow a similar workflow to Gitlab.com trial extensions, read and understand the [Plan Change Request](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/saas/trials_and_plan_change/#plan-change-requests) workflow.
    1. [ ] Navigate to [SaaS - Change existing plan internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=subject%3A%22IR%20-%20SaaS%20-%20Change%20existing%20SaaS%20trial%20plan%22%20order_by%3Acreated_at%20sort%3Adesc) and work on two issues of your choice. Link them below:
        1. [ ] `Issue Link`
        1. [ ] `Issue Link`

1. [ ] Self Managed Trial Extensions are also called Temporary Licenses: Read and understand the [Extending GitLab Self-managed Trials](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/self-managed/trials/) workflow.
    1. [ ] Navigate to [SM - Extend an existing trial internal requests](https://gitlab.zendesk.com/agent/search/1?copy&type=ticket&q=subject%3A%22IR%20-%20SM%20-%20Extend%20an%20existing%20trial%22%20order_by%3Acreated_at%20sort%3Adesc) and work on two issues of your choice. Link them below:
        1. [ ] `Issue Link`
        1. [ ] `Issue Link`

**Resend/Forward License Requests**

Sometimes the Sales team or a customer themselves will request to resend or forward the license to a different email address.
1. [ ] Read how to [resend and forward licenses](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/self-managed/sending_license_to_different_email/)
    1. [ ] Note that Cloud Activation codes can only be sent to the `Sold To` contact in Zuora.

## Final Steps, Onboarding Feedback & Documentation

1. [ ] Let your trainer know that you have worked through this issue and you're ready for a review.
1. [ ] Set aside time with your trainer to go through this issue and review any topics you struggled with and talk through any questions you have.
1. [ ] As you work through the L&R queue and on issues, if you spot something in the [Fulfillment backlog](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Afulfillment) that would makes things better for customers and Support, please don’t forget to label it with `Support Priority` - this helps the product team in prioritising for the next milestone.

Keeping our documentation and workflows up to date will ensure that all Support team members will be able to access and learn from the best practices of the past. Giving feedback about your onboarding experience will ensure that this document is always up to date, and those coming after you will have an easier time coming in.

1. [ ] (Optional) Make an update to this template to make it better and link the MR below. The files are located in an issue template in the ['support-training` repository](/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates). Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).
    1. `Link MR Here`

#### Congratulations on completing your L&R training module successfully!
/label ~"Module::Subscriptions License and Renewals"
/assign me