---
module-name: "GitLab Runner"
area: "Product Knowledge"
gitlab-group: "Verify:Runner"
maintainers:
  - rvzon
---

## Overview

**Goal**: Set a clear path for GitLab Runner training

**Objectives**: At the end of this module, you should be able to:
- <insert a few concrete actions that learner should be able to do at the end of this module>

Recommended is to tackle the stages as defined below, you don't have to complete all of the steps in a stage before you can move on.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GitLab Runner - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab Runner questions to you
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what GitLab Runners are

- [ ] **Done with Stage 1**

1. [ ] Start with reading about [GitLab Runners](https://docs.gitlab.com/runner/) (No need to follow any links on the page)
1. [ ] [Install a GitLab Runner on preferred OS](https://docs.gitlab.com/runner/install/linux-repository.html)
1. [ ] Create an example project on your own GitLab instance.

## Stage 2: Basic GitLab Runner

- [ ] **Done with Stage 2**

1. [ ] Learn about possible variables in the [.gitlab-ci.yml docs](https://docs.gitlab.com/ee/ci/yaml/)
1. [ ] Read up on [Executors](https://docs.gitlab.com/runner/executors/)
1. [ ] Register your runner as a [Shared Runner](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-shared-runner-with-a-runner-authentication-token) on your instance.
1. [ ] Register your runner as a [Project Runner](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-project-runner-with-a-runner-authentication-token) on your project, and test that it now runs the builds for your project.
1. [ ] Read about [Pipelines and Jobs](https://docs.gitlab.com/ee/ci/pipelines/).
1. [ ] Learn about [using tags](https://docs.gitlab.com/ee/ci/yaml/#tags)

## Stage 3: Intermediate GitLab Runner

- [ ] **Done with Stage 3**

1. [ ] [Install and register a GitLab Runner through Docker image](https://docs.gitlab.com/runner/install/docker.html)
1. [ ] Read about [services](https://docs.gitlab.com/ee/ci/services/)
1. [ ] Configure a Service to be used in a pipeline
1. [ ] Read the [troubleshoot section for GitLab Runner](https://docs.gitlab.com/runner/faq/)
1. [ ] [Unregister](https://docs.gitlab.com/runner/commands/#gitlab-runner-unregister) a GitLab Runner

## Stage 4: Advanced GitLab Runner

- [ ] **Done with Stage 4**

1. [ ] Read [Install and register GitLab Runner for autoscaling with Docker Machine](https://docs.gitlab.com/runner/executors/docker_machine.html)
1. [ ] Read how to [install GitLab Runner on Windows](https://docs.gitlab.com/runner/install/windows.html)
1. [ ] Optional: [download a Windows 10 evaluation VM](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/) and install and register GitLab Runner on the Windows 10 evaluation VM.
1. [ ] Configure a [self-signed certificate](https://docs.gitlab.com/runner/configuration/tls-self-signed.html#self-signed-certificates-or-custom-certification-authorities) for a runner.
1. [ ] Install [GitLab Runner Kubernetes clusters](https://docs.gitlab.com/runner/install/kubernetes.html).

## Optional: Pipeline section

- [ ] **Done with the Optional Pipeline section**

1. [ ] [Multi-project pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines)
1. [ ] [Parent-Child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#parent-child-pipelines)
1. [ ] [Pipelines API](https://docs.gitlab.com/ee/api/pipelines.html)
1. [ ] Speed up a pipeline using [caching](https://docs.gitlab.com/ee/ci/yaml/#cache).

### Stage 5: Tickets

- [ ] **Done with Stage 5**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final Stage

1. [ ] Manager: Have a GitLab Runner expert in Support review 3-5 of the tickets from Stage 5 and report back to you on whether sufficient GitLab Runner knowledge has been demonstrated.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as the Runner Expert on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::GitLab Runner"
/assign me