---
module-name: "GitLab-com SaaS Basics"
area: "Customer Service"
maintainers:
  - ralfaro
---

## Introduction

This training module provides Support Engineers with the basics of answering GitLab.com (SaaS) product related tickets,
for both new and existing Support team members who are looking to start working on SaaS tickets.

**Goals**

At the end of this module, you should be able to:
- use chatops to look up information.
- answer GitLab.com questions.

**General Timeline and Expectations**

- This module should take you **2 weeks to complete**.
- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/), the page also shows you the modules under the GitLab.com SAAS Support Basics Pathway.

## Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: `<module title> - <your name>`.
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Submit a [Slack, Google Groups, 1Password Vaults or Groups Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests) to be added to the `support-dotcom` Slack **Group** (not CHANNEL). This can only be done by a Slack Admin in our IT department. In the `System name` area, specify as follows:

```
- [ ] System name: Slack Group support-dotcom
    - Justification for this access: I am completing the SaaS Learning Module.
```

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.


## Stage 1: Overview and Chatops

**NOTE**

In Stage 1 you will be using an [Auditor account](https://docs.gitlab.com/ee/administration/auditor_users.html) which is a read-only account, and does not have admin access. If you have any issues completing tasks, please pair with a support team member that has admin access.
To find someone with admin access, look for a [team member that has 50% or more SaaS focus](https://gitlab-com.gitlab.io/support/team/areas-of-focus.html).

---

- [ ] **Done with Stage 1**

1. [ ] Read the [GitLab.com Overview page](https://handbook.gitlab.com/handbook/support/workflows/gitlab-com_overview/).
1. [ ] You should have already received an Auditor account for GitLab.com during the onboarding process. If not, follow up on your onboarding issue, or [create a new GitLab.com account](https://gitlab.com/users/sign_in?#register-pane) and submit a new access request. [Example access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/8641).
    - If your regular GitLab account has the following details:
        - username:`awesome_user7`
        - email: `awesomeuser@gitlab.com`
    - Your auditor account should be:
        - username: `awesome_user7-auditor`
        - email: `awesomeuser+auditor@gitlab.com`

Setup the chatops access as follows:

1. [ ] Read the first two sections: [the Chatops intro and how it works](https://docs.gitlab.com/ee/ci/chatops).
1. [ ] If you don't already have it, [get Chatops access](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom).
    - Note: Use the "Google" sign in on the ops instance to create an account if needed.
1. [ ] Read about [commonly used chatops commands](https://handbook.gitlab.com/handbook/support/workflows/chatops/).
1. Practice using chatops by finding your own user and one of the support groups. You can direct message chatops by finding 'GitLab Chatops' under Apps in Slack.
    - [ ] What's your user ID? ID:
    - [ ] What's the group ID for `gitlab-com/support`? ID:
    - [ ] Find a feature flag to search for and get its current status. Take a screenshot and post it in a comment to the issue. Please blur out any user/group-identifiable information or don't include one with scoping.
1. [ ] Take a look at our [improving chatops for Support epic](https://gitlab.com/groups/gitlab-com/-/epics/306). Consider contributing to one of the issues in the future. As more of these functions become available, the more we can move cases out of the admin-only section.
    - [ ] (Optional) Watch this [Getting Started with Chatops in Support](https://youtu.be/9o8PDeljXtw) video (~35 minutes) to understand how to get started with contributing to chatops in GitLab Support.

## Stage 2: GitLab.com Accounts

*Note*: Stage 2 and 3 are reversible. For those who want a more "structured" introduction to procedural tickets, start here.
If you're already familiar with GitLab and are used to troubleshooting self-managed instances, consider doing Stage 3 first.

See also Stage 4 for a place to record pairing sessions.

- [ ] **Done with Stage 2**

Keep in mind that only GitLab team members are admins, and that accounts are bound by our Terms of Use as covered [in the overview](https://handbook.gitlab.com/handbook/support/workflows/gitlab-com_overview/).

_Bottom Line_: Never share information that a user would not have access to even if it's about another user within their organization, except for Enterprise Users.
You can still suggest possible solutions based on the description of the problem (and even what you find out about the account), but definitive information cannot be shared.

### Triaging

Many common tickets are triaged to other places.

1. [ ] Personal Data Access and Account Deletion requests: review the [workflow](https://handbook.gitlab.com/handbook/support/workflows/account_deletion_access_request_workflows/) and the [introduction page](https://handbook.gitlab.com/handbook/support/workflows/personal_data_access_account_deletion/) to understand how to process these types of requests. Note that there are macros in Zendesk for these cases.
1. [ ] Subpoenas and other legal (court) requests. See [Subpoenas and other requests for information](https://handbook.gitlab.com/handbook/support/workflows/information-request/)
    - Note: If the customer request is _not_ tied to an active court case or other legal matter, it may fall under one of the other cases below.
1. [ ] Read over the [example cases that we redirect to Security](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/).
    1. [ ] [Copyright (DMCA) Removal Requests](https://handbook.gitlab.com/handbook/support/workflows/dmca/#submitting-a-dmca-request) has its own email.

### Email Receiving

One of the main differences of working with GitLab.com is that we receive many requests from [free users](https://about.gitlab.com/support/statement-of-support/#free-and-community-edition-users) because we are the administrators of GitLab.com. One of the most common cases is not receiving confirmation, password, or other system notifications from GitLab.

1. [ ] Read the [Confirmation Emails workflow](https://handbook.gitlab.com/handbook/support/workflows/confirmation_emails/).
1. [ ] Answer at least 1 ticket with an email receiving workflow: (insert ticket link here)
1. [ ] Briefly look over [Real Time Blocklist Delisting](https://handbook.gitlab.com/handbook/support/workflows/rbl_delisting/) for when GitLab emails are being marked as spam.

### Other Account Issues

1. [ ] Take a look at the [list of GitLab.com workflows](https://handbook.gitlab.com/handbook/support/workflows/quick-reference/)

Remember: When triaging tickets and you're unsure if a user is asking about a Self-managed instance or GitLab.com, use the GitLab User Lookup app in right ticket sidebar to see if the user exists.

## Stage 3: GitLab.com Architecture and Troubleshooting

- [ ] **Done with Stage 3**

**Keep in mind:**

1. As previously noted, GitLab.com users do not have admin accounts. Please do not send any doc links that are self-managed only (check badging at the top of the page or section for **FREE/STARTER/PREMIUM/ULTIMATE ONLY**) or anything from the `administration` section.
1. Troubleshooting problems on SaaS and Self-managed are the same or similar. The main difference is where and how we gather the necessary information.

### Architecture

1. Review materials relevant to GitLab.com Architecture
    1. [ ] Read about the [GitLab architecture](https://docs.gitlab.com/ce/development/architecture.html) up to and including the `Simplified component overview` section.
        - Note: You're not expected to remember everything, but to get a general sense of GitLab's architecture. Feel free to read the details on any component based on your interest.
    1. [ ] Give the [Production architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/) page a brief lookover.
        - Note: Mostly, you'll want a broad understanding of the "Current", "Service", and "Network Architecture" sections.
    1. [ ] Give the [GitLab.com Setting docs page](https://docs.gitlab.com/ee/user/gitlab_com/) a brief review to know what settings we publicize.
        - Note: Instance level settings are split between admin dashboard, [gprd-base settings](https://ops.gitlab.net/gitlab-com/gl-infra/chef-repo/-/blob/master/roles/gprd-base.json) (equivalent of `gitlab.rb`), and secrets (which only infra has access to).

### Searching logs and filing issues

1. Learn about finding and filing errors. Note: Use the Okta option to sign in to Kibana and Sentry.
    1. [ ] [Using Kibana](https://handbook.gitlab.com/handbook/support/workflows/kibana/).
        - Note: Typical usage is `rails` and `sidekiq` logs. `HAproxy` and [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/blob/docs/cloudflare/logging.md) log [access is restricted](https://gitlab.com/gitlab-com/runbooks#on-call). Ask in the #production Slack channel if necessary.
    1. [ ] [Searching Kibana, Sentry, and filing GitLab issues from Sentry](https://handbook.gitlab.com/handbook/support/workflows/500_errors/).
    1. [ ] As an exercise, visit a non-existing page on GitLab.com, such as [gitlab.com/gitlab-com/doesntexist](https://gitlab.com/gitlab-com/doesntexist). Search Kibana for the 404 error using your username. Add a screenshot of the relevant portion of the error you find in Kibana as a comment to this issue.
    1. [ ] Search Sentry using the `correlation_id`. You may not find anything and the search is not reliable, but take a screenshot of your search and results and add it as a comment to this issue anyway.
    1. [ ] Exercise: Read through the [import workflow](https://handbook.gitlab.com/handbook/support/workflows/importing_projects/#project-import-overview), then search for an example [import error](https://handbook.gitlab.com/handbook/support/workflows/kibana/#import-errors).
    1. [ ] Importing customer projects is the only current case of "getting around" a bug or lack of feature. Read [the rest of the import criteria](https://handbook.gitlab.com/handbook/support/workflows/importing_projects/#pre-approved-cases) to learn when we do these as a courtesy.
1. If an issue can only be resolved through rails console access, you can [file a "GitLab.com Console Escalation" issue](https://handbook.gitlab.com/handbook/support/workflows/working-with-issues/#operational-escalation-points).
1. [ ] Recommended: Pair on ticket(s) that require use of Kibana/Sentry. Alternatively, wait until you have your first ticket that requires Kibana and/or Sentry. 

### Places to reproduce

The best way to troubleshoot is to try to reproduce it.

1. [ ] You have access to 2 GitLab.com personal namespaces, one for each plan tier. <USERNAME>_premium_group, and <USERNAME>_ultimate_group.
    - Since these are [private groups](https://docs.gitlab.com/ee/user/public_access.html#private-projects-and-groups), you can not link them in tickets for customers to view.
    - If you find out that you do not have access to one of these groups, ask for help in the `#support_operations` Slack channel.

### Specific cases

Because GitLab is the steward of GitLab.com, we sometimes need to troubleshoot some of our integrations. We have workflows for some cases. You don't need to read these in detail now, but keep in mind that these are available:

1. [ ] [Custom domain verification on GitLab.com](https://handbook.gitlab.com/handbook/support/workflows/verify_pages_domain/)
1. [ ] [Service Desk Troubleshooting](https://handbook.gitlab.com/handbook/support/workflows/service_desk_troubleshooting/)

### IP blocks and log requests

1. [ ] At times, the infra team blocks users due to alerts of possible abuse. Learn about [sending notices](https://handbook.gitlab.com/handbook/support/workflows/sending_notices/).
    - Note: If needed, ping the [CMOC](https://handbook.gitlab.com/handbook/support/internal-support/#regarding-gitlab-support-plans-and-namespaces) to send these out.
1. [ ] Read about [identifying the cause of IP blocks](https://handbook.gitlab.com/handbook/support/workflows/ip-blocks/). You won't find a block, but try doing a search by IP of your own IP to see what gets logged in the `rails` log.
1. [ ] When responding to users about possible blocks, you'll need to keep in mind what information you're allowed to provide. See the [log and audit requests workflow](https://handbook.gitlab.com/handbook/support/workflows/log_requests/) for more information.

### Security issues

Security's focus is instance wide security breaches and vulnerabilities. They do however take care of some account related cases (covered in previous stage).

1. [ ] Commonly, users file commit associations as a "security" issue. Follow the [investigate commits](https://handbook.gitlab.com/handbook/support/workflows/investigate_commits/) on determining whether it's a security incident.

### GitLab.com incidents

[Incidents](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-management) are anomalous conditions that result in — or may lead to — service degradation or outages. You will learn more about GitLab.com incidents in the [On-Call Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/On-Call%20Basics.md) module, so you can skip this section if you're already doing that module.

If you notice an influx of very similar SaaS tickets in a short period of time, first check [status.gitlab.com](https://status.gitlab.com/) and the [#incident-management channel](https://gitlab.slack.com/archives/CB7P5CJS1) to see if an incident has been declared. If one hasn't been declared, you may need to do so.

1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.
1. [ ] Read about [how to report an incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident).
1. [ ] Give the [Tracking Incidents](https://handbook.gitlab.com/handbook/support/workflows/tracking_incidents/) workflow a brief review to understand how to handle tickets related to a declared incident.

## Stage 4: Keeping up to date

- [ ] **Done with Stage 4**

Keeping up to date and asking questions:

1. [ ] Keep an eye out for "SaaS focus" marked items in the [Support Week in Review (SWIR)](https://gitlab.com/gitlab-com/support/readiness/support-week-in-review/-/issues/?label_name%5B%5D=SWIR%3A%3ADigest).
1. [ ] Make sure you're in the #support_gitlab-com Slack channel where you can ask questions about tickets, process, etc. Also check the pinned messages in that channel for recent changes.

## Stage 5: GitLab.com User Accounts work and admin access (with manager approval)

If you are not doing this stage, skip to the last section.

- [ ] **Done with Stage 5**

1. [ ] Speak to your manager about whether you will be handling "SaaS Account" tickets. If yes, do this section. If not, don't. See [the support training handbook page](https://handbook.gitlab.com/handbook/support/training/#support-engineer-area-of-focus-pathway) for more information.
1. [ ] Typically at week 3-4 at GitLab, you and your manager should check in that you're ready for admin access. If you are, complete [GitLab.com Admin access Training Module](.gitlab/issue_templates/GitLab-com Admin.md). Note: We recommend submitting the access request approximately 1 week before you will be actively using it.

### Internal Requests

1. Update notification settings to **Watch** which will send you notifications for all activity, or **Custom** for new issues in the following project:
    1. [ ] [internal requests](https://gitlab.com/gitlab-com/support/internal-requests). Alternatively, given there are a lot of different requests, only [subscribe](https://docs.gitlab.com/ee/user/project/labels.html#subscribing-to-labels) to the following labels:
        - [Admin Escalation](https://gitlab.com/gitlab-com/support/internal-requests/-/labels?subscribed=&search=admin+escalation)
        - [DEWR](https://gitlab.com/gitlab-com/support/internal-requests/-/labels?subscribed=&search=DEWR) (Dotcom Escalation Weekly Report)
1. For [internal requests](https://gitlab.com/gitlab-com/support/internal-requests), you only need to work on the requests that relate specifically to GitLab.com. Many of the other issues as labelled are specific to other areas (such as Console or Licensing).
    1. [ ] Check out the list of [templates](https://gitlab.com/gitlab-com/support/internal-requests/-/tree/master/.gitlab/issue_templates).
    1. [ ] Read the [Servicing Internal Requests workflow](https://handbook.gitlab.com/handbook/support/workflows/internal_requests/) which covers most internal issues.
        1. [ ] (Optional as these are rare) Link to issue:

### User Accounts

Aside from the ones covered in Stage 2, the most common user requests have to do with disabling 2FA and name squatting. However, most of these workflows apply in other cases as well.

1. [ ] When making any changes to a user account, make sure to [use an admin note](https://handbook.gitlab.com/handbook/support/workflows/admin_note/).
1. [ ] For all 2FA disabling requests, use the [Account Ownership Verification](https://handbook.gitlab.com/handbook/support/workflows/account_verification/). This workflow may be used for some other cases as well where we want to verify the ownership.
    - [ ] Link to an account ticket requiring verification:
1. [ ] [Name Squatting Requests](https://handbook.gitlab.com/handbook/support/workflows/namesquatting_policy/) apply to groups and users. Included here since it is often for users.
1. [ ] Except in the cases above, or wherever else there is a clear ask of the action (including impersonating the user), always [ask for permission to take action](https://handbook.gitlab.com/handbook/support/workflows/account_changes/#asking-permission).

### Other Common Requests

1. [ ] [Reinstating blocked accounts](https://handbook.gitlab.com/handbook/support/workflows/reinstating-blocked-accounts/) when users say they they've been mistakenly blocked.
    - [ ] (Optional) Link to a ticket about blocked account:
1. [ ] [Personal Identifiable Information Removal Requests](https://handbook.gitlab.com/handbook/support/workflows/pii_removal_requests/) are uncommon, but read the workflow to understand Support's responsibility.

### Data Requests

1. [ ] Discuss with your manager if you should be added to the group of Support team members working on Data requests.
    - If no, skip this section.
    - If you're unsure, you can post in the `#support_gitlab-com` channel to shadow a Support team member specifically on an Account Deletion request before making a decision.
1. If yes,
    1. [ ] submit a merge request to your [support team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry), changing `works_account_deletion` to `true`. Assign to your manager for approval.
    1. [ ] Submit an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) for "Contributor" access to the [Personal Data Access Request shared Drive](https://drive.google.com/drive/folders/0AA4kcF3prJ6pUk9PVA), tagging `@lyle` for approval and provisioning.
 
[Personal Data Access and Account Deletion Requests](https://handbook.gitlab.com/handbook/support/workflows/personal_data_access_account_deletion/) are created in the [Account Deletion and Other Requests](https://gitlab.com/gitlab-com/gdpr-request) project tracker. Once you've been assigned your first issue, follow the workflow to work on it. Pair with someone else who works on these requests as needed.

1. [ ] Link to issue:


## Stage 6: GitLab.com console (with manager approval)

Depending on your prior experience and readiness, discuss with your manager whether to open the module listed below. Your manager may decide to have you pick this up later once you have more familiarity with GitLab.

1. [ ] Open a module on [GitLab.com Console](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab-com%20Console).

## Final Stage

1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Please submit MRs for this Issue Template with any improvements that you have noticed.
1. [ ] Submit an MR to update your `focuses` slug in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic and the percentage of your time you are dedicated to this Area of Focus. The settings will display on the [Areas of Focus page](https://gitlab-com.gitlab.io/support/team/areas-of-focus.html).

**NOTE:** if you completed Stage 5 (user accounts + admin access) section, you should be working on SaaS Account tickets and admin-related internal requests, and your Area of Focus should be "SaaS 50%" or higher.


/label ~module
/label ~"Module::GitLab-com SaaS Basics"
