---
module-name: "terraform"
area: "Troubleshooting & Diagnostics"
maintainers:
- vstoianovici
- faleksic
---

# Overview

**Goal**: Understand what **Terraform** is and how GitLab Support uses this tool to automate GitLab installation setups.

*Length*: 16 hours

**Objectives**: At the end of this module, you should be familiar with:

- How to setup **Terraform**.
- How **Terraform** works.
- How to write some **Terraform** syntax to provision infrastructure.
- How we use **Terraform** in Support.
- How we use **Terraform** in GitLab (as a means to distribute our product to customers)

## Prerequisites

- You will need to expense [this Terraform course from Udemy](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/)
- Join [#sandbox-cloud-questions](https://gitlab.slack.com/archives/C01EYTW836W) (a place for Sandcloud Cloud specific questions and support)

## Considerations

- The [Terraform Beginner to Advanced - Using GCP](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/) course from Udemy is decent at getting you up and running with Terrafrom. You should follow the entire course but if you're pressed with time I'd use speed 1.5x on the clips and only hit the concepts referenced as links below.

## Stage 0: Create and commit to the module

- [ ] **Done with Stage 0**

1. [ ] Create an issue using this template by making the Issue Title: Terraform - YOURNAME

1. [ ] Add yourself and your trainer as the assignees.

1. [ ] Notify your manager to let them know you've started.

1. [ ] Commit to this by notifying the current experts so that they have a heads up that you are undertaking the module.

1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Basics of Terraform

- [ ] **Done with Stage 1**

1. [ ] What is Terraform: have a look at [this video](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/learn/lecture/14598328#overview) for a short introduction. You'll notice the distinction between Infrastructure as Code (Terraform) and Configuration Management (Ansible, Chef) and how Terraform fits into the DevOps workflow and into the Hashicorp ecosystem.

1. [ ] There's an interesting compilation of Terraform resources in [this article](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca). Just keep this tab open because as you start ramping up and things will start making sense, these resources will provide some extra context to the bare concepts.

1. [ ] Here's [how to install Terraform](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/learn/lecture/14599566#overview) on macOS and Linux.

1. [ ] Here's how [Terraform works](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/learn/lecture/15453934#overview) conceptually.

1. [ ] What is the [Terraform state](https://developer.hashicorp.com/terraform/language/state/purpose)?

1. [ ] If you don't have it yet, create a GCP Cloud Sandbox project:

   1. [ ] Login to [GitLab Cloud Sandbox](https://gitlabsandbox.cloud/) using Okta

   1. [ ] [Create a new individual Cloud Account](https://gitlabsandbox.cloud/cloud/accounts/create?cloud_provider_id=669306fb-8bbc-4033-9e04-daf503bec095&cloud_organization_unit_id=4e49c1d2-ddfc-4c16-87c7-84b91275ff9d) for GCP.

   1. [ ] While there, enable the `Compute Engine` API:  Go to `APIs & Services` in the left pane of the GCP console -> `Dashboard` -> click on `Compute Engine API` -> and then enable it

1. [ ] Now that we've introduced a few of the concepts, let's [set-up your GCP credentials and run your first Terraform script](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/learn/lecture/15453932#overview) in your own GCP project (Created via [GitLab Cloud Sandbox](https://gitlabsandbox.cloud/login)) or in the `support-resources` GCP project. If you choose to do this in the `support-resources` GCP project and don't have the necessary permissions, please ping your trainer.

   The Udemy video is a bit outdated when dealing with service account keys. After heading to <https://console.cloud.google.com/apis/credentials/serviceaccountkey?pli=1> and creating a Service Account as instructed, head over to the **service account details -> Keys -> Add key -> Create new key -> Key type** select **JSON** to create and download the key.

## Stage 2: Terraform Syntax and Concepts

- [ ] **Done with Stage 2**

1. [ ] For more clarity on the following concepts please feel free to also head over to the [HashiCorp docs](https://www.terraform.io/docs/language/):

1. [ ] [This article](https://blog.gruntwork.io/terraform-up-running-2nd-edition-early-release-is-now-available-b104fc29783f) is a very condensed version of the concepts we will be going into bellow, it's great to keep around as a sort of cheatsheet.

1. [ ] Complete the rest of the videos in [Terraform Beginner to Advanced - Using Google Cloud Platform](https://www.udemy.com/course/terraform-beginner-to-advanced-using-google-cloud-platform/learn/) Udemy course. Make sure that you cover the following basics of Terraform Syntax and Concepts:

    - [ ] `Variables`. Using the old way of referencing variables will output a bunch of warnings in the console, so we'll stick with using the the `new way`, meaning no `""` and no `{}`.

    - [ ] `Outputs`.

    - [ ] `List, Counts and Length`.

    - [ ] `Depends on`.

    - [ ] `Locals`.

    - [ ] `Conditionals`.

    - [ ] `Firewall rules`.

    - [ ] `Subnets`.

    - [ ] `Volumes`.

    - [ ] `Modules`.

    - [ ] `Maps`.

    - [ ] `Join`.

    - [ ] `Buckets`.

    - [ ] `Databases`.

    - [ ] `Autoscaling`.

    - [ ] `Kubernetes & VPC Registry`

1. [ ] Watch this [YouTube tutorial](https://youtu.be/PtxtGPxCaQ8) that also uses GitLab to understand the concept of `Workspaces` - and how we would use it to basically have the ability to use multiple state files, but you can also give the [HashiCorp docs](https://www.terraform.io/docs/language/state/workspaces.html) a read. If you need further explanations please also look at this other [free tutorial](https://youtu.be/JmfEKEYdxKU).

1. [ ] Refer to [this free tutorial](https://youtu.be/9cDDZzl7zow) to learn about `Data Sources` in Terraform.

## Stage 3: A deeper understanding of [HackyStack](https://gitlab.com/gitlab-com/business-technology/engineering/tools/hackystack)

- [ ] **Done with Stage 3**

1. [ ] Start by reading the [hackystack README file](https://gitlab.com/gitlab-com/business-technology/engineering/tools/hackystack/-/blob/main/README.md) to understand what it is and what problems it solves.

1. [ ] Read through the [Terraform Environments](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#terraform-environments) handbook page.

1. [ ] Have a look at this [high level presentation of HackyStack and Sandbox Cloud](https://docs.google.com/presentation/d/1kbaub1-ztxGCV7wAzhNWXsH5beIu_HAVGSBjr8-0qTw).
1. [ ] Browse through [Infrastructure-as-Code](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#infrastructure-as-code) section.
    1. [ ] Look into what templates are available in [project-templates](https://gitlab.com/gitlab-com/sandbox-cloud/project-templates).
    1. [ ] Look into what providers are available in [terraform-modules](https://gitlab.com/gitlab-com/sandbox-cloud/terraform-modules).

## Stage 4: Quiz

- [ ] **Done with Stage 4**

   1. [ ] Complete the tasks below, share the requested details for each step, redact credentials, and then ping an expert for review:
      1. [ ] Using Sandbox Cloud [create a new Terraform environment](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#terraform-environments)
         1. [ ] Attach a screenshot of the completed environment creation 
      2. [ ] Plan, Create and Destroy a GitLab instance and a GitLab runner.
         1. [ ] Share output logs of `terraform apply`
         2. [ ] Share output logs of `terraform destroy`
      3. [ ] Plan, Create and Destroy a GitLab instance, in the [Cloud Sandbox](https://gitlabsandbox.cloud/dashboard), with the following changes (see [Additional options for the Gitlab-Omnibus module](https://gitlab.com/gitlab-com/sandbox-cloud/project-templates/support-resources-template/-/blob/main/README.md#additional-options-for-the-gitlab-omnibus-module) for keywords):
         1. [ ] Share output logs of `terraform apply`
         2. [ ] Share output logs of `terraform destroy`
        - GitLab version should be one version older than the current one (for example version 14.2)
        - GitLab instance should be seeded with data (`seed_gl_db` keyword)
        - GitLab instance should use https (`use_https` keyword)

   2. [ ] Once you have completed this, have the expert comment below
          acknowledging your success.

### Penultimate Stage: Review

- [ ] **Done with Review Stage**

You feel confident you've achieved all of the objectives, and now know:

- How to setup **Terraform**.
- How **Terraform** works.
- How to write some **Terraform** syntax to provision infrastructure.
- How we use **Terraform** in Support.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself and perhaps mention it in [this issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/2962). Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

### Final Stage: Completion

- [ ] **Done with this Module**

1. [ ] Have your trainer review this issue. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in Terraform on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

At this point, provided you have some understanding of Ansible, you may be able to tackle the [GET Project](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit). Have a look [here](https://www.youtube.com/watch?v=96D8IwARkRI&ab_channel=GitLabUnfiltered) for a quick introduction or commit to the [GET Training module](https://gitlab.com/gitlab-com/support/support-training/-/blob/main/.gitlab/issue_templates/GitLab%20Environment%20Toolkit.md).

If, however, you'd like to learn more about Ansible please start the Ansible Module (check progress Ansible module creation in [issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/2965)).

/assign me
/label ~module
/label ~"Module::Terraform"
