---
module-name: "SSAT Reviewing Manager"
area: "Customer Service"
maintainers:
  - rspainhower
---

## Introduction

Welcome to the SSAT Reviewing Manager training module!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and public](https://about.gitlab.com/handbook/values/#transparency).**

**Goals of this checklist**

At the end of the checklist, manager should

- Understand SSAT surveys reviewing responsibilities
- Understand how to respond to feedback, and how to utilize the feedback given
- Be able to participate in the SSAT Reviewing Manager on-call rotation

**General Timeline and Expectations**

- Read about our [Support Onboarding](https://handbook.gitlab.com/handbook/support/training/#support-manager-onboarding-pathway) process, the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.

### Stage 0: SSAT Surveys

An important part of GitLab Support Managers' responsibilities is evaluating and responding to feedback provided by our customers. Our customers' voices matter, and Support Satisfaction (Customer Satisfaction) is one of the ways we measure our performance.

* [ ] Read about the [ways we measure our performance](https://handbook.gitlab.com/handbook/support/#how-we-measure-our-performance).
* [ ] Review our SSAT KPI in the [performance indicators handbook](https://handbook.gitlab.com/handbook/support/performance-indicators/#support-satisfaction-ssat).

### Stage 1: SSAT Feedback Issues

Through a ZenDesk automation, we invite the customer to complete our SSAT survey shortly after we close their ticket. If the customer completes and returns a survey, further ZenDesk automation generates a `feedback issue`, and then managers review both `Good` and `Bad` feedback issues for potential action. 

One of the core factors of our design of this system is to get the feedback *into GitLab the software* where GitLab teams collaborate. This allows us to leverage issue cross-linking and to surface feedback directly into the sales/product/engineering cycle. Using GitLab to handle SSAT Feedback Issues avoids the "feedback in a void" problem, in which customers' responses are not provided to the teams that could actually take action.

* [ ] Read about our [ZenDesk Triggers](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_triggers/).

### Stage 2: Handling SSAT Feedback Issues

All Support Managers (excluding Senior Support Managers and above) participate in a weekly PagerDuty rotation, and are responsible for taking actions on the `Good` and `Bad` SSAT Feedback Issues.

* [ ] Study the [Support Workflows handbook](https://handbook.gitlab.com/handbook/support/workflows/how-to-respond-to-feedback/) to learn how Support Managers handle SSAT Feedback Issues. 
* [ ] Return to your Support Manager Onboarding Issue for instructions on joining the rotation.

#### Congratulations on completing the SSAT Reviewing Manager module successfully!

/label ~onboarding
/label ~"Module::SSAT Reviewing Manager"
