---
module-name: "Support Manager Basics"
area: "Customer Service"
maintainers:
  - rspainhower
---

## Introduction

Welcome to the Support Manager focused onboarding issue!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and public](https://about.gitlab.com/handbook/values/#transparency).**

**Goals of this checklist**

At the end of the checklist, you will be
- able to understand the existing support manager workflows and processes
- aware of the company resources available to effectively manage your team, and to motivate, coach, mentor and guide your team
- able to access and understand the support metrics and dashboards
- able to understand our interview process and tools

**General Timeline and Expectations** 

- This issue should take you **two weeks to complete**.
- Stages of this module can be done in any order (please before starting go through the list to get a better idea on the steps)
- Note that, as a manager, you will be pulled into day to day requirements and activities of your team from as early as week 3 or as soon as you have your team transitioned to you. 

## Stage 0: Support Manager Workflows & Processes

1. [ ] If not done already, have your on-boarding buddy invite you to the private slack channels #spt-hiring-mgmt, and #spt_managers-internal.
1. [ ] Learn how Support Management at GitLab [keeps track of the issues and projects](https://handbook.gitlab.com/handbook/support/managers/#how-to-see-what-the-support-managers-are-working-on) that we are working on.
1. [ ] To ensure we are in constant communication with our counterparts in other regions, Support Management runs cross-regional sync meetings weekly. Read about the [purpose and recommended procedure to run these](https://handbook.gitlab.com/handbook/support/managers/#support-leadership-meetings).
1. [ ] Read about the [Support Engineer Responsibilities](https://handbook.gitlab.com/handbook/support/support-engineer-responsibilities/) to understand what the focus areas will be for your team and how you can best support them to excel in those areas.
1. [ ] Read about [the tools](https://handbook.gitlab.com/handbook/support/managers/#metrics) we use in Support Management to generate metrics.
   1. [ ] Login to [Zendesk Explore](https://gitlab.zendesk.com/explore/) and review the Support Metrics dashboard.
   1. [ ] Engage in two 1-1 pairing sessions with support managers - one in your region and one out - to learn about what reports they use regularly and why it is useful to them.
1. [ ] Learn how we interact with the [Support Operations team](https://handbook.gitlab.com/handbook/support/support-ops/#introduction-to-support-operations-team-support-ops) and arrange a coffee chat with the Support Operations Manager or their manager. 
1. [ ] Understand your responsibilities for reviewing and merging troubleshooting documentation
   1. [ ] Read up on the [historical context](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1765#background) behind why Support Managers have maintainer access to production repos.
   1. [ ] **With Great Power Comes Great Responsibility:** Maintainer access gives you the ability to merge any MR in a repository. Please understand that you should [only merge MRs that make changes to the troubleshooting sections](https://docs.gitlab.com/ee/administration/index.html#support-team-documentation) in the documentation; otherwise general doc MRs should be assigned to the [relevant technical writer](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers) and code MRs should follow the [code review process](https://docs.gitlab.com/ee/development/code_review.html).
   1. [ ] Read the [Support Team Documentation](https://docs.gitlab.com/ee/administration/index.html#support-team-documentation) of the GitLab Administrator documentation
   1. [ ] Create an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to request `maintainer` access to the following repos: [gitlab](https://gitlab.com/gitlab-org/gitlab), [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab), [gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner), [charts/gitlab](https://gitlab.com/gitlab-org/charts/gitlab). 
       * Use the following justification: [Support managers should have merge access to repos housing docs](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1769)
1. [ ] Learn about the Manager On-Call rotations and expectations.
   1. [ ] Read about the [Support Manager On-Call](https://handbook.gitlab.com/handbook/support/on-call/#manager-on-call) process.
   1. [ ] Learn about the [SSAT Feedback Issues](https://gitlab.com/gitlab-com/support/feedback/-/tree/master) and the [SSAT Reviewing Manager](https://handbook.gitlab.com/handbook/support/workflows/how-to-respond-to-feedback/) responsibilities.
   1. [ ] Have a discussion with your manager on when to add yourself to these rotations on PagerDuty. 
      - Typically, you should be ready to be added to the SSAT rotation by the beginning of your 5th week here.
      - At the same time you can be added to the On-call shadow rotation.
1. [ ] Arrange to join the Manager On-Call rotations.
   1. [ ] Support Manager On-Call
      1. 1. [ ] Create a new Issue in the [support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta) Project requesting you be added to the `Support Manager On-Call` PagerDuty rotation for your region. 
      1. [ ] Assign the Issue to your Manager for approval and implementation.
   1. [ ] SSAT Reviewing Manager
      1. [ ] Create a new Issue in the [support-team-meta](https://gitlab.com/gitlab-com/support/support-team-meta) Project requesting you be added to the `SSAT Reviewing Manager` PagerDuty rotation for your region. 
      1. [ ] Assign the Issue to your Manager for approval and implementation.
      1. [ ] Locate your PagerDuty ID by navigating to your profile and noting the 7-character unique string at the end of the URL: `https://gitlab.pagerduty.com/users/AAAAAAA`
      1. [ ] Add your PagerDuty ID to the following file, following instructions [here](https://gitlab.com/gitlab-com/support/feedback/-/tree/master#how-it-works): `https://gitlab.com/gitlab-com/support/feedback/-/blob/master/managers.yml`


### Stage 1: Get to know your team members!

Try to get through this section **before** you have your team transitioned to you. If that happened before you got to this stage, don't worry! You can always come back, refer and iterate.

1. [ ] Review the [Getting to Know Your Team Members](https://handbook.gitlab.com/handbook/support/managers/getting-to-know-you/) handbook page. This page will help you get prepared for the transition of your new team members. Use it as a guideline as you work with the manager who is transitioning team members over to you.
1. [ ] 1:1s are an important part of fostering the relationship between you and your team member. Review the [Support 1:1s page](https://handbook.gitlab.com/handbook/support/managers/support-1-1s/) for useful pointers and recommendations on how you can make 1:1s more efficient for you and your team member. Remember, these are only guidelines and you should tailor them based on individual requirements and expectations.
1. [ ] Get a glimpse of your peers' 1-1 documents to have a better idea on what to include.

## Stage 2: Hiring Tools & Processes

Talk to your onboarding buddy to understand what hiring is happening right now. They will let you know about timelines and give you an overview of when you can start participating in interviews.

1. [ ] Complete the [support interview training issue](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Module%20-%20Support%20Interview%20Training) to learn about how assessments and technical interviews are conducted. Reach out to People Ops if it was not created for you. 
1. [ ] On completion, raise an MR to have yourself added to the appropriate section of the [Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md#apac-hiring-managers).  The Talent Acquisition team use this to select interviewers for the various stages of the recruiting.

## Stage 3: Support Focuses and Learning Pathways

Support Engineers have different sets of tasks to perform to help our customers depending on their primary focus. As a manager, it is important to understand this as it will help you guide your team on prioritization and load balancing. Though you will learn about this through different channels and mediums as you progress through onboarding, take a look at the [area of focus onboarding pathway content for support engineers](https://handbook.gitlab.com/handbook/support/training/#support-engineer-onboarding-pathway) to get a quick idea of the work involved in the different focuses:

(Note: It is not mandatory for you to complete these modules - however, feel free to do so if you are interested!)

1. [ ] Review the content under the [SaaS & Self Managed Support Learning Pathway](https://handbook.gitlab.com/handbook/support/training/#gitlab-support-learning-pathway).
1. [ ] Review the content under the [License and Renewals Learning Pathway](https://handbook.gitlab.com/handbook/support/training/#license-and-renewals-learning-pathway).

## Stage 4: Pair with Support Engineers!

- Perform at least **5 pairing sessions** with Support Engineers - pair with at least 2 SaaS engineers, 2 Self-managed engineers, and 1 License & Renewals engineer. These sessions will help you understand the different workflows in action, [Support Global Groups](https://handbook.gitlab.com/handbook/support/support-global-groups/#introduction-to-support-global-groups), and how we manage tickets and customers day to day. They might also give you ideas for process improvements! You can complete this stage as you work through the other onboarding modules in your pathway. Identify people with these skills in our [Areas of Focus website](https://gitlab-com.gitlab.io/support/team/areas-of-focus/).
- Consider people outside of your immediate region to avoid silos.

1. [ ] Familiarize yourself with the `#spt_pairing` Slack channel, the [Support Pairing Project](https://gitlab.com/gitlab-com/support/support-pairing), and the [summary of pairings page](https://gitlab-com.gitlab.io/support/support-pairing/).
1. [ ] Create a Google Calendar event, inviting that person. If you're unsure of times due to timezones, feel free to send them a message in Slack first. When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing/-/issues) and use the appropriate template for the call. 
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
   1. [ ] Call with ___; issue link: ___
1. [ ] Shadow at least 2 emergencies with Support Engineers. This will help in understanding the workflow, and also give a first hand experience on how GitLab handles emergencies. You can do this by one of the following methods:
- Look out for Emergency PagerDuty notifications in the #support_self-managed slack channel, or,
- Request SEs in your region to tag you when they get paged for an emergency, or,
- Consider joining a shadow rotation and getting paged right along with the [Support Engineer On-Call](https://handbook.gitlab.com/handbook/support/on-call/#how-support-on-call-works) - your onboarding buddy can help you with this
Add the emergency ticket links below:
   1. [ ] `Ticket Link`
   1. [ ] `Ticket Link`

## Stage 5: Dealing with Imposter Syndrome thoughts whilst working remotely at GitLab

We appreciate that for some new hires working remotely at GitLab may appear initially challenging and as a consequence of this some employees may experience feelings of imposter syndrome. As such we we would encourage you to:

1. [ ] Watch the following video where [support managers discuss feelings of imposter syndrome whilst working at GitLab](https://drive.google.com/file/d/1KVtni9ZNbyL2CmL9ScD3hm2v1wZ8NM3z/view)
1. [ ] Watch the L&D team host a discussion with Sid (GitLab CEO) [on mental health and addressing imposter syndrome](https://www.youtube.com/watch?v=od_KdZqc69k)
1. [ ] Read the following blog on [how to tackle impostor syndrome while working remotely](https://about.gitlab.com/blog/2020/09/02/imposter-syndrome-and-remote-work/)
1. [ ] Check in with your manager at the 3 months and at 6 month interval to discuss any thoughts or concerns you may have regarding feelings of imposter syndrome

## Congratulations! You made it!

You are now ready to continue on your onboarding path to tackle the next module in line. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [training page](https://handbook.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under your Onboarding pathway!

Please also submit MRs for any improvements that you can think of! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/main/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::Support Manager Basics"
