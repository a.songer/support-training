---
module-name: "DAST"
area: "Product Knowledge"
gitlab-group: "Sec:Secure"
maintainers:
  - [lyle]
  - [kategrechishkina]
---

### Overview

**Goal**: 

**Objectives**: 

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: DAST - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: DAST
     level: 2
   ```

### Stage 1: DAST? Huh??

#### Dynamic Application Security Testing (DAST)

##### What is it?

Dynamic Application Security Testing (DAST) simulates real-world attacks on a customer's deployed application to identify potential vulnerabilities and weaknesses. This is normally done on a review app or pre-prod environment. As scans can perform any function a user can and may trigger bugs that may modify or cause the loss of production data, we do not advise use on a production environment.

##### What customer problem does it solve?

- DAST can detect issues that are difficult or impossible to identify with static analysis, such as runtime errors, injection attacks, web-server configuration issues, and other types of dynamic threats. 
- Identification of zero-day vulnerabilities: DAST can identify previously unknown vulnerabilities that may not be addressed by traditional security testing methods. GitLab offers [Coverage-guided fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/) and [Web API Fuzz Testing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) to enable this.
- Integration into CI/CD pipelines allow for continuous monitoring of the application's security as it evolves over time.
- Similar to SAST, integration into a single application means that these scans happen earlier in the software development process (before they're exploited in the wild).

### Stage 2: How do these scanners work?

- [ ] GitLab has a number of offerings that enable customers to do Dynamic Testing against their projects. Read [Dynamic Application Security Testing (DAST)](https://docs.gitlab.com/ee/user/application_security/dast/) documentation page to learn what DAST solutions GitLab provides for application and web API testing.

#### DAST for applications

1. [ ] GitLab provides two DAST analyzers that can be used to scan your application for known vulnerabilities. Skim through the following documentation pages:
    - [ ] [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html)
    - [ ] [DAST proxy-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/proxy-based.html), [support will be removed in 17.0](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136191).
1. [ ] Most of the time customers will want to do DAST against authenticated applications. Learn how to configure [DAST to authenticate to your application](https://docs.gitlab.com/ee/user/application_security/dast/authentication.html).
    - [ ] Watch [DAST - Configuring Auth with Azure AD SSO](https://www.youtube.com/watch?v=5erk6ecbARE&list=PL05JrBw4t0KoFGl1qSAuzSua5t8k2U2Ol&index=5) demo to see how to configure DAST authentication for websites that use SSO authentication flow. 
    - [ ] Customers often reach out to us with DAST authentication issues. We have a good selection of known issues and techniques covered in [troubleshooting authentication](https://docs.gitlab.com/ee/user/application_security/dast/authentication_troubleshooting.html#troubleshooting) document. Give it a read to familiarize yourself with what errors customers might be dealing with when configuring authenticated DAST scan. The first thing you'll want to do is collect an [authentication report](https://docs.gitlab.com/ee/user/application_security/dast/authentication_troubleshooting.html#configure-the-authentication-report) to understand if analyzer is able to find the fields and submit the details.
1. [ ] DAST for websites includes crawl as part of the scan. This stage starts after authentication. Learn how [Crawling an application](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html#crawling-an-application) is performed with [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html).
1. [ ] Learn about vulnerability checks available with [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/checks/) and find out [why browser-based analyzer findings are different from proxy-based analyzer](https://gitlab.com/gitlab-org/gitlab/-/blob/v16.6.0-ee/doc/user/application_security/dast/browser_based.md#why-browser-based-scans-produce-different-results-to-proxy-based-scans).
1. [ ] If you are interested in learning about web vulnerabilities, go through this free online course: [Server-side vulnerabilities](https://portswigger.net/web-security/learning-paths/server-side-vulnerabilities-apprentice). After completing their hands-on labs you should understand better how DAST analyzers perform attacks.
1. [ ] Watch [Demo of browser-based DAST check 22.1](https://www.youtube.com/watch?v=suhuke9hKtg) (Be sure to switch to the Gitlab Unfiltered Youtube account).
1. [ ] Bookmark [DAST demo projects](https://gitlab.com/gitlab-org/security-products/demos/dast/), use these applications to quickly test DAST with CI/CD. These projects use [Docker Services](https://docs.gitlab.com/ee/user/application_security/dast/#docker-services) to deploy applications.
1. [ ] [dast project](https://gitlab.com/gitlab-org/security-products/dast) is hosting the analyzer's code. Start your code dives here if you need it to help the customer. For now, bookmark the project.
1. [ ] Watch [Support DAST Deep Dive Session](https://youtu.be/-WeA12bl-Iw). Make sure to log in using Unfiltered YouTube channel. The video covers what DAST offerings GitLab has. It also goes into details on how to do DAST against a website with [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html) and troubleshoot failing scans. You can also review the [presentation deck](https://docs.google.com/presentation/d/1F7F8d86cNVfpUz8Id_IGfIZETr4f_wgVmHGp050S7Gg).
1. [ ] Give [DAST Browser-Based Troubleshooting doc](https://docs.gitlab.com/ee/user/application_security/dast/browser/troubleshooting.html) a read to familiarize yourself with known issues and troubleshooting techniques that will be helpful when dealing with customer tickets about DAST.
1. [ ] **Checkpoint \#1:** Leave a note documenting any [TIL](https://knowyourmeme.com/memes/today-i-learned-til) facts, techniques, commands your learned while working through this module so far.
1. (Optional) Review [Browserker shorts collection](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/tree/main/docs/shorts?ref_type=heads) to understand the internals of browser-based DAST scanner.

Let us learn by doing and run some DAST jobs:
1. - [ ] Fork [DVWA demo project](https://gitlab.com/gitlab-org/security-products/demos/dast/dvwa) to an ultimate-level group and configure the job to:
    - [ ] use [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html)
    - [ ] make it run authenticated scan with [authentication report enabled](https://docs.gitlab.com/ee/user/application_security/dast/authentication.html#configure-the-authentication-report)
    - [ ] enable [Active scan](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html#active-scans), use `DAST_BROWSER_INCLUDE_ONLY_RULES` variable to select these rules: `89.1,78.1,548.1`
    - [ ] when the job completes explore [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/), authentication report, job log and [Pipeline Security Tab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html)
    - [ ] share the project link in a comment
1. [ ] Run DAST against your test GitLab installation. 
    - [ ] Make sure you create a separate user to authenticate during the scan.
    - [ ] If you run active, authenticated scan, it might timeout against GitLab app, limit the number of rules you test for if that happens and tweak some of the settings we recommend in [managing scan time](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html#managing-scan-time).
1. [ ] You can also use [docker containers](https://handbook.gitlab.com/handbook/support/workflows/test_env/#docker) to start test applications locally and run the analyzer manually. Try it with [Juiceshop](https://owasp.org/www-project-juice-shop/):
    - [ ] `docker run --rm -p 3000:3000 bkimminich/juice-shop`
    - [ ] `docker run --env "DAST_WEBSITE=http://localhost:3000" --network host --rm -v "${PWD}":/zap/wrk registry.gitlab.com/security-products/dast /analyze`
1. [ ] **Checkpoint \#2:** Leave a note documenting any [TIL](https://knowyourmeme.com/memes/today-i-learned-til) facts, techniques, commands your learned while working through this module so far.

#### DAST of web APIs

1. [ ] [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/) is GitLab's offering that helps customers do DAST testing on their web APIs.
1. [ ] Watch [DAST REST API Scans](https://www.youtube.com/watch?v=NxaGmrBpRB0&list=PL05JrBw4t0KqugiR7wybHAL7Ww2UbiJnz&index=3) demo to understand how DAST API is configured for basic API applications. The video was recorded when we used ZAP for API scans so the variables used in this demo might be outdated, but it provides a good overview of what is need to run DAST API scan.
1. [ ] Web APIs can be described with different API description formats, [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/) requires customers to specify which format their API is described with. Check out [the current supported formats](https://docs.gitlab.com/ee/user/application_security/dast_api/).
1. [ ] DAST API can also be authenticated. Check out [Authentication](https://docs.gitlab.com/ee/user/application_security/dast_api/configuration/customizing_analyzer_settings.html#authentication) section to learn what authentication methods are supported. 
    - [ ] DAST API can be configured to work with short-lived authentication tokens (tokens that might not be valid for the duration of the scan). Watch [Configure DAST API with a short lived API token](https://www.youtube.com/watch?v=ZV0dKK2qFp8&list=PL05JrBw4t0KqugiR7wybHAL7Ww2UbiJnz&index=7) to learn more.
1. [ ] Bookmark [api-fuzzing-src](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src) project, you might need it to dive into analyzer's code when helping customers.
1. [ ] A frequent customer request about DAST API is that their scan is taking a long time to run. Start by going through [Performance tuning and testing speed](https://docs.gitlab.com/ee/user/application_security/dast_api/performance.html#performance-tuning-and-testing-speed) document to see which of scenarios described there might be applicable to your customer.
1. [ ] Other errors customers report with the DAST API are caused by using invalid schema API specification file, incorrect target URLs or a bug in the analyzer. Review [Troubleshooting](https://docs.gitlab.com/ee/user/application_security/dast_api/troubleshooting.html#troubleshooting) to see more examples.
1. [ ] **Checkpoint \#3:** Leave a note documenting any [TIL](https://knowyourmeme.com/memes/today-i-learned-til) facts, techniques, commands your learned while working through this module so far.

##### On-demand DAST

1. [ ] DAST scans (both regular and API) are often long-running jobs. Because of that customers often want to be able to run them on-demand or on a schedule. To meet this need, GitLab allows running [DAST On Demand Scans](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#dast-on-demand-scan). Give this documentation a quick read.
1. [ ] There are two components that customers need to configure for an on-demand scan: [Site profile](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#site-profile) and [Scanner profile](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#scanner-profile). When configuring a [Site profile](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#site-profile), customers can configure their website as _API_ or a _website_ which is changes the underlying analyzer that is used to run the scan: [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/) for API site profiles or [DAST proxy-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/proxy-based.html). We are going to replace [DAST proxy-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/proxy-based.html) with [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser/#dast-browser-based-analyzer) in  17.0 for on-demand website scans ([rollout issue](https://gitlab.com/gitlab-org/gitlab/-/issues/431139)).
1. [ ] To be able to run active scan, the selected site profile should be validated. Read about [Validation options that are available](https://docs.gitlab.com/ee/user/application_security/dast/on-demand_scan.html#validate-a-site-profile).
1. [ ] To get your hands dirty, configure and run on-demand DAST job against your test GitLab application. Share a link to the job in the comment.

#### Fuzz testing options

Fuzzing is a technique where an automation hits the application attempting to cause crashes or bugs. To achieve this the analyzer sends randomized inputs to try trigger application to behave unexpectedly. GitLab provides two features for customers to enable fuzzing:

1. [ ] [Web API Fuzz Testing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) to fuzz their REST APIs. This analyzer uses the same engine as [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/) - [api-fuzzing-src](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src).
    - [ ] While [DAST API](https://docs.gitlab.com/ee/user/application_security/dast_api/) scans for known vulnerabilities, [Web API Fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) tries to find new ones. 
    - [ ] [Authentication](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/#authentication) and [API types](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/#enable-web-api-fuzzing) are configured very similar to how it is done with DAST REST API Scans. Note that variable names are different.
    - [ ] All configurations to the scan behavior should be done by checking out a `.gitlab/gitlab-api-fuzzing-config.yml` file to the repository. The customers can work off of [the default one](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing/-/blob/master/gitlab-api-fuzzing-config.yml) and adjust the behavior of checks and profiles.
    - [ ] Bookmark [API Fuzzing demo projects](https://gitlab.com/gitlab-org/security-products/demos/api-fuzzing)
    - [ ] [Performance tuning](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/performance.html), [Troubleshooting](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/troubleshooting.html#troubleshooting) techniques, and known issues of DAST API Fuzzing are very similar to those of [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/#dast-api-analyzer) because the underlying analyzer is the same for both of them.
1. [ ] [Coverage-guided fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/) is the second feature that enables customers to do fuzz testing in a CI job. It requires customers to instrument their applications.
    - [ ] Customers have an option to select from a number of [Supported fuzzing engines and languages](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#supported-fuzzing-engines-and-languages).
    - [ ] We provide [Coverage Fuzzing demo projects](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing) to get them started with instrumenting their applications. Make sure to explore some of these and bookmark them.
    - [ ] In addition to scan report (`gl-coverage-fuzzing-report.json`), Coverage-guided CI job should also [export an artifact](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#output) containing information about the recorded crashes and the corpus of the scan.
    - [ ] **Corpus** is a set of inputs that were used against a fuzz target during CI job. It can reused in subsequent jobs and it can also be stored in [Corpus registry](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#corpus-registry) in GitLab.
    - [ ] Go though the [Perform fuzz testing in GitLab](https://docs.gitlab.com/ee/tutorials/fuzz_testing/) tutorial on a test project and add a link to a successful job as a comment to this training issue.
    - [ ] The main requirement that customers should meet with Coverage-guided fuzz testing is that they should instrument their application. How one does that depends on [Fuzzing Engine](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#supported-fuzzing-engines-and-languages) used. Troubleshooting and helping with instrumenting customer applications often falls [outside of scope of support](https://about.gitlab.com/support/statement-of-support/#out-of-scope). However, we can always use [Coverage Fuzzing example projects](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing) to help customer get started.
    - [ ] You might always find yourself helping customers upload or download corpus as part of Coverage-guided fuzz testing job. There is a small [Troubleshooting](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#troubleshooting) section you can start your troubleshooting.
1. [ ] **Checkpoint \#4:** Leave a note documenting any [TIL](https://knowyourmeme.com/memes/today-i-learned-til) facts, techniques, commands your learned while working through this module so far.
 
### Stage 3: Tickets and RFH issues

1. [ ] Review 5 most recent closed [request-for-help issues raised with Dynamic Analysis group](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/?sort=created_date&state=closed&label_name%5B%5D=Help%20group%3A%3Adynamic%20analysis&first_page_size=100). Provide concise summary of the root cause and troubleshooting techniques used in each issue (use this issue's comments):
    - [ ] [issue ID](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/ID) - [link-to-comment]()
    - [ ] [issue ID](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/ID) - [link-to-comment]()
    - [ ] [issue ID](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/ID) - [link-to-comment]()
    - [ ] [issue ID](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/ID) - [link-to-comment]()
    - [ ] [issue ID](https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/ID) - [link-to-comment]()

1. [ ] Work 2 DAST tickets to resolution
    - [ ] [id](https://gitlab.zendesk.com/agent/tickets/ID)
    - [ ] [id](https://gitlab.zendesk.com/agent/tickets/ID)

### Penultimate Stage: Review

- [ ] After completing this module, you should know very well what DAST is and what analyzers GitLab has to enable DAST on customer projects.
- [ ] You should know how these analyzers can be configured: [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html), [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/), [Web API Fuzz Testing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/).
- [ ] You should have a good selection of test projects to work with when helping customers with their DAST-related issues/questions. 
- [ ] Review the comments you left on this issue to refresh the memory of the knowledge you acquired while working this module.
- [ ] If you know how this module can be improved, raise an MR with your suggestions.

### Final Stage: Completion

1. [ ] Have your trainer review this issue. If you do not have a trainer, ask a support code contributor to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit an MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).
1. [ ] Consider also taking the [Documentation training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Documentation.md) which focuses on GitLab docs changes.

/label ~module
/label ~"Module::DAST"
/assign me
