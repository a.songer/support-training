---
module-name: "SCIM"
area: "Product Knowledge"
gitlab-group: "Manage:Access"
maintainers:
  - alvin
---

## Overview

Before tackling this module, you will need to first complete the SAML module. Remember to contribute to any documentation that needs updating.

**Goal**: Set a clear path for SCIM Expert training

**Objectives**: At the end of this module, you should be able to:
- Understand how to set up SAML and SCIM apps for SSO for Groups
- Troubleshoot customer's issues with SAML and SCIM

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: SCIM - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what SCIM is

- [ ] **Done with Stage 1**

1. [ ] Read through the [GitLab SCIM provisioning using SAML SSO for Groups Documentation](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html).
1. [ ] Read through the [Configure SCIM for self-managed GitLab instances Documentation](https://docs.gitlab.com/ee/user/admin_area/settings/scim_setup.html#configure-scim-for-self-managed-gitlab-instances)
1. [ ] Watch the [Support SCIM Deep Dive](https://youtu.be/4OxDhtR_WYM) (recorded December 2020), with optional [APAC Q&A](https://youtu.be/SUegvRltdCA) and [EMEA Q&A](https://youtu.be/VS8oRwfNfrQ) sessions. You can also [access the SCIM Deep Dive slides](https://docs.google.com/presentation/d/1ZceIBJFuWzWMtRMsCSVnoKOHbVFo4TWHegKdNVacD8g/edit).
1. [ ] Read through the [Overview of SCIM](https://scim.cloud/#Overview) at `scim.cloud`

## Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Create a test app on a cloud provider IdP where we support SCIM and connect it either with your GDK or a GitLab.com group.
    1. [ ] The [infrastructure for troubleshooting workflow page](https://handbook.gitlab.com/handbook/support/workflows/test_env/#azure-testing-environment) has info on getting access to Azure and Okta. Alternatively, you can create a trial account on one of the platforms.
   Although trying one IDP should be enough, please consider trying both Azure and for this exercise as most questions are about them.
       1. [ ] OKTA setup
       1. [ ] Azure setup
    1. [ ] If using GitLab.com, ensure your GitLab.com test group has Silver or Gold plan. If needed, create an access request to get a test group upgraded, or ask team members for access to an existing one.
    1. [ ] Follow the documentation to set up SAML and SCIM. The [specific provider sections](https://docs.gitlab.com/ee/user/group/saml_sso/#providers) link to a demo video if one exists. If we have SCIM for an IdP where no such video exists, consider contributing one!
    1. [ ] Ensure you are familiar with the [troubleshooting section](https://docs.gitlab.com/ee/user/group/saml_sso/#troubleshooting) in order to know what common cases are documented.

## Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Customer call (optional)

- [ ] **Done with Stage 4**

We do not often get calls focused on SCIM, so this is an optional section.

1. [ ] Pair on or perform two calls, where a customer is having trouble with SCIM.
   1. [ ] call with ___
   1. [ ] call with ___

## Stage 5: Assessment

- [ ] **Done with Stage 5**

1. [ ] Complete [the self-assessment](https://forms.gle/iMtYbWuR9eBXHiTR6). Assessments are stored in the [Support Team Drive Training/Training Module Assessments folder](https://drive.google.com/drive/u/0/folders/147D5ecSDsV2J9OCN_6t-J4vYzPLTebue). If you have any questions about the assessment, please ask your trainer or an expert.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in SCIM on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::SCIM"
/label ~"group::authentication and authorization" 
/assign me