---
module-name: "Helm and GitLab Charts"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
status: Draft
maintainers:
  - ckaburu
---


## Overview

This is a continuation in the Kubernetes Series and we will be focusing on using Helm to install GitLab on Kubernetes

**Goal**: Set a clear path for the start of your Helm journey.

**Objectives**: At the end of this module, you should be able to:
- Install Helm 3
- Create Helm charts
- Understand Helm templates
- Add Helm dependencies to existing charts
- Configure and pass custom values to the charts
- Installing & Upgrading GitLab with Helm

## Stage 0: Pre-Requisites

1. [ ] kubernetes-part-1
2. [ ] kubernetes-part-2

## Stage 1: Background Knowledge

This stage is meant to provide you with detailed background knowledge to get you started.

**Helm Charts**
1. [ ] [Kubernetes: Package Management with Helm](https://www.linkedin.com/learning/kubernetes-package-management-with-helm/manage-kubernetes-workloads-with-helm?)
1. [ ] [Building Helm Charts From the Ground Up: An Introduction to Kubernetes](https://www.youtube.com/watch?v=vQX5nokoqrQ)
1. [ ] [Helm from basics to advanced](https://banzaicloud.com/blog/creating-helm-charts/)
1. [ ] [Helm 3, the Good, the Bad and the Ugly](https://banzaicloud.com/blog/helm3-the-good-the-bad-and-the-ugly/)
1. [ ] [Continuous Deployment with Helm](https://www.youtube.com/watch?v=HzJ9ycX1h0c&t=1490s)
1. [ ] [The Chart Template Developer’s Guide](https://helm.sh/docs/chart_template_guide/getting_started/)
1. [ ] [Helm Chart Patterns](https://www.youtube.com/watch?v=WugC_mbbiWU)

**GitLab Cloud Native Charts**
1. [ ] [Architecture](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/architecture/index.md)
1. [ ] [Deployment Guide](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/deployment.md)
1. [ ] [Storage Guide](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/storage.md)
1. [ ] [Troubleshooting](https://gitlab.com/gitlab-org/charts/gitlab/tree/master/doc/troubleshooting)
1. [ ] [Known Limitations](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/index.md#limitations)

## Stage 2: Hands-on Exercises
These exercises are meant to give hands-on experience. They can be accessed by signing-up to [O'Reilly platform](https://www.oreilly.com/)  with your personal email (gitlab domain emails will redirect to sign-in with Okta, and fail with "You're not assigned to this application" ). Monthly subscription cost is reimbursable as per this [issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4453#note_1118168603)

**Basics**
1. [ ] [Helm 3 Foundation](https://learning.oreilly.com/scenarios/building-a-helm/9781492095651/) 

**Intermediate**

GitLab installation and upgrades:

1. [ ] [Google Kubernetes Engine](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/installation/cloud/gke.md) [Demonstration](https://youtu.be/XcJqIggsJ5E)
1. [ ] [Azure Kubernetes Service](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/installation/cloud/aks.md)
1. [ ] [Amazon EKS](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/installation/cloud/eks.md)
1. [ ] [Oracle Container Engine for Kubernetes](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/installation/cloud/oke.md)
1. [ ] [Customizing your installation](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/charts/globals.md)

**Advanced**
1. [ ] [Using the Grafana Chart](https://docs.gitlab.com/charts/charts/gitlab/gitlab-grafana/index.html)
1. [ ] [Setup the Pages Chart](https://docs.gitlab.com/charts/charts/gitlab/gitlab-pages/index.html)
1. [ ] [Using the Container Registry](https://docs.gitlab.com/charts/charts/registry/index.html)

## Stage 3: Pairing Sessions

- [ ] **Done with Stage 3**

The essence of these pairing sessions is to learn together with other team members while solving customer Helm-related tickets. 

1. [ ] Mention 5 pairing sessions you participated in, share key takeaways in the comments area below (if any).
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Tickets

- [ ] **Done with Stage 4**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Contribute valuable responses on at least 5 Helm-related tickets, even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Final Stage

1. [ ] Have your trainer review your tickets. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as Helm Expert on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::Helm and GitLab Charts"
/assign me
