---
module-name: "Continuous Integration"
area: "Product Knowledge"
gitlab-group: "Verify:Continuous Integration"
maintainers:
  - manuelgrabowski
---

**Goal:** Set a clear path for GitLab-CI training

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GitLab CI - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab CI questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: CI
     level: 1
   ```

## Stage 1: Become familiar with what GitLab CI is

- [ ] **Done with Stage 1**

1. [ ] GitLab University
   1. [ ] [CI/CD concepts](https://docs.gitlab.com/ee/ci/introduction/index.html)
   1. [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)
   1. [ ] [10 Most Common Errors In GitLab CI Pipelines](https://medium.com/devops-with-valentine/top-10-most-common-errors-in-gitlab-ci-ce5d206e8c03)
1. [ ] [GitLab CI/CD Training Video by Jose Tores](https://drive.google.com/file/d/0B5OISI5eJZ-DSzA5THdvMG9Jbm8), see also [Slides](https://gitlab-org.gitlab.io/ci-training-slides/#/) and [Example App](https://gitlab.com/gitlab-org/ci-training-sample).
1. [ ] [GitLab CI/CD end user architecture walkthrough video](https://youtu.be/PGyhBwLyK2U?t=2270) – relevant part runs from 37m:50s - 43m:13s.
1. [ ] [GitLab CI/CD backend architecture walkthrough video](https://youtu.be/ew4BwohS5OY) – watch to learn how everything connects together behind the scenes, see also the [diagram](https://drive.google.com/file/d/1LFl-KW4fgpBPzz8VIH9rsOlAH4t0xwKj/view) used in the video.
1. [ ] [Watch Advanced CI/CD Workshop](https://drive.google.com/drive/folders/1v1VubOu5VCsqphOa3OiczPAd_xS4Ooaw) – hands on lab video covering many aspects of CI/CD.

## Stage 2: Basic CI

- [ ] **Done with Stage 2**

1. [ ] Read the Table Of Contents for the [.gitlab-ci.yml docs](https://docs.gitlab.com/ee/ci/yaml/README.html)
   and make sure you know the meaning of every heading.
1. [ ] Set up a repository on GitLab.com and create a .gitlab-ci.yml file to
   get a pipeline running using the free shared runners on GitLab.com (this can
   be a basic toy project).
   - [ ] For additional steps on setting up your first GitLab project, refer to [freeCodeCamp - Gitlab CI Lesson 2 Notes](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md#lesson-2-your-first-gitlab-project).
1. [ ] Note that your builds may show as **Pending** until a shared runner becomes
   available (the next step will solve this).
1. [ ] Install your own Runner (locally or hosted somewhere) following the [Setup Instructions](https://docs.gitlab.com/runner/install/)
1. [ ] Read about the different [Executors](https://docs.gitlab.com/runner/executors/)
   - [ ] The Docker executor is commonly used by GitLab customers; watch this video about [GitLab & Docker](https://www.youtube.com/watch?v=ugOrCcbdHko&index=12&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
1. [ ] Register your runner as a [Specific Runner](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#specific-runners)
   on your project, and test that it now runs the builds for your project.
1. [ ] Create an example project on your own GitLab instance.
1. [ ] Register a shared runner on your GitLab instance and make sure it can
   successfully run the build for your project.
1. [ ] Optional, for GitLab.com: Watch [this video](https://drive.google.com/file/d/17pjKta4qfTyHcyy9qVpSNQhmysYBRZTp) on ways to troubleshoot runner on GitLab.com. Refer to [this issue for questions and links](https://gitlab.com/gitlab-com/support/dotcom/dotcom-meta/-/issues/58#note_80876655). Note: As it was recorded August 2018, some links may be out of date.

## Stage 3: Intermediate CI

- [ ] **Done with Stage 3**

1. [ ] Read about [Pipelines and Jobs](https://docs.gitlab.com/ee/ci/pipelines/). Watch the [Pipeline stages video](https://youtu.be/PGyhBwLyK2U?t=2594) from freeCodeCamp.org to get an understanding of how jobs run.  Part of playlist running from (43m:14s - 48m:10s)
1. [ ] Check how to [see the status of Pipeline and Jobs](https://docs.gitlab.com/ee/ci/quick_start/#view-the-status-of-your-pipeline-and-jobs).
1. [ ] Read about using [Docker Images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html) with GitLab CI.
1. [ ] [Register a runner that uses the Docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#register-a-runner-that-uses-the-docker-executor)
   and change your `.gitlab-ci.yml` to use a Docker image for your language of
   choice.
1. [ ] Review the Runner [Troubleshooting guide and try to reproduce issues where possible](https://docs.gitlab.com/runner/faq/).
1. [ ] Read about [Artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).
1. [ ] Change your `.gitlab-ci.yml` to [create artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#create-job-artifacts).
1. [ ] Set the [Maximum Artifacts size](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#maximum-artifacts-size) and create artifacts above the limit, then check the build log for errors.
1. [ ] Read about [Multi-project pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines).
1. [ ] Change your `.gitlab-ci.yml` to trigger a pipeline in another project.
1. [ ] Read the rest of the [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/).
1. [ ] Follow and complete the [Create a complex pipeline](https://docs.gitlab.com/ee/ci/quick_start/tutorial.html) tutorial from our docs to put all of stage 3 into practice.
## Stage 4: Advanced CI

Review with your trainer/manager to decide if you will do some or all of the tasks in this stage.

- [ ] **Done with Stage 4**

1. [ ] Set up a [manual deploy step](https://docs.gitlab.com/ee/ci/yaml/#when), so that it does not deploy to production unless someone clicks the button to do so.
1. [ ] Speed up a pipeline using [caching](https://docs.gitlab.com/ee/ci/yaml/#cache).
1. [ ] Consider taking the [GitLab Certified CI/CD Associate Learning Path](https://university.gitlab.com/learning-paths/certified-ci-cd-specialist-learning-path) course on LevelUp.

### Stage 4.1 Container Registry

Consider doing the [Container Registry](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Container%20Registryd) module first/instead.

1. [ ] Set up a [container registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html) for your project.
1. [ ] [Build a Docker image and upload it](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-images-by-using-docker-commands) to the project registry as part of the CI pipeline.
1. [ ] Learn how to [Use kaniko to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) as an alternative way.


### Stage 4.2 Kubernetes

1. [ ] [Add or integrate a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/) to your project.
1. [ ] Deploy your [application to Kubernetes](https://medium.com/john-lewis-software-engineering/deploying-to-google-kubernetes-engine-from-gitlab-ci-feaf51dae0c1) via GitLab CI using the Docker container you built. One option is to [customize the Auto DevOps template](https://docs.gitlab.com/ee/topics/autodevops/#customizing).
1. [ ] Set up [Review Apps](https://docs.gitlab.com/ee/topics/autodevops/#customizing) to be deployed to Kubernetes.



## Stage 5: Tickets

- [ ] **Done with Stage 5**

Finding tickets to work on can be tricky especially in the smaller SGG queues. Make sure you let your SGG group and the wider support organisation know that you are looking for CI tickets to work on (using your SGG specific Slack channel `#spt_gg_<groupname>` and the `#support_team-chat`).
It is also encouraged to take CI tickets from other SGG groups as documented - [Support Global Groups - FAQ > Tickets in other groups](https://handbook.gitlab.com/handbook/support/support-global-groups/sgg-faq/#tickets-in-other-groups).

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 10 GitLab CI tickets and paste the links here, even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay it to the customer.
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __
   1. __

## Stage 6: Pair on Customer Calls

This step is considered optional however highly recommended as to give you more experience. These types of calls are very rare so consider participating in at least one call if available.

1. [ ] Participate on two customer calls regarding troubleshooting GitLab CI.
  1. call with ___
  1. call with ___

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage

1. [ ] Have a [CI expert](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) in Support review 5-7 of the tickets from Stage 5 and add a comment on this issue to confirm that sufficient CI knowledge has been demonstrated.
1. [ ] Schedule a call with your manager (or integrate into 1:1) to review how the module went.
1. [ ] Update your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: CI
     level: 2
   ```

/label ~module
/label ~"Module::Continuous Integration"
/assign me
