---
module-name: "Service Desk"
area: "Product Knowledge"
gitlab-group: "Plan:Certify"
maintainers:
  - TBD
---

## Overview

**Goal**: Set a clear path for Service Desk Expert training

*Length*: <estimate # of hours, use single number or range>

**Objectives**: At the end of this module, you should be able to:

- Learn about Service Desk
- Learn about GitLab's Service Desk integration
- Feel comfortable answering some common scenarios.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Service Desk - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Service Desk questions to you.
1. [ ] Optional: In your Slack Notification Settings, set **Service Desk** and **Incoming Email** as [Keyword Notifications](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications).
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what Service Desk is

1. [ ] Read documentation
  1. [ ] [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html)
  1. [ ] [Setting Up Incoming Email](https://docs.gitlab.com/ee/administration/incoming_email.html#set-it-up)
  1. [ ] [Using customized email templates](https://docs.gitlab.com/ee/user/project/service_desk.html#using-customized-email-templates)
  1. [ ] [Service Desk Troubleshooting](https://handbook.gitlab.com/handbook/support/workflows/service_desk_troubleshooting/)
1. [ ] Watch videos
  1. [ ] [Demo of Service Desk](https://www.youtube.com/watch?v=m6oHRIeT1AE&feature=youtu.be)
  1. [ ] [Learn about Service Desk](https://youtu.be/PId7jlcAkqk)
1. [ ] Ticket Review
   1. [ ] Importance of `%{key}` in the config
      1. [ ] Ticket: [141916](https://gitlab.zendesk.com/agent/tickets/141916)
      1. [ ] Code: [incoming_email.rb](https://gitlab.com/gitlab-org/gitlab/blob/v12.6.1-ee/lib/gitlab/incoming_email.rb#L10-20)
   1. [ ] Importance of using two separate mail boxes for `Incoming Mail` and `Service Desk`
      1. [ ] Ticket: [233518](https://gitlab.zendesk.com/agent/tickets/233518)
   1. [ ] GitLab ignores auto-replies, which might catch automatically forwarded emails
      1. [ ] Ticket: [129899](https://gitlab.zendesk.com/agent/tickets/129899)
      1. [ ] Code: [receiver.rb](https://gitlab.com/gitlab-org/gitlab/blob/v12.6.1-ee/lib/gitlab/email/receiver.rb#L99-118)
   1. [ ] Sometimes, we just need to get the customer to test sending an email
      1. [ ] Ticket: [137369](https://gitlab.zendesk.com/agent/tickets/137369)

## Stage 2: Technical setup

1. [ ] Setup a GitLab instance via Omnibus (Service Desk is available to all tiers.)
1. [ ] Enable incoming email on your GitLab instance (recommended to use your
      `@gitlab.com` email account)
1. [ ] Check your email credentials are working (
      `sudo gitlab-rake gitlab:incoming_email:check`)
1. [ ] Create a new project named `service-desk-test`
1. [ ] Test Service Desk is working
  1. Create an issue via email
  1. Comment on the issue via the GitLab UI
  1. Reply to said comment via email
1. [ ] Create a new `Thank you email` template

## Stage 3: Tickets

1. [ ] Answer 2 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __

## Stage 4: Quiz

1. [ ] Ping a trainer to assist you in testing your instance. The only thing you
      need to give the trainer is the email address to use for your project
      (created in Stage 2).
1. [ ] Request the trainer create an issue on the project via email.
1. [ ] Post a screenshot of the created issue with the trainer's comment down
      below.
1. [ ] Post a screenshot of the `Thank you email` (make sure it is using your
      custom template) that the trainer receives after they create an issue.
1. [ ] Once you have completed this, have the trainer comment below
      acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in your [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/wikis/Support-team-entry) with this training module's topic. You will now be listed as an expert in Service Desk on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::Service Desk"
/assign me