---
is_hidden: true
comments: false
---

# Git Implementation Details
The [Git SCM book](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain) has a great explanation of the core data structures used by Git and there are lots of how-to's on using it, but how to we get from `git push` to packed refs being shot across the internet?  

Let's dig into the implementation!

Of course all of this could change in future versions of Git, but probably not anytime soon.  Plus, it's interesting to understand how a major cross-platform tool is organized internally.

**Note**: This performed using the following configuration:

   * Ubuntu 18.10
   * Git 2.20.1
      * Built from source with `-g -Og -Wall -fno-omit-frame-pointer`

# Getting to `git` - How subcommands are executed
To start, we'll use our from-source build to look into how Git subcommands are called using a simple push via SSH as an example.

`GIT_TRACE=true git push origin master` outputs:

```bash
21:25:09.041029 git.c:418               trace: built-in: git push origin master
21:25:09.041359 run-command.c:643       trace: run_command: unset GIT_PREFIX; ssh git@192.168.122.140 'git-receive-pack '\''root/git_strace.git'\'''
21:25:09.745266 run-command.c:643       trace: run_command: git pack-objects --all-progress-implied --revs --stdout --thin --delta-base-offset --progress
21:25:09.747352 git.c:418               trace: built-in: git pack-objects --all-progress-implied --revs --stdout --thin --delta-base-offset --progress
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 2 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 373 bytes | 373.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To 192.168.122.140:root/git_strace.git
   8d7e1e2..3f8bdb5  master -> master
```

Git's internal tracing calls out four actions:

   1. `built-in: git push origin master`
   2. `run_command: ssh git@192.168.122.140 git-receive-pack ...` 
   3. `run_command: git pack-objects ...`
   4. `built-in: git pack-objects ...`

`ssh` has a `run_command` event, while `git pack-object` has both a `run_command` and a `built-in` event.  It sounds like Git may be treating running external programs and Git subcommands similarly.  We'll use [strace](http://man7.org/linux/man-pages/man1/strace.1.html) to see which programs are being executed during the push.

`strace -fe execve git push origin master` shows two child programs were executed by Git:

   1. `/usr/bin/ssh git@192.168.122.140 git-receive-pack root/git_strace.git`
   2. `/usr/libexec/git-core/git pack-objects --all-progress-implied --revs --stdout --thin --delta-base-offset --progress`

We'll look into the details of what `receive-pack` and `pack-objects` do in a future installment, but for now the Git SCM page has a good write up [here](https://git-scm.com/docs/git-receive-pack) and [here](https://git-scm.com/docs/git-pack-objects).  For now let's just look into why we are calling `/usr/libexec/git-core/git` to perform `pack-objects`?  Is it this different from `/usr/bin/git`?

```bash
$ openssl sha256 /usr/bin/git /usr/libexec/git-core/git
SHA256(/usr/bin/git)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
SHA256(/usr/libexec/git-core/git)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
```

Hmm, so we have an identical copy of Git used to perform tasks the main Git executable could presumably perform. A bit of `grep`ing finds this reference in `Documentation/howto/new-command.txt`:

> Git subcommands are standalone executables that live in the Git exec path, normally /usr/lib/git-core.  The git executable itself is a thin wrapper that knows where the subcommands live, and runs them by passing command-line arguments to them.

#### Journey into `/git-core`

First, how many subcommands are present in `/usr/libexec/git-core`?

```bash
$ find /usr/libexec/git-core -maxdepth 1 -type f -executable | wc -l
164

$ ls -l
-rwxr-xr-x 127 root root 18190608 Dec 28 04:52 git*
-rwxr-xr-x 127 root root 18190608 Dec 28 04:52 git-add*
-rwxr-xr-x   1 root root    44066 Dec 28 04:52 git-add--interactive*
-rwxr-xr-x 127 root root 18190608 Dec 28 04:52 git-am*
-rwxr-xr-x 127 root root 18190608 Dec 28 04:52 git-annotate*
-rwxr-xr-x 127 root root 18190608 Dec 28 04:52 git-apply*
-rwxr-xr-x   1 root root    37001 Dec 28 04:52 git-archimport*
... and many more 'git-' commands
```

As expected we see the many Git subcommands here, but it's a bit odd that so many of them are the same size as `git`.

```bash
$ cd /usr/libexec/git-core
$ openssl sha256 git git-add git-am git-annotate git-apply /usr/bin/git
SHA256(git)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
SHA256(git-add)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
SHA256(git-am)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
SHA256(git-annotate)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
SHA256(git-apply)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
SHA256(/usr/bin/git)= 3122c8ffa90170489b057b2806a0fc4c37ece6731482de6db64a401580559685
```

Yup, these six files are identical other than their title.  If we search by the file size of `git` how many copies are there?

```bash
$ ls -l | grep 18190608 | wc -l
123
```

123 copies, that seems a bit extravagant.  So rather than a thin wrapper as described in the how-to, it looks like most subcommands are full copies of `git`.

So that covers those files, but what are the rest?

```bash
$ find /usr/libexec/git-core/ -maxdepth 1 -type f -executable | xargs file | awk '{file=""; for (i=2; i<5; i++){file=file" "$i}; print file}' | sort | uniq -c | sort -n
      1  Python script, ASCII
      7  Perl script text
     17  POSIX shell script,
    139  ELF 64-bit LSB
```

So we have shell scripts, python, and perl, and 16 compiled files that are not copies of `git`.  What are the compiled files?

```bash
$ find /usr/libexec/git-core/ -maxdepth 1 -type f -executable -printf "%p %s\n" | grep -v 18190608 | awk '{print $1}' | xargs file -F ' ' | grep ELF | awk '{print $1}' | sort
/usr/libexec/git-core/git-credential-cache
/usr/libexec/git-core/git-credential-cache--daemon
/usr/libexec/git-core/git-credential-store
/usr/libexec/git-core/git-daemon
/usr/libexec/git-core/git-fast-import
/usr/libexec/git-core/git-http-backend
/usr/libexec/git-core/git-http-fetch
/usr/libexec/git-core/git-http-push
/usr/libexec/git-core/git-imap-send
/usr/libexec/git-core/git-remote-ftp
/usr/libexec/git-core/git-remote-ftps
/usr/libexec/git-core/git-remote-http
/usr/libexec/git-core/git-remote-https
/usr/libexec/git-core/git-remote-testsvn
/usr/libexec/git-core/git-shell
/usr/libexec/git-core/git-sh-i18n--envsubst
```

Based on [the docs](https://git-scm.com/docs), these mostly handle credentials and networking, but it's not clear to me what makes them special.

What functions are handled via bash/perl/python?

```bash
$ find /usr/libexec/git-core/ -maxdepth 1 -type f -executable -printf "%p\n" | xargs file -F ' ' | grep -v ELF | sort -k2
/usr/libexec/git-core/git-svn                       Perl script text executable
/usr/libexec/git-core/git-cvsimport                 Perl script text executable
/usr/libexec/git-core/git-cvsserver                 Perl script text executable
/usr/libexec/git-core/git-archimport                Perl script text executable
/usr/libexec/git-core/git-send-email                Perl script text executable
/usr/libexec/git-core/git-cvsexportcommit           Perl script text executable
/usr/libexec/git-core/git-add--interactive          Perl script text executable
/usr/libexec/git-core/git-gui                       POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-stash                     POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-bisect                    POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-citool                    POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-mergetool                 POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-submodule                 POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-quiltimport               POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-web--browse               POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-gui--askpass              POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-request-pull              POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-filter-branch             POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-legacy-rebase             POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-merge-octopus             POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-merge-resolve             POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-merge-one-file            POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-difftool--helper          POSIX shell script, ASCII text executable
/usr/libexec/git-core/git-instaweb                  POSIX shell script, ASCII text executable, with very long lines
/usr/libexec/git-core/git-p4                        Python script, ASCII text executable
```

Looks like a mix, lots of helper functions, but also some common commands like `git-stash`.

#### When are the separate subcommand files executed?
It is odd that we have these named subcommand files that weren't used when we performed the `git push`.  That executed `/usr/libexec/git-core/git`, not `/usr/libexec/git-core/git-push`, so why bother with the subcommand file at all?

What happens with subcommands that are not included in the primary binary?  Let's try running a subcommand that is handled via a shell script, `git stash` as an easy example, and see what file is executed.

```bash
$ strace -fe execve git stash
execve("/usr/bin/git", ["git", "stash"], 0x7ffc07f8c278 /* 21 vars */) = 0
trace: Process 5670 attached
[pid  5670] execve("/usr/libexec/git-core/git-stash", ["/usr/libexec/git-core/git-stash"], 0x562ccf7cb2b0 /* 22 vars */) = 0
...
```

So the named files are directly called in the case of non-compiled subcommands.  What happens when we directly execute a subcommand file that is also bundled in `git`?

```bash
$ /usr/libexec/git-core/git-push origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 2 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 230 bytes | 28.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0)
To 192.168.122.140:root/git_strace.git
   02dcd84..83c2361  master -> master
```

So that works, but isn't what Git chooses to do typically.

#### Why the duplicate files?
Why bother with separate executables for subcommands, if Git ignores them internally?  I haven't found any explicit documentation, but my guess is that this is to keep a consistent external interface for compiled and script subcommands.

In addition, many subcommands have been moved from a script to C over the years.  Older integrations with Git may have been built assuming that a `git-push` executable exists, for example.

Now let's reverse the question, why not split the C subcommands into many small binaries?  I suspect that keeping them together simplifies the build process - one binary with one set of dependencies.  If you've built one subcommand you've built the others, with the exception of the 19 that are separate for some reason.

## Other Install Methods and Operating Systems
Are these duplicate files normal, or an artifact of my building from source?  Let's check how standard Git installs work on Linux and other OS's.

#### Git via `apt-get`
 To start we'll use look into Git as installed via `sudo apt-get install git`.  Is the same `/usr/libexec/git-core` directory present?

```bash
apt-get$ ls -lA /usr/libexec/git-core
ls: cannot access '/usr/libexec/git-core': No such file or directory
```

Hmm, the path must be different.  The man page mentions an `--exec-path` option:

> --exec-path[=<path>]
>        Path to wherever your core Git programs are installed. This can also be controlled by setting the GIT_EXEC_PATH environment variable. If
>        no path is given, git will print the current setting and then exit.
   
Let's try that:

```bash
apt-get$ git --exec-path
/usr/lib/git-core
```

That's the path mentioned in the howto doc above, makes sense.

```bash
apt-get$ ls -lA /usr/lib/git-core
-rwxr-xr-x 1 root root 2739984 Dec 15 09:31 git
lrwxrwxrwx 1 root root       3 Dec 15 09:31 git-add -> git
-rwxr-xr-x 1 root root   44066 Dec 15 09:31 git-add--interactive
lrwxrwxrwx 1 root root       3 Dec 15 09:31 git-am -> git
lrwxrwxrwx 1 root root       3 Dec 15 09:31 git-annotate -> git
lrwxrwxrwx 1 root root       3 Dec 15 09:31 git-apply -> git
lrwxrwxrwx 1 root root       3 Dec 15 09:31 git-archive -> git
...
```

So normally these files are symlinks against a single copy in `/usr/lib/git-core`, but we still have separate `/usr/bin/git` and `/usr/lib/git-core/git` files.

Back to the main test machine!  Will a push succeed if we make `/usr/bin/git` a symlink to `/usr/libexec/git-core/git`?

```bash
$ sudo mv /usr/bin/git /usr/bin/git.bak && sudo ln -s /usr/libexec/git-core/git /usr/bin/git
$ git push origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 2 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 233 bytes | 29.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0)
To 157.230.10.127:root/git_strace.git
   ee3a237..1d5e5c0  master -> master
```

It works for push, but who knows if all subcommands are unaffected.

#### macOS
With macOS we see a similar setup to `apt-get` with Xcode, but Homebrew is a bit different.

Xcode:
```bash
macos$ /usr/bin/git --exec-path
/Applications/Xcode.app/Contents/Developer/usr/libexec/git-core

macos$ ls -l /Applications/Xcode.app/Contents/Developer/usr/libexec/git-core
-rwxr-xr-x   1 root  wheel  2128288 Oct 19 21:23 git
lrwxr-xr-x   1 root  wheel        3 Jul 25 22:34 git-add -> git
-rwxr-xr-x   1 root  wheel    44784 Aug 14 05:15 git-add--interactive
lrwxr-xr-x   1 root  wheel        3 Jul 25 22:34 git-am -> git
lrwxr-xr-x   1 root  wheel        3 Jul 25 22:34 git-annotate -> git
lrwxr-xr-x   1 root  wheel        3 Jul 25 22:34 git-apply -> git
lrwxr-xr-x   1 root  wheel        3 Jul 25 22:34 git-archive -> git
...
```

Homebrew:
```bash
macos$ /usr/local/bin/git --exec-path
/usr/local/Cellar/git/2.20.1/libexec/git-core

macos$ ls -l /usr/local/Cellar/git/2.20.1/libexec/git-core
lrwxr-xr-x   1 wchandler  staff       13 Dec 14 23:24 git -> ../../bin/git
lrwxr-xr-x   1 wchandler  staff       13 Dec 14 23:24 git-add -> ../../bin/git
-rwxr-xr-x   1 wchandler  staff    44259 Dec 31 00:47 git-add--interactive
lrwxr-xr-x   1 wchandler  staff       13 Dec 14 23:24 git-am -> ../../bin/git
lrwxr-xr-x   1 wchandler  staff       13 Dec 14 23:24 git-annotate -> ../../bin/git
lrwxr-xr-x   1 wchandler  staff       13 Dec 14 23:24 git-apply -> ../../bin/git
-rwxr-xr-x   1 wchandler  staff    37194 Dec 31 00:47 git-archimport
lrwxr-xr-x   1 wchandler  staff       13 Dec 14 23:24 git-archive -> ../../bin/git
...
```

Here the `git-core` binaries symlink to `/usr/local/Cellar/git/2.20.1/bin`.  We still only have one copy of the main Git executable, just with an extra layer of indirection.

```bash
macos$ ls -l /usr/local/Cellar/git/2.20.1/bin
-rwxr-xr-x  1 wchandler  staff  2436304 Dec 31 00:47 git
-rwxr-xr-x  1 wchandler  staff   162924 Dec 31 00:47 git-cvsserver
lrwxr-xr-x  1 wchandler  staff        3 Dec 14 23:24 git-receive-pack -> git
-rwxr-xr-x  1 wchandler  staff  1363252 Dec 31 00:47 git-shell
lrwxr-xr-x  1 wchandler  staff        3 Dec 14 23:24 git-upload-archive -> git
lrwxr-xr-x  1 wchandler  staff        3 Dec 14 23:24 git-upload-pack -> git
-r-xr-xr-x  1 wchandler  staff   351673 Dec 14 23:24 gitk
```

#### Git Bash on Windows
What about Windows?  The Git Bash bundle installs a number of Git executables:
   
   * `C:\Program Files\Git\cmd\git.exe` - 40 KB
   * `C:\Program Files\Git\bin\git.exe` - 40 KB
   * `C:\Program Files\Git\mingw\bin\git.exe` - 2.21 MB
   * `C:\Program Files\Git\mingw\libexec\git-core\git.exe` - 2.21 MB

Git Bash defaults to use `C:\Program Files\Git\mingw\bin\git.exe` for `git`, and `libexec\git-core\git.exe` is familiar from Linux, so why the other two `git.exe's? 

Using `Process Monitor`, we can see that `C:\Program Files\Git\cmd\git.exe` and `C:\Program Files\Git\bin\git.exe` are wrappers that will execute `C:\Program Files\Git\minggw\bin\git.exe`.  My guess is that these other executables enable other applications to call the main Git executable more easily.  

Like the from-source version of Git on Linux, the subcommand files in `C:\Program Files\Git\mingw\libexec\git-core` are full executables rather than symlinks.

Apparently in the past there was a `git.cmd` file that has since been replaced with the small `git.exe` listed above.  [This Stack Overflow answer](https://stackoverflow.com/questions/8947140/git-cmd-vs-git-exe-what-is-the-difference-and-which-one-should-be-used) has a bit more information.

## Summary
On Linux running `/usr/bin/git` does not directly execute a Git command.  For most subcommands it will call a duplicate copy of itself that is housed in `/usr/lib/git-core` to perform the action.  The same behavior is seen on macOS and Windows with differing paths.

Not directly executing the subcommand in `/usr/bin/git` is most likely to enable cross-platform compatibility.  The `git.exe` executable in $PATH on Windows is not a full version of `git`, by calling into `git-core` we can be sure that the correct file is being executed.

**Subcommand breakdown:**
   * 123 subcommands use a copy of `git`
      * In the standard `apt-get` package the files for subcommands are a symlink to `/usr/lib/git-core/git`
      * Self-compiled installs have separate copies of the executable for each subcommand
      * Windows also uses separate copies of the executable for subcommands, and has two additional wrapper `git.exe` executables that will call into the primary executable at `C:\Program Files\Git\mingw\bin\git.exe`
   * 25 subcommands are bash, perl, and python scripts covering a variety of functions
   * 16 subcommands are compiled programs that are not bundled into the main `git` executable.  These mostly perform security and networking functions, but it is unclear to me why these specifically need to be separate
